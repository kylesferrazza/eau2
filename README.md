![Build and Test](https://github.com/kylesferrazza/eau2/workflows/Build%20and%20Test/badge.svg)

# eau2

*We have a customer, it's Pear, if they pay we are set.*

Kyle Sferrazza and Connor Mullaly

Term Project for **CS4500**: *Software Development* at Northeastern University.

[Project Description](http://janvitek.org/events/NEU/4500/s20/projects2.html)
