# Introduction
The `eau2` system is a distributed data processing application. Its purpose is to load datasets and allow queries to be run on the data. This will allow a user to filter through large datasets very quickly and retrieve any information that they need. To run efficiently, the system will use a distributed approach with multiple running nodes. Each node will store a subset of the dataset and will be used in conjunction to achieve the desired queries.

# Running Tests

We have chosen to compile our project solely with cmake and we utilize ctest to run all of our tests for us. To compile the project, run `cmake .` in the eau2 directory. After that, run `ctest .` to run all of the tests. If you want to run a specific application, navigate to the `/bin` directory after you have compiled. You can run `./app_trivial` or `./app_demo` . There are two different scripts to run the wordcount application from the eau2 directory after running make.run `./run-word-count.sh` to simulate a wordcount application with 4 nodes and run `./run-word-count-many.sh [NUM NODES]` to simulate the wordcount application with many nodes.

We also have three different networking tests that can be run using the commands `make runDataTest` , `make runKillTest` , or `make runExtraTest` from the eau2 directory.

# Architecture

The system is organized into three abstraction layers.

From bottom to top:

## Key-Value Store

This is where the data actually gets stored on each node. Each node holds a portion of the data, and the stores talk to each other over the network to exchange data when necessary.

## Distributed Data Access

This layer provides an abstraction on the Key-Value store. Its clients can use distributed arrays and dataframes without worrying where the data is or how it is moved between nodes.

## Application

The Application layer abstracts even further to allow library clients to use the overall distributed application. This is where queries to run on the system are implemented by the user.

# Implementation

Relevant classes by layer:

## Store

### `KVStore` 
The key-value store that holds the data associated with this node.

#### Fields

| name                                      | description       |
| ----------------------------------------- | ----------------- |
| `std::unordered_map<std::string, char*> storage` | storage of serialized blobs |

#### Methods

| name                         | description                                                   |
| ---------------------------- | ------------------------------------------------------------- |
| `char *get(Key k)` | non-blocking get from the key-value store                     |
| `char *waitAndGet(Key k)` | blocking get from the key-value store (wait for key to exist) |
| `void *put(Key k, char* b)` | stores the given Value at the given key in the store          |

### `KDStore` 

The key-dataframe store that holds the dataframe associated with this node.
It will basically use the KVStore and convert Value to Dataframe

#### Fields

| name          | description                         |
| ------------- | ----------------------------------- |
| `KVStore *kv` | The store of Values to be converted |

#### Methods

| name                              | description                                                   |
| --------------------------------- | ------------------------------------------------------------- |
| `DataFrame *get(Key k)` | non-blocking get from the key-value store                     |
| `DataFrame *waitAndGet(Key k)` | blocking get from the key-value store (wait for key to exist) |
| `void *put(Key k, DataFrame *df)` | stores the given dataframe at the given key in the store      |

### `KKLStore` 

The key-KeyList store that holds the KeyList associated with this node.

#### Fields

| name          | description                         |
| ------------- | ----------------------------------- |
| `KVStore *kv` | The store of Values to be converted |

#### Methods

| name                              | description                                                   |
| --------------------------------- | ------------------------------------------------------------- |
| `KeyList *get(Key k)` | get from the key-value store                     |
| `void *put(Key k, KeyList *kl)` | stores the given KeyList at the given key in the store      |

### `DistributedKDStore` 

This is a key to dataframe store that is itself a client. It interacts with other nodes through networking to retrieve dataframes that are not stored in local memory.

#### Fields

| name          | description                         |
| ------------- | ----------------------------------- |
| `KDStore *dfstore` | The local store of dataframes |
| `KKLStore *klstore` | The local store of keyLists |
| `std::vector<WaitAndGet> waiting` | the list of pending waitAndGet requests | 

#### Methods

| name                              | description                                                   |
| --------------------------------- | ------------------------------------------------------------- |
| `KeyList *get(Key k)` | non-blocking get of the keyList for the given Key                  |
| `KeyList *waitAndGet(Key k)` | blocking get (wait for key to exist) |
| `void *put(Key k, DataFrame *df)` | stores the given dataframe distributed across the nodes at generated keys and stores the list of keys for the chunks at the given key in the store      |
| `DataFrame *toDataFrame(KeyList *kl)` | converts a keyList to a dataframe by retrieving each chunk and combining them into a complete dataframe |
| `DataFrame *getDf_(Key k)` | retrieves the dataframe stored at the key which has been generated by the chunking of put |
| `void local_map(KeyList *kl, Reader *reader)` | map the reader on every chunk that is stored local to this node in the given keylist |

### `Key` 

Used as keys in `Store` .

#### Fields

| name              | description                                        |
| ----------------- | -------------------------------------------------- |
| `std::string key` | a string key (for searching)                       |
| `int homeNode` | a home node id (says which KV store owns the data) |

#### Methods

| name                                 | description                             |
| ------------------------------------ | --------------------------------------- |
| `Key(std::string key, int homeNode)` | construct a `Key` with the given fields |

### `KeyList` 

A list of generated keys that dataframe chunks are stored at.

#### Fields

| name              | description                                        |
| ----------------- | -------------------------------------------------- |
| `std::vector<Key *> keys; ` | A the internal list of keys |

#### Methods

| name                                 | description                             |
| ------------------------------------ | --------------------------------------- |
| `Key *at(size_t idx)` | retreive the key at the given index |
| `void push_back(Key *k)` | add a key to the end of the list |
| `size_t size()` | return how many keys are in the list |

## Data Access

### `DataFrame` 

The key-value store that holds the data associated with this node.
It is used as values in `Store` .

#### Fields

| name                     | description                                                                               |
| ------------------------ | ----------------------------------------------------------------------------------------- |
| `ColumnStorage *storage` | Storage for the columns of the dataframe. Resizable two-dimensional object pointer array.|

#### Methods

| name                                                                       | description                                         |
| -------------------------------------------------------------------------- | --------------------------------------------------- |
| `static DataFrame *fromScalar(Key &k, Store &s, int scalar)` | Construct a dataframe with one `int` value.|
| `static DataFrame *fromScalar(Key &k, Store &s, double scalar)` | Construct a dataframe with one `double` value.|
| `static DataFrame *fromScalar(Key &k, Store &s, bool scalar)` | Construct a dataframe with one `bool` value.|
| `static DataFrame *fromScalar(Key &k, Store &s, String *scalar)` | Construct a dataframe with one `String *` value.|
| `static DataFrame *fromArray(Key &k, Store &s, size_t size, int *arr)` | Construct a dataframe with an array of `int` s.|
| `static DataFrame *fromArray(Key &k, Store &s, size_t size, double *arr)` | Construct a dataframe with an array of `double` s.|
| `static DataFrame *fromArray(Key &k, Store &s, size_t size, bool *arr)` | Construct a dataframe with an array of `bool` s.|
| `static DataFrame *fromArray(Key &k, Store &s, size_t size, String **arr)` | Construct a dataframe with an array of `String *` s.|
| `static DataFrame *fromVisitor(Key *key, DistributedKDStore *store, const char *schema, Writer *w)` | Constructs a dataframe using the given Writer and stores it at the given key |
| `int get_int(size_t col, size_t row)` | Return a int from the dataframe.|
| `double get_double(size_t col, size_t row)` | Return a double from the dataframe.|
| `bool get_bool(size_t col, size_t row)` | Return a bool from the dataframe.|
| `std::string get_string(size_t col, size_t row)` | Return a String from the dataframe.|

## Application

### `Application` 

User programs (queries) will extend this class.

#### Fields

| name          | description                        |
| ------------- | ---------------------------------- |
| `DistributedKDStore *kv` | the key-value store for this node.|
| `size_t idx_` | the node idx for this application |
| `bool isServer` | determines if this application started a server |
| `Server *s` | The server started by the first node |

#### Methods

| name                      | description                                                        |
| ------------------------- | ------------------------------------------------------------------ |
| `Application(size_t idx)` | Initialize the application with a node id.|
| `virtual void run_()` | Do the work for the current node. Main application code goes here.|
| `int this_node()` | return the current node's id                                       |

# Use Cases

Below is an example use case of our client utilizing the eau2 system. This is based on hypothetical operations that a client might desire and it is still unsure what functionality it will have in the end.

``` cpp
class Demo : public Application {
public:
  Key main("main", 0); 
  Key verify("verif", 0); 
  Key check("ck", 0); 
 
  Demo(size_t idx): Application(idx) {}
 
  void run_() override {

    switch(this_node()) {
    case 0:   producer(); break; 
    case 1:   counter(); break; 
    case 2:   summarizer(); 

   }
  }
 
  void producer() {

    size_t SZ = 100*1000; 
    double* vals = new double[SZ]; 
    double sum = 0; 
    for (size_t i = 0; i < SZ; ++i) sum += vals[i] = i; 
    DataFrame::fromArray(&main, &kv, SZ, vals); 
    DataFrame::fromScalar(&check, &kv, sum); 

  }
 
  void counter() {

    DataFrame* v = kv.waitAndGet(main); 
    size_t sum = 0; 
    for (size_t i = 0; i < 100*1000; ++i) sum += v->get_double(0, i); 
    p("The sum is  ").pln(sum); 
    DataFrame::fromScalar(&verify, &kv, sum); 

  }
 
  void summarizer() {

    DataFrame* result = kv.waitAndGet(verify); 
    DataFrame* expected = kv.waitAndGet(check); 
    pln(expected->get_double(0, 0)==result->get_double(0, 0) ? "SUCCESS":"FAILURE"); 

  }
}; 
```

# Open Questions

* Why were we given code with memory leaks in it, such as the SIMap?

# Status

## Completed So Far:

* __Dataframe__:

We have already completed an implementation of a dataframe that will hold the information of a dataset and run arbitrary operations on this data.

* __Networking__:

We have already created a networking API that can be used to allow nodes to communicate with each other. We also added message types for Put, Get, WaitAndGet, and Reply to allow for distributed key stores

* __Serialization__:

We have completed all necessary serialization. This includes all message serialization and dataframe serialization including unit tests.

* __Key Value Store__:

We have implemented a simple key to dataframe store that can run locally. This was enough to run the trivial example.

* __Key Value Store Distribution__:

We have implemented functionality in the dataframe to chunk the data based on an arbitrary maximum rows per chunk. We also have functionality to merge these chunks into a single dataframe. The distributedKDStore put chunks at generated keys at each node. This happens whenever a dataframe is being put into the distributed store. The list of keys to the chunk is then stored at the master key for that dataframe, telling each node where the chunks are located.

* __Trivial Application__:

The trivial application compiles and works correctly

* __Demo application__:

The demo application works correctly and incorporates sending dataframes over the network. One issue that we were seeing was that it works every time on WSL (windows subsystem for linux), but it fails every so often on unix systems due to a connection error. We are still working to figure this out, but we believe this to be some difference in windows vs unix sockets.

* __WordCount Application__:

We have implemented the WordCount application. It works successfully when running 5 nodes, but we are getting connection errors that we were not able to debug when running 6+ nodes. We are also having difficulty synchronizing the starting of the server with the connection of different nodes since this was the first application that did not run each node from the same main file. This caused us to add sleeps into the code to wait for arbitrary connections.

## Needs to be done:

* __Data Adapter__:

We have selected a group's data adapter that is already completed. This will allow us to read in data from a SoR file. Although the data adapter has been implemented, we will need to somehow interface this with our dataframe in order to read a file in as a dataframe.

* __Linus Application__ 

We have not started on the linus application yet

