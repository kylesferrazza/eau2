#!/usr/bin/env bash

set -euo pipefail

if [ $# -ne 1 ]; then
  echo "Usage: $0 <num-nodes>"
  exit 1
fi

NUM=$1

echo "Starting word count with $NUM nodes."

( ./bin/wordcount 0 ./test/application/100k.txt ) &

sleep 1

for n in $(seq 1 $(($NUM-1))); do
  ( ./bin/wordcount "$n" ./test/application/100k.txt ) &
  sleep 0.2
done

wait

echo "Word count finished."
