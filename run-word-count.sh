#!/usr/bin/env bash

set -euo pipefail

echo "Starting word count."
{
  ./bin/wordcount 0 ./test/application/100k.txt
} &

sleep 0.2

{
  ./bin/wordcount 1 ./test/application/100k.txt
} &

{
  ./bin/wordcount 2 ./test/application/100k.txt
} &

{
  ./bin/wordcount 3 ./test/application/100k.txt
} &

wait
echo "Word count finished."
