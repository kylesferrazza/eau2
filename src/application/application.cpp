#include "application.h"

#include "../networking/network.h"
#include "../networking/threadmsg.h"

ThreadMsg globalMsg;

int START_PORT = 9000;

Application::Application(size_t idx) {
  this->idx_ = idx;
  this->isServer = idx == 0;

  int clientPort = START_PORT + 1 + idx;
  sockaddr_in clientAddress = createAddress("127.0.0.1", clientPort);
  sockaddr_in serverAddress = createAddress("127.0.0.1", START_PORT);
  this->kv = new DistributedKDStore(clientAddress, serverAddress, idx);

  // start server
  if (isServer) {
    s = new Server(serverAddress);
    s->start();
  }

  // start client
  this->kv->start();
}

Application::~Application() {
  this->kv->stop();
  delete kv;
  if (isServer) {
    this->s->stop();
    delete s;
  }
}

size_t Application::this_node() { return this->idx_; }
