#pragma once
#include "../networking/server.h"
#include "../store/distributedKDStore.h"
#include "../util/object.h"

class Application : public Object {
public:
  // member variables
  DistributedKDStore *kv;
  size_t idx_;

  bool isServer;
  Server *s;

  // an application with idx 0 will be the server
  // all applications will act as clients
  Application(size_t idx);

  ~Application();

  // methods
  size_t this_node();

  virtual void run_() = 0;
};
