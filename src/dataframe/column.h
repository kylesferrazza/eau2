#pragma once

#include "columns/columnUtil.h"

#include "../util/serializable.h"
#include "../util/string.h"

class IntColumn;
class BoolColumn;
class DoubleColumn;
class StringColumn;

/**************************************************************************
 * Column ::
 * Represents one column of a data frame which holds values of a single type.
 * This abstract class defines methods overriden in subclasses. There is
 * one subclass per element type. Columns are mutable, equality is pointer
 * equality. */
class Column : public Serializable {
public:
  /** Type converters: Return same column under its actual type, or
   *  nullptr if of the wrong type.  */
  virtual IntColumn *as_int();

  virtual BoolColumn *as_bool();

  virtual DoubleColumn *as_double();

  virtual StringColumn *as_string();

  /** Type appropriate push_back methods. Calling the wrong method is
   * undefined behavior. **/
  virtual void push_back(int val);

  virtual void push_back(bool val);

  virtual void push_back(double val);

  virtual void push_back(String *val);

  /** Returns the number of elements in the column. */
  virtual size_t size() = 0;

  /** Return the type of this column as a char: 'S', 'B', 'I' and 'D'. */
  char get_type();

  virtual char my_type() = 0;

  virtual char *serialize() = 0;

  virtual bool equals(Object *other) = 0;

  virtual Column *clone() override = 0;
};
