#include "bool.h"

#include <cstdarg>

BoolStorage::BoolStorage() {
  size_ = 0;
  numBlocks_ = STORAGE_SIZE;
  storage_ = new bool *[numBlocks_];
  for (int i = 0; i < numBlocks_; i++) {
    storage_[i] = new bool[BLOCK_SIZE];
  }
}

BoolStorage::~BoolStorage() {
  for (int i = 0; i < numBlocks_; i++) {
    delete[] storage_[i];
  }
  delete[] storage_;
}

bool BoolStorage::get(size_t i) {
  assert(i < size_);

  int blockNum = (int)i / 50;
  int idx = (int)i % 50;

  assert(blockNum < numBlocks_);
  assert(idx < BLOCK_SIZE);

  return storage_[blockNum][idx];
}

void BoolStorage::set(size_t i, bool val) {
  assert(i < size_);

  int blockNum = (int)i / 50;
  int idx = (int)i % 50;

  assert(blockNum < numBlocks_);
  assert(idx < BLOCK_SIZE);

  storage_[blockNum][idx] = val;
}

void BoolStorage::grow() {
  size_t newSize = growth(this->numBlocks_);
  bool **newStorage = new bool *[newSize];
  for (int i = 0; i < numBlocks_; i++) {
    newStorage[i] = storage_[i];
  }
  for (int i = numBlocks_; i < newSize; i++) {
    newStorage[i] = new bool[BLOCK_SIZE];
  }
  this->numBlocks_ = newSize;
  delete[] storage_;
  this->storage_ = newStorage;
}

int BoolStorage::capacity() { return this->numBlocks_ * BLOCK_SIZE; }

void BoolStorage::push_back(bool val) {
  assert(size_ <= capacity());
  if (size_ == capacity()) {
    grow();
  }
  assert(size_ < capacity());
  this->size_ += 1;
  this->set(size_ - 1, val);
}

size_t BoolStorage::size() { return this->size_; }

bool BoolStorage::equals(Object *other) {
  BoolStorage *that = dynamic_cast<BoolStorage *>(other);

  // ensure that given object is a BoolStorage
  if (that == nullptr) {
    return false;
  }
  if (this->size_ != that->size_)
    return false;
  for (int i = 0; i < size_; i++) {
    if (this->get(i) != that->get(i)) {
      return false;
    }
  }
  return true;
}

char *BoolStorage::serialize() {
  size_t sizeSize = sizeof(size_t);
  size_t boolSize = sizeof(bool);
  size_t endSize = sizeSize                    // size
		   + (boolSize * this->size()) // all of the bools
      ;

  char *ret = new char[endSize];
  serializedSoFar_ = 0;

  // serialize the size of the int storage
  this->cpySerialize(ret, &this->size_, sizeSize);

  // serialize each int element
  for (size_t i = 0; i < this->size_; i++) {
    bool current = this->get(i);
    this->cpySerialize(ret, &current, boolSize);
  }

  return ret;
}

BoolStorage *BoolStorage::deserialize(char *buf) {
  BoolStorage *ret = new BoolStorage();
  ret->deserializedSoFar_ = 0;

  size_t sizeSize = sizeof(size_t);
  size_t boolSize = sizeof(bool);

  size_t size = 0;
  ret->cpyDeserialize(&size, buf, sizeSize);

  for (size_t i = 0; i < size; i++) {
    bool current = false;
    ret->cpyDeserialize(&current, buf, boolSize);

    ret->push_back(current);
  }

  return ret;
}

BoolColumn::BoolColumn() : Column() { storage_ = new BoolStorage(); }
BoolColumn::BoolColumn(int n, ...) : BoolColumn() {
    va_list vaList;
    va_start(vaList, n);
    for (int i = 0; i < n; i++) {
      // va_arg promotes bool to int
      bool val = va_arg(vaList, int);
      this->push_back(val);
    }
    va_end(vaList);
  }

BoolColumn::~BoolColumn() { delete storage_; }

bool BoolColumn::get(size_t idx) { return this->storage_->get(idx); }

BoolColumn *BoolColumn::as_bool() { return this; }

void BoolColumn::set(size_t idx, bool val) { this->storage_->set(idx, val); }

void BoolColumn::push_back(bool val) { this->storage_->push_back(val); }

char BoolColumn::my_type() { return 'B'; }

size_t BoolColumn::size() { return this->storage_->size(); }

bool BoolColumn::equals(Object *o) {
  BoolColumn *casted_col = dynamic_cast<BoolColumn *>(o);

  // ensure that given object is an BoolColumn
  if (casted_col == nullptr) {
    return false;
  }

  return this->storage_->equals(casted_col->storage_);
}

char *BoolColumn::serialize() {
  char *ret = this->storage_->serialize();
  this->serializedSoFar_ = this->storage_->numSerializedBytes();
  return ret;
}

BoolColumn *BoolColumn::deserialize(char *buf) {
  BoolStorage *storage = BoolStorage::deserialize(buf);

  BoolColumn *ret = new BoolColumn();
  delete ret->storage_;
  ret->storage_ = storage;

  return ret;
}

BoolColumn *BoolColumn::clone() {
  BoolColumn *nbc = new BoolColumn();
  for (size_t i = 0; i < this->size(); i++) {
    nbc->push_back(this->storage_->get(i));
  }
  return nbc;
}
