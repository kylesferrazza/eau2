#pragma once

#include "../column.h"
#include "../../util/serializable.h"

class BoolStorage : public Serializable {
public:
  bool **storage_;
  size_t size_;
  size_t numBlocks_;

  BoolStorage();

  ~BoolStorage();

  bool get(size_t i);

  void set(size_t i, bool val);

  void grow();

  int capacity();

  void push_back(bool val);

  size_t size();

  /** Equality comparison for boolStorage based on the stored bools */
  bool equals(Object *other);

  //-----------------serialization---------------
  virtual char *serialize();

  static BoolStorage *deserialize(char *buf);
};

/*************************************************************************
 * BoolColumn::
 * Holds primitive int values, unwrapped.
 */
class BoolColumn : public Column {
public:
  BoolStorage *storage_;
  BoolColumn();
  BoolColumn(int n, ...);

  ~BoolColumn() override;

  bool get(size_t idx);

  BoolColumn *as_bool() override;

  /** Set value at idx. An out of bound idx is undefined.  */
  void set(size_t idx, bool val);

  void push_back(bool val) override;

  char my_type() override;

  size_t size() override;

  /* determines equality based on the boolStorage of each column */
  bool equals(Object *o) override;

  //----------------Serialization-------------
  virtual char *serialize() override;

  static BoolColumn *deserialize(char *buf);

  virtual BoolColumn *clone() override;
};
