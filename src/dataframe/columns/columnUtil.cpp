#include "columnUtil.h"

#include <cstddef>

size_t growth(size_t numBlocks) { return numBlocks * 2; }
