#pragma once

#include <cstddef>

// The size of an allocated block.
const int STORAGE_SIZE = 2;

// The size of an allocated block.
const int BLOCK_SIZE = 50;

// The number of threads to start when pmapping
const size_t NUM_THREADS = 4;

size_t growth(size_t numBlocks);
