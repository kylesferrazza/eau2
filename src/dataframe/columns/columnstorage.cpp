#include "columnstorage.h"

#include "../column.h"
#include "../../util/serializable.h"

#include "bool.h"
#include "int.h"
#include "double.h"
#include "string.h"

ColumnStorage::ColumnStorage() : ObjectStorage() {}

ColumnStorage::ColumnStorage(ColumnStorage *from) : ObjectStorage(from) {}

Column *ColumnStorage::get(size_t i) {
  Object *obj = ObjectStorage::get(i);
  Column *col = dynamic_cast<Column *>(obj);
  return col;
}

void ColumnStorage::set(size_t i, Column *val) { ObjectStorage::set(i, val); }

char *ColumnStorage::serialize() {
  size_t sizeSize = sizeof(size_t);
  size_t charSize = sizeof(char);

  char **serializedColumns = new char *[this->size_]; // serialized data
  size_t *columnSizes =
      new size_t[this->size_];         // number of serialized bytes per column
  char *types = new char[this->size_]; // column type

  // serialize each of the columns
  for (size_t i = 0; i < this->size_; i++) {
    Column *current = this->get(i);
    char type = current->get_type();
    size_t serializedBytes = 0;
    char *serialized;
    if (type == 'B') {
      BoolColumn *bc = current->as_bool();
      serialized = bc->serialize();
      serializedBytes = bc->numSerializedBytes();
    } else if (type == 'D') {
      DoubleColumn *dc = current->as_double();
      serialized = dc->serialize();
      serializedBytes = dc->numSerializedBytes();
    } else if (type == 'I') {
      IntColumn *ic = current->as_int();
      serialized = ic->serialize();
      serializedBytes = ic->numSerializedBytes();
    } else if (type == 'S') {
      StringColumn *sc = current->as_string();
      serialized = sc->serialize();
      serializedBytes = sc->numSerializedBytes();
    } else {
      assert(false);
    }
    serializedColumns[i] = serialized;
    columnSizes[i] = serializedBytes;
    types[i] = type;
  }

  // calculate the end size
  size_t endSize = sizeSize; // number of columns
  // for every column
  for (size_t i = 0; i < this->size_; i++) {
    endSize += charSize;       // column type
    endSize += sizeSize;       // size_t to tell how many bytes that column is
    endSize += columnSizes[i]; // number of serialized bytes for that column
  }

  // create return buffer of size endSize
  char *ret = new char[endSize];
  this->serializedSoFar_ = 0;

  // start putting serialized data in return buffer
  this->cpySerialize(ret, &this->size_,
		     sizeSize); // start with total number of columns

  // for every column
  for (size_t i = 0; i < this->size(); i++) {
    this->cpySerialize(ret, &types[i], charSize); // column type

    this->cpySerialize(ret, &columnSizes[i],
		       sizeSize); // column bytes

    for (size_t j = 0; j < columnSizes[i]; j++) {
      this->cpySerialize(ret, &serializedColumns[i][j], charSize);
    }
  }

  for (size_t i = 0; i < this->size_; i++) {
    delete[] serializedColumns[i];
  }
  delete[] serializedColumns;

  delete[] types;
  delete[] columnSizes;

  return ret;
}

ColumnStorage *ColumnStorage::deserialize(char *buf) {
  ColumnStorage *ret = new ColumnStorage();
  ret->deserializedSoFar_ = 0;

  size_t sizeSize = sizeof(size_t);
  size_t charSize = sizeof(bool);

  size_t numColumns = 0;
  ret->cpyDeserialize(&numColumns, buf, sizeSize);

  char colType = '0';
  size_t colBytes = 0;

  for (size_t i = 0; i < numColumns; i++) {
    ret->cpyDeserialize(&colType, buf,
			charSize); // store the type of the current columns

    ret->cpyDeserialize(&colBytes, buf,
			sizeSize); // store the number of serialized bytes for
				   // the current column

    char *columnBuf = new char[colBytes];

    ret->cpyDeserialize(columnBuf, buf, colBytes);

    if (colType == 'B') {
      BoolColumn *current = BoolColumn::deserialize(columnBuf);
      ret->push_back(current);
    } else if (colType == 'D') {
      DoubleColumn *current = DoubleColumn::deserialize(columnBuf);
      ret->push_back(current);
    } else if (colType == 'I') {
      IntColumn *current = IntColumn::deserialize(columnBuf);
      ret->push_back(current);
    } else if (colType == 'S') {
      StringColumn *current = StringColumn::deserialize(columnBuf);
      ret->push_back(current);
    } else {
      assert(false);
    }

    delete[] columnBuf;
  }

  return ret;
}
