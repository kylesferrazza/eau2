#pragma once

#include <cstddef>
#include "objectstorage.h"
#include "../column.h"

/*************************************************************************
 * ColumnStorage::
 * Implementation of ObjectStorage that specifically stores Columns
 * for the dataframe to hold
 */
class ColumnStorage : public ObjectStorage {
public:
  ColumnStorage();

  ColumnStorage(ColumnStorage *from);

  Column *get(size_t i) override;

  // needs to be overridden to only allow strings to be added
  void set(size_t i, Column *val);

  //----------------Serialization------------------

  virtual char *serialize() override;

  static ColumnStorage *deserialize(char *buf);
};
