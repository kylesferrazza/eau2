#include "double.h"

#include "columnUtil.h"

#include <cstdarg>

DoubleStorage::DoubleStorage() {
  size_ = 0;
  numBlocks_ = STORAGE_SIZE;
  storage_ = new double *[numBlocks_];
  for (int i = 0; i < numBlocks_; i++) {
    storage_[i] = new double[BLOCK_SIZE];
  }
}

DoubleStorage::~DoubleStorage() {
  for (int i = 0; i < numBlocks_; i++) {
    delete[] storage_[i];
  }
  delete[] storage_;
}

double DoubleStorage::get(size_t i) {
  assert(i < size_);

  int blockNum = (int)i / 50;
  int idx = (int)i % 50;

  assert(blockNum < numBlocks_);
  assert(idx < BLOCK_SIZE);

  return storage_[blockNum][idx];
}

void DoubleStorage::set(size_t i, double val) {
  assert(i < size_);

  int blockNum = (int)i / 50;
  int idx = (int)i % 50;

  assert(blockNum < numBlocks_);
  assert(idx < BLOCK_SIZE);

  storage_[blockNum][idx] = val;
}

void DoubleStorage::grow() {
  size_t newSize = growth(this->numBlocks_);
  double **newStorage = new double *[newSize];
  for (int i = 0; i < numBlocks_; i++) {
    newStorage[i] = storage_[i];
  }
  for (int i = numBlocks_; i < newSize; i++) {
    newStorage[i] = new double[BLOCK_SIZE];
  }
  this->numBlocks_ = newSize;
  delete[] storage_;
  this->storage_ = newStorage;
}

int DoubleStorage::capacity() { return this->numBlocks_ * BLOCK_SIZE; }

void DoubleStorage::push_back(double val) {
  assert(size_ <= capacity());
  if (size_ == capacity()) {
    grow();
  }
  assert(size_ < capacity());
  this->size_ += 1;
  this->set(size_ - 1, val);
}

size_t DoubleStorage::size() { return this->size_; }

bool DoubleStorage::equals(Object *other) {
  DoubleStorage *that = dynamic_cast<DoubleStorage *>(other);

  // ensure that given object is a DoubleStorage
  if (that == nullptr) {
    return false;
  }
  if (this->size_ != that->size_)
    return false;
  for (int i = 0; i < size_; i++) {
    if (this->get(i) != that->get(i)) {
      return false;
    }
  }
  return true;
}

char *DoubleStorage::serialize() {
  size_t sizeSize = sizeof(size_t);
  size_t doubleSize = sizeof(double);
  size_t endSize = sizeSize                      // size
		   + (doubleSize * this->size()) // all of the doubles
      ;

  char *ret = new char[endSize];
  serializedSoFar_ = 0;

  // serialize the size of the int storage
  this->cpySerialize(ret, &this->size_, sizeSize);

  // serialize each int element
  for (size_t i = 0; i < this->size_; i++) {
    double current = this->get(i);
    this->cpySerialize(ret, &current, doubleSize);
  }

  return ret;
}

DoubleStorage *DoubleStorage::deserialize(char *buf) {
  DoubleStorage *ret = new DoubleStorage();
  ret->deserializedSoFar_ = 0;

  size_t sizeSize = sizeof(size_t);
  size_t doubleSize = sizeof(double);

  size_t size = 0;
  ret->cpyDeserialize(&size, buf, sizeSize);

  for (size_t i = 0; i < size; i++) {
    double current = 0.0;
    ret->cpyDeserialize(&current, buf, doubleSize);

    ret->push_back(current);
  }

  return ret;
}

DoubleColumn::DoubleColumn() : Column() { storage_ = new DoubleStorage(); }

DoubleColumn::DoubleColumn(int n, ...) : DoubleColumn() {
  va_list vaList;
  va_start(vaList, n);
  for (int i = 0; i < n; i++) {
    double val = va_arg(vaList, double);
    this->push_back(val);
  }
  va_end(vaList);
}

DoubleColumn::~DoubleColumn() { delete storage_; }

double DoubleColumn::get(size_t idx) { return this->storage_->get(idx); }

DoubleColumn *DoubleColumn::as_double() { return this; }

void DoubleColumn::set(size_t idx, double val) { this->storage_->set(idx, val); }

void DoubleColumn::push_back(double val) { this->storage_->push_back(val); }

char DoubleColumn::my_type() { return 'D'; }

size_t DoubleColumn::size() { return this->storage_->size(); }

bool DoubleColumn::equals(Object *o) {
  DoubleColumn *casted_col = dynamic_cast<DoubleColumn *>(o);

  // ensure that given object is an DoubleColumn
  if (casted_col == nullptr) {
    return false;
  }

  return this->storage_->equals(casted_col->storage_);
}

char *DoubleColumn::serialize() {
  char *ret = this->storage_->serialize();
  this->serializedSoFar_ = this->storage_->numSerializedBytes();
  return ret;
}

DoubleColumn *DoubleColumn::deserialize(char *buf) {
  DoubleStorage *storage = DoubleStorage::deserialize(buf);

  DoubleColumn *ret = new DoubleColumn();
  delete ret->storage_;
  ret->storage_ = storage;

  return ret;
}

DoubleColumn *DoubleColumn::clone() {
  DoubleColumn *ndc = new DoubleColumn();
  for (size_t i = 0; i < this->size(); i++) {
    ndc->push_back(this->storage_->get(i));
  }
  return ndc;
}
