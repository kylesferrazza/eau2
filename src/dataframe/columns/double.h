#pragma once

#include "../../util/serializable.h"
#include "../column.h"

class DoubleStorage : public Serializable {
public:
  double **storage_;
  size_t size_;
  size_t numBlocks_;

  DoubleStorage();

  ~DoubleStorage();

  double get(size_t i);

  void set(size_t i, double val);

  void grow();

  int capacity();

  void push_back(double val);

  size_t size();

  /* Equality comparison based on each corresponding double in storage */
  bool equals(Object *other) override;

  //-----------------serialization---------------
  virtual char *serialize() override;

  static DoubleStorage *deserialize(char *buf);
};

/*************************************************************************
 * IntColumn::
 * Holds primitive int values, unwrapped.
 */
class DoubleColumn : public Column {
public:
  DoubleStorage *storage_;
  DoubleColumn();
  DoubleColumn(int n, ...);

  ~DoubleColumn() override;

  double get(size_t idx);

  DoubleColumn *as_double() override;

  /** Set value at idx. An out of bound idx is undefined.  */
  void set(size_t idx, double val);

  void push_back(double val) override;

  char my_type() override;

  size_t size() override;

  /* Equality comparison based on equality of double storage */
  bool equals(Object *o) override;

  //----------------Serialization-------------
  virtual char *serialize() override;

  static DoubleColumn *deserialize(char *buf);

  virtual DoubleColumn *clone() override;
};
