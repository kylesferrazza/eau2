#include "int.h"

#include <cassert>
#include <cstdarg>

IntStorage::IntStorage() {
  size_ = 0;
  numBlocks_ = STORAGE_SIZE;
  storage_ = new int *[numBlocks_];
  for (int i = 0; i < numBlocks_; i++) {
    storage_[i] = new int[BLOCK_SIZE];
  }
}

IntStorage::~IntStorage() {
  for (int i = 0; i < numBlocks_; i++) {
    delete[] storage_[i];
  }
  delete[] storage_;
}

int IntStorage::get(size_t i) {
  assert(i < size_);

  int blockNum = (int)i / 50;
  int idx = (int)i % 50;

  assert(blockNum < numBlocks_);
  assert(idx < BLOCK_SIZE);

  return storage_[blockNum][idx];
}

void IntStorage::set(size_t i, int val) {
  assert(i < size_);

  int blockNum = (int)i / 50;
  int idx = (int)i % 50;

  assert(blockNum < numBlocks_);
  assert(idx < BLOCK_SIZE);

  storage_[blockNum][idx] = val;
}

void IntStorage::grow() {
  size_t newSize = growth(this->numBlocks_);
  int **newStorage = new int *[newSize];
  for (int i = 0; i < numBlocks_; i++) {
    newStorage[i] = storage_[i];
  }
  for (int i = numBlocks_; i < newSize; i++) {
    newStorage[i] = new int[BLOCK_SIZE];
  }
  this->numBlocks_ = newSize;
  delete[] storage_;
  this->storage_ = newStorage;
}

int IntStorage::capacity() { return this->numBlocks_ * BLOCK_SIZE; }

void IntStorage::push_back(int val) {
  assert(size_ <= capacity());
  if (size_ == capacity()) {
    grow();
  }
  assert(size_ < capacity());
  this->size_ += 1;
  this->set(size_ - 1, val);
}

size_t IntStorage::size() { return this->size_; }

bool IntStorage::equals(Object *other) {
  IntStorage *that = dynamic_cast<IntStorage *>(other);

  // ensure that given object is a IntStorage
  if (that == nullptr) {
    return false;
  }
  if (this->size_ != that->size_)
    return false;
  for (int i = 0; i < size_; i++) {
    if (this->get(i) != that->get(i)) {
      return false;
    }
  }
  return true;
}

char *IntStorage::serialize() {
  size_t sizeSize = sizeof(size_t);
  size_t intSize = sizeof(int);
  size_t endSize = sizeSize                   // size
		   + (intSize * this->size()) // all of the ints
      ;

  char *ret = new char[endSize];
  serializedSoFar_ = 0;

  // serialize the size of the int storage
  this->cpySerialize(ret, &this->size_, sizeSize);

  // serialize each int element
  for (size_t i = 0; i < this->size_; i++) {
    int current = this->get(i);
    this->cpySerialize(ret, &current, intSize);
  }

  return ret;
}

IntStorage *IntStorage::deserialize(char *buf) {
  IntStorage *ret = new IntStorage();
  ret->deserializedSoFar_ = 0;

  size_t sizeSize = sizeof(size_t);
  size_t intSize = sizeof(int);

  size_t size = 0;
  ret->cpyDeserialize(&size, buf, sizeSize);

  for (size_t i = 0; i < size; i++) {
    int current = 0;
    ret->cpyDeserialize(&current, buf, intSize);

    ret->push_back(current);
  }

  return ret;
}

IntColumn::IntColumn() : Column() { storage_ = new IntStorage(); }

IntColumn::IntColumn(int n, ...) : IntColumn() {
  va_list vaList;
  va_start(vaList, n);
  for (int i = 0; i < n; i++) {
    int val = va_arg(vaList, int);
    this->push_back(val);
  }
  va_end(vaList);
}

IntColumn::~IntColumn() { delete storage_; }

int IntColumn::get(size_t idx) { return this->storage_->get(idx); }

IntColumn *IntColumn::as_int() { return this; }

void IntColumn::set(size_t idx, int val) { this->storage_->set(idx, val); }

void IntColumn::push_back(int val) { this->storage_->push_back(val); }

char IntColumn::my_type() { return 'I'; }

size_t IntColumn::size() { return this->storage_->size(); }

bool IntColumn::equals(Object *o) {
  IntColumn *casted_col = dynamic_cast<IntColumn *>(o);

  // ensure that given object is an IntColumn
  if (casted_col == nullptr) {
    return false;
  }

  return this->storage_->equals(casted_col->storage_);
}

char *IntColumn::serialize() {
  char *ret = this->storage_->serialize();
  this->serializedSoFar_ = this->storage_->numSerializedBytes();
  return ret;
}

IntColumn *IntColumn::deserialize(char *buf) {
  IntStorage *storage = IntStorage::deserialize(buf);

  IntColumn *ret = new IntColumn();
  delete ret->storage_;
  ret->storage_ = storage;

  return ret;
}

IntColumn *IntColumn::clone() {
  IntColumn *nic = new IntColumn();
  for (size_t i = 0; i < this->size(); i++) {
    nic->push_back(this->storage_->get(i));
  }
  return nic;
}

CharStorage::CharStorage() : IntStorage() {}
CharStorage::CharStorage(CharStorage *from) : CharStorage() {
  size_t fromSize = from->size();
  for (int i = 0; i < fromSize; i++) {
    this->push_back(from->get(i));
  }
  assert(this->size() == fromSize);
}

CharStorage *CharStorage::deserialize(char *buf) {
  IntStorage *is = IntStorage::deserialize(buf);

  return reinterpret_cast<CharStorage *>(is);
}
