#pragma once

#include "../../util/object.h"
#include "../../util/serializable.h"
#include "../column.h"
#include "columnUtil.h"

class IntStorage : public Serializable {
public:
  int **storage_;
  size_t size_;
  size_t numBlocks_;

  IntStorage();

  ~IntStorage();

  int get(size_t i);

  void set(size_t i, int val);

  void grow();

  int capacity();

  void push_back(int val);

  size_t size();

  /* Equality comparison based on each int in storage */
  bool equals(Object *other);

  //-------------serialization-------------
  virtual char *serialize();

  static IntStorage *deserialize(char *buf);
};

/*************************************************************************
 * IntColumn::
 * Holds primitive int values, unwrapped.
 */
class IntColumn : public Column {
public:
  IntStorage *storage_;

  IntColumn();

  IntColumn(int n, ...);

  ~IntColumn() override;

  int get(size_t idx);
  IntColumn *as_int() override;

  /** Set value at idx. An out of bound idx is undefined.  */
  void set(size_t idx, int val);

  void push_back(int val) override;

  char my_type() override;

  size_t size() override;

  /* Equality comparison based on equality of int storage */
  bool equals(Object *o) override;

  //----------------Serialization-------------
  virtual char *serialize() override;

  static IntColumn *deserialize(char *buf);

  virtual IntColumn *clone() override;
};

/*************************************************************************
 * CharStorage::
 * Implementation of IntStorage that specifically stores chars
 * since chars are represented as ints.  Used by schema and row
 */
class CharStorage : public IntStorage {
public:
  CharStorage();
  CharStorage(CharStorage *from);

  static CharStorage *deserialize(char *buf);
};
