#include "objectstorage.h"

#include "../../util/object.h"
#include "../../util/serializable.h"
#include "columnUtil.h"
#include <cassert>

ObjectStorage::ObjectStorage() {
  size_ = 0;
  numBlocks_ = STORAGE_SIZE;
  storage_ = new Object **[numBlocks_];
  for (int i = 0; i < numBlocks_; i++) {
    storage_[i] = new Object *[BLOCK_SIZE];
  }
}

ObjectStorage::ObjectStorage(ObjectStorage *from) : ObjectStorage() {
  int fromSize = from->size();
  for (int i = 0; i < fromSize; i++) {
    Object *current = from->get(i);
    assert(current != nullptr);
    this->push_back(current->clone());
  }
  assert(this->size() == fromSize);
}

ObjectStorage::~ObjectStorage() {
  // TODO: this needs to happen here
  // this->deleteContents();
  for (int i = 0; i < numBlocks_; i++) {
    Object **current = storage_[i];
    delete[] current;
  }
  delete[] storage_;
}

void ObjectStorage::deleteContents() {
  int numToDelete = size_;
  for (int i = 0; i < numBlocks_; i++) {
    Object **block = storage_[i];
    for (int item = 0; item < BLOCK_SIZE; item++) {
      if (numToDelete <= 0)
        return;
      Object *obj = block[item];
      assert(obj != nullptr);
      delete obj;
      numToDelete--;
    }
  }
}

Object *ObjectStorage::get(size_t i) {
  assert (i >= 0);
  assert(i < size_);

  int blockNum = (int)i / 50;
  int idx = (int)i % 50;

  assert(blockNum < numBlocks_);
  assert(idx < BLOCK_SIZE);

  return storage_[blockNum][idx];
}

void ObjectStorage::set(size_t i, Object *val) {
  assert(val != nullptr);
  assert(i >= 0);
  assert(i < size_);

  int blockNum = (int)i / 50;
  int idx = (int)i % 50;

  assert(blockNum < numBlocks_);
  assert(idx < BLOCK_SIZE);

  storage_[blockNum][idx] = val;
}

void ObjectStorage::grow() {
  size_t newSize = growth(this->numBlocks_);
  Object ***newStorage = new Object **[newSize];
  for (int i = 0; i < numBlocks_; i++) {
    newStorage[i] = storage_[i];
  }
  for (int i = numBlocks_; i < newSize; i++) {
    newStorage[i] = new Object *[BLOCK_SIZE];
  }
  this->numBlocks_ = newSize;
  delete[] storage_;
  this->storage_ = newStorage;
}

int ObjectStorage::capacity() { return this->numBlocks_ * BLOCK_SIZE; }

void ObjectStorage::push_back(Object *val) {
  assert(val != nullptr);
  assert(size_ <= capacity());
  if (size_ == capacity()) {
    grow();
  }
  assert(size_ < capacity());
  this->size_ += 1;
  this->set(size_ - 1, val);
}

size_t ObjectStorage::size() { return this->size_; }

/** determines equality by calling each Object's equals method
 *  on the corresponding objects of the other storage */
bool ObjectStorage::equals(Object *other) {
  ObjectStorage *that = dynamic_cast<ObjectStorage *>(other);

  // ensure that given object is an ObjectStorage
  if (that == nullptr) {
    return false;
  }
  if (this->size_ != that->size_)
    return false;
  for (int i = 0; i < size_; i++) {
    Object *mine = this->get(i);
    Object *theirs = that->get(i);
    if (mine != nullptr && theirs != nullptr) {
      if (!mine->equals(theirs))
        return false;
      else
        continue;
    } else {
      return mine == theirs;
    }
  }
  return true;
}
