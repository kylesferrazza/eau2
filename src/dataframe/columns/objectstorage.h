#pragma once

#include "../../util/object.h"
#include "../../util/serializable.h"
#include "columnUtil.h"

/*************************************************************************
 * ObjectStorage::
 * This storage class is a way to store a list of objects without
 * ever copying any data.  It does this by holding pointers to blocks
 * of objects.
 */
class ObjectStorage : public Serializable {
public:
  Object ***storage_;
  size_t size_;
  size_t numBlocks_;

  ObjectStorage();

  // Copy constructor.
  ObjectStorage(ObjectStorage *from);

  virtual ~ObjectStorage();

  virtual void deleteContents();

  virtual Object *get(size_t i);

  virtual void set(size_t i, Object *val);

  void grow();

  int capacity();

  void push_back(Object *val);

  size_t size();

  /** determines equality by calling each Object's equals method
   *  on the corresponding objects of the other storage */
  bool equals(Object *other) override;
};
