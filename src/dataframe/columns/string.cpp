#include "string.h"

#include <cstdarg>

#include "columnUtil.h"

StringStorage::StringStorage() : ObjectStorage() {}

StringStorage::~StringStorage() { this->deleteContents(); }

StringStorage::StringStorage(StringStorage *from) : ObjectStorage(from) {}

void StringStorage::deleteContents() {
  int numToDelete = size_;
  for (int i = 0; i < numBlocks_; i++) {
    Object **block = storage_[i];
    for (int item = 0; item < BLOCK_SIZE; item++) {
      if (numToDelete <= 0)
        return;
      Object *obj = block[item];
      assert(obj != nullptr);
      String *str = dynamic_cast<String*>(obj);
      assert(str != nullptr);
      delete str;
      numToDelete--;
    }
  }
}

String *StringStorage::get(size_t i) {
  Object *obj = ObjectStorage::get(i);
  String *str = dynamic_cast<String *>(obj);
  return str;
}

void StringStorage::set(size_t i, String *val) {
  delete this->get(i);
  ObjectStorage::set(i, val);
}

int StringStorage::indexOf(const char *string) {
  for (int i = 0; i < size_; i++) {
    String *s = get(i);
    if (s == nullptr)
      continue;
    String *cmp = new String(string);
    bool b = s->equals(cmp);
    delete cmp;
    if (b)
      return i;
  }
  return -1;
}

char *StringStorage::serialize() {
  size_t sizeSize = sizeof(size_t);
  size_t charSize = sizeof(bool);
  size_t endSize = sizeSize; // size

  // add the size of each string to the size of buffer
  for (size_t i = 0; i < this->size(); i++) {
    endSize += sizeSize; // specify length of the string
    String *cur = this->get(i);
    size_t stringSize = cur->size();
    endSize += stringSize * charSize;
  }

  char *ret = new char[endSize];
  serializedSoFar_ = 0;

  // serialize the size of the String storage
  this->cpySerialize(ret, &this->size_, sizeSize);

  // serialize each string element
  for (size_t i = 0; i < this->size_; i++) {
    String *current = this->get(i);

    // put the size of the string
    size_t stringSize = current->size();
    this->cpySerialize(ret, &stringSize, sizeSize);

    // serialize each character of the string
    for (size_t j = 0; j < stringSize; j++) {
      char cur = current->at(j);
      this->cpySerialize(ret, &cur, charSize);
    }
  }

  return ret;
}

StringStorage *StringStorage::deserialize(char *buf) {
  StringStorage *ret = new StringStorage();
  ret->deserializedSoFar_ = 0;

  size_t sizeSize = sizeof(size_t);
  size_t charSize = sizeof(char);

  size_t numStrings = 0;
  ret->cpyDeserialize(&numStrings, buf, sizeSize);

  size_t stringSize = 0;

  for (size_t i = 0; i < numStrings; i++) {
    // get the string size from the buf
    ret->cpyDeserialize(&stringSize, buf, sizeSize);
    char *s = new char[stringSize + 1];
    for (size_t j = 0; j < stringSize; j++) {
      ret->cpyDeserialize(s + j, buf, charSize);
    }

    s[stringSize] = 0; // terminate the string

    String *current = new String(s);

    delete[] s;
    ret->push_back(current);
  }

  return ret;
}

StringColumn::StringColumn() { storage_ = new StringStorage(); }
StringColumn::StringColumn(int n, ...) : StringColumn() {
  va_list vaList;
  va_start(vaList, n);
  for (int i = 0; i < n; i++) {
    String *val = va_arg(vaList, String *);
    this->push_back(val);
  }
  va_end(vaList);
}

StringColumn::~StringColumn() {
  delete storage_;
}

StringColumn *StringColumn::as_string() { return this; }

String *StringColumn::get(size_t idx) { return storage_->get(idx); }

void StringColumn::set(size_t idx, String *val) {
  storage_->set(idx, val->clone());
}

size_t StringColumn::size() { return storage_->size(); }

char StringColumn::my_type() { return 'S'; }

void StringColumn::push_back(String *val) { storage_->push_back(val->clone()); }

bool StringColumn::equals(Object *other) {
  StringColumn *casted_col = dynamic_cast<StringColumn *>(other);

  // ensure that given object is a StringColumn
  if (casted_col == nullptr) {
    return false;
  }

  return this->storage_->equals(casted_col->storage_);
}

char *StringColumn::serialize() {
  char *ret = this->storage_->serialize();
  this->serializedSoFar_ = this->storage_->numSerializedBytes();
  return ret;
}

StringColumn *StringColumn::deserialize(char *buf) {
  StringStorage *storage = StringStorage::deserialize(buf);

  StringColumn *ret = new StringColumn();
  delete ret->storage_;
  ret->storage_ = storage;

  return ret;
}

StringColumn *StringColumn::clone() {
  StringColumn *nsc = new StringColumn();
  for (size_t i = 0; i < this->size(); i++) {
    nsc->push_back(this->storage_->get(i));
  }
  return nsc;
}
