#pragma once

#include "../../util/object.h"
#include "../../util/serializable.h"
#include "../../util/string.h"
#include "../column.h"

#include "columnUtil.h"
#include "objectstorage.h"

class StringStorage : public ObjectStorage {
public:
  StringStorage();

  StringStorage(StringStorage *from);

  ~StringStorage() override;

  void deleteContents() override;

  String *get(size_t i) override;

  void set(size_t i, String *val);

  int indexOf(const char *string);

  //-----------------serialization---------------
  virtual char *serialize() override;

  static StringStorage *deserialize(char *buf);
};

/*************************************************************************
 * StringColumn::
 * Holds string pointers. The strings are external.  Nullptr is a valid
 * value.
 */
class StringColumn : public Column {
public:
  StringStorage *storage_;

  StringColumn();

  StringColumn(int n, ...);

  ~StringColumn() override;

  StringColumn *as_string() override;

  /** Returns the string at idx; undefined on invalid idx.*/
  String *get(size_t idx);

  /** Acquire ownership fo the string.  Out of bound idx is undefined. */
  void set(size_t idx, String *val);

  size_t size() override;

  char my_type() override;

  void push_back(String *val) override;

  /* Equality comparison based on each String in storage */
  bool equals(Object *other) override;

  //----------------Serialization-------------
  virtual char *serialize() override;

  static StringColumn *deserialize(char *buf);

  virtual StringColumn *clone() override;
};
