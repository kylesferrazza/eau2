#include "dataframe.h"

#include "columns/bool.h"
#include "columns/int.h"
#include "columns/string.h"

#include <cmath>

DataFrame::DataFrame(DataFrame &df) {
  // Store schema from given dataframe
  schema_ = new Schema(df.get_schema());

  // Create necesarry columns based on schema
  createColumns_(schema_);
}

DataFrame::DataFrame(Schema &schema) {
  // copy given schema
  schema_ = new Schema(schema);

  // Create necesarry columns based on schema
  createColumns_(schema_);
}

DataFrame::~DataFrame() {
  delete schema_;
  this->columns_->deleteContents();
  delete columns_;
}

Schema &DataFrame::get_schema() { return *schema_; }

void DataFrame::add_column(Column *col) {
  // ensure that column length is the same as the other if there are any
  if (this->ncols() != 0) {
    assert(col->size() == this->nrows());
  }

  // if this is the first column, set num rows to col size
  if (this->ncols() == 0) {
    this->num_rows_ = col->size();
  }

  // add column to schema
  char col_type = col->get_type();
  schema_->add_column(col_type);

  // add column to dataframe column storage
  columns_->push_back(col->clone());
  num_cols_++;
}

int DataFrame::get_int(size_t col, size_t row) {
  // assert that retrieval is in bound of dataframe
  assert(col <= num_cols_);
  assert(row <= num_rows_);

  return columns_->get(col)->as_int()->get(row);
}

bool DataFrame::get_bool(size_t col, size_t row) {
  // assert that retrieval is in bound of dataframe
  assert(col <= num_cols_);
  assert(row <= num_rows_);

  return columns_->get(col)->as_bool()->get(row);
}

double DataFrame::get_double(size_t col, size_t row) {
  // assert that retrieval is in bound of dataframe
  assert(col <= num_cols_);
  assert(row <= num_rows_);

  return columns_->get(col)->as_double()->get(row);
}

String *DataFrame::get_string(size_t col, size_t row) {
  // assert that retrieval is in bound of dataframe
  assert(col <= num_cols_);
  assert(row <= num_rows_);

  return columns_->get(col)->as_string()->get(row);
}

void DataFrame::set(size_t col, size_t row, int val) {
  // assert that col and row are in bounds of dataframe
  assert(col <= num_cols_);
  assert(row <= num_rows_);

  // Cast column to correct column type
  IntColumn *typed_col = columns_->get(col)->as_int();

  typed_col->set(row, val);
}

void DataFrame::set(size_t col, size_t row, bool val) {
  // assert that col and row are in bounds of dataframe
  assert(col <= num_cols_);
  assert(row <= num_rows_);

  // Cast column to correct column type
  BoolColumn *typed_col = columns_->get(col)->as_bool();

  typed_col->set(row, val);
}

void DataFrame::set(size_t col, size_t row, double val) {
  // assert that col and row are in bounds of dataframe
  assert(col <= num_cols_);
  assert(row <= num_rows_);

  // Cast column to correct column type
  DoubleColumn *typed_col = columns_->get(col)->as_double();

  typed_col->set(row, val);
}

void DataFrame::set(size_t col, size_t row, String *val) {
  // assert that col and row are in bounds of dataframe
  assert(col <= num_cols_);
  assert(row <= num_rows_);

  // Cast column to correct column type
  StringColumn *typed_col = columns_->get(col)->as_string();

  typed_col->set(row, val);
}

void DataFrame::fill_row(size_t idx, Row &row) {
  // assert that row number is in bounds of dataframe
  assert(idx < num_rows_);

  // Loop through all columns in the dataset
  for (size_t i = 0; i < num_cols_; i++) {
    char col_type = schema_->col_type(i);
    if (col_type == 'I') {
      row.set(i, columns_->get(i)->as_int()->get(idx));
    } else if (col_type == 'D') {
      row.set(i, columns_->get(i)->as_double()->get(idx));
    } else if (col_type == 'B') {
      row.set(i, columns_->get(i)->as_bool()->get(idx));
    } else if (col_type == 'S') {
      row.set(i, columns_->get(i)->as_string()->get(idx));
    } else {
      assert(false); // this shouldn't happen
    }
  }
}

void DataFrame::add_row(Row &row) {
  // Loop through all columns in the dataset
  for (size_t i = 0; i < num_cols_; i++) {
    char col_type = schema_->col_type(i);
    if (col_type == 'I') {
      columns_->get(i)->push_back(row.get_int(i));
    } else if (col_type == 'D') {
      columns_->get(i)->push_back(row.get_double(i));
    } else if (col_type == 'B') {
      columns_->get(i)->push_back(row.get_bool(i));
    } else if (col_type == 'S') {
      String *fromRow = row.get_string(i);
      columns_->get(i)->push_back(fromRow);
    } else {
      assert(false); // this shouldn't happen
    }
  }

  num_rows_++;
}

size_t DataFrame::nrows() { return num_rows_; }

size_t DataFrame::ncols() { return num_cols_; }

void DataFrame::map(Rower &r) { map_subset(r, 0, this->nrows()); }

void DataFrame::pmap(Rower &r) {
  Thread **threads = new Thread *[NUM_THREADS];
  Rower **rowers = new Rower *[NUM_THREADS];

  // clone rowers
  rowers[0] = &r;
  for (size_t i = 1; i < NUM_THREADS; i++) {
    Rower *clone = r.dup();
    rowers[i] = clone;
  }

  // create threads
  size_t rows_per_thread = (this->nrows() / NUM_THREADS);
  for (size_t i = 0; i < NUM_THREADS; i++) {
    size_t start = i * rows_per_thread;
    size_t finish;

    if (i == NUM_THREADS - 1) {
      finish = this->nrows();
    } else {
      finish = (i + 1) * rows_per_thread;
    }

    RowerThread *thread = new RowerThread(this, *rowers[i], start, finish);
    threads[i] = thread;
  }

  // start each thread
  for (size_t i = 0; i < NUM_THREADS; i++) {
    threads[i]->start();
  }

  // join each thread
  for (size_t i = 0; i < NUM_THREADS; i++) {
    threads[i]->join();
  }

  // join_delete each
  for (size_t i = NUM_THREADS - 1; i > 0; i--) {
    rowers[i - 1]->join_delete(rowers[i]);
  }

  // delete threads
  for (size_t i = 0; i < NUM_THREADS; i++) {
    delete threads[i];
  }
  delete[] threads;

  // delete rower storage (clones should have been deleted with join_delete)
  delete[] rowers;
}

void DataFrame::map_subset(Rower &r, size_t start_idx, size_t finish_idx) {
  for (size_t i = start_idx; i < finish_idx; i++) {
    // fill a row object with the current dataframe row
    Row *curRow = new Row(this->get_schema());
    curRow->set_idx(i);
    this->fill_row(i, *curRow);
    // It is the rowers job to mutate the values in the dataframe
    // based on Jan's rower example
    r.accept(*curRow);
    delete curRow;
  }
}

DataFrame *DataFrame::filter(Rower &r) {
  // create an empty dataframe with the same schema as this dataframe
  DataFrame *filtered = new DataFrame(*this);

  for (size_t i = 0; i < this->nrows(); i++) {
    // fill a row object with the current dataframe row
    Row *curRow = new Row(this->get_schema());
    curRow->set_idx(i);
    this->fill_row(i, *curRow);

    // Only add row to the filtered dataframe if the rower returns true on
    // that row
    if (r.accept(*curRow)) {
      filtered->add_row(*curRow);
    }

    delete curRow;
  }

  return filtered;
}

void DataFrame::print() {
  // print the sor representation to standard out
  char *str = this->c_str();
  printf("%s", str); // already has a trailing newline
  delete[] str;
}

bool DataFrame::equals(Object *o) {
  DataFrame *df = dynamic_cast<DataFrame *>(o);

  // return false if the given object is not a dataframe
  if (df == nullptr) {
    return false;
  }

  // return false if the schemas are not equal
  if (!this->get_schema().equals(&df->get_schema())) {
    return false;
  }

  if (this->ncols() == df->ncols()) {
    for (size_t i = 0; i < this->ncols(); i++) {
      if (!this->columns_->get(i)->equals(df->columns_->get(i))) {
        return false;
      }
    }
  } else {
    return false; // return false if the number of columns don't match
  }

  return true; // object is equal if all of the conditions pass
}

char *DataFrame::c_str() {
  StrBuff *str_buf = new StrBuff();
  for (size_t row_idx = 0; row_idx < num_rows_; row_idx++) {
    for (size_t col_idx = 0; col_idx < num_cols_; col_idx++) {
      char col_type = schema_->col_type(col_idx);
      if (col_type == 'I') {
        int val = columns_->get(col_idx)->as_int()->get(row_idx);
        int len = sizeof(int) + 2 + 1;
        char buffer[len];
        snprintf(buffer, len, "<%d>", val);
        buffer[len] = 0;
        str_buf->c(buffer);
      } else if (col_type == 'D') {
        double val = columns_->get(col_idx)->as_double()->get(row_idx);
        int digits = 32;
        int len = digits + 2 + 1;
        char buffer[len];
        snprintf(buffer, len, "<%g>", val);
        str_buf->c(buffer);
      } else if (col_type == 'B') {
        bool val = columns_->get(col_idx)->as_bool()->get(row_idx);
        int len = 3 + 1;
        char buffer[len];
        snprintf(buffer, len, "<%d>", val);
        str_buf->c(buffer);
      } else if (col_type == 'S') {
        String *val = columns_->get(col_idx)->as_string()->get(row_idx);
        char *cstr = val->c_str();
        int len = strlen(cstr) + 2 + 1;
        char buffer[len];
        snprintf(buffer, len, "<%s>", cstr);
        str_buf->c(buffer);
      } else {
        assert(false); // this shouldn't happen
      }
    }
    str_buf->c("\n");
  }
  String *sor = str_buf->get();
  delete str_buf;

  char *cpyMe = sor->c_str();
  size_t numChars = strlen(cpyMe);
  char *ret = new char[numChars + 1];
  strncpy(ret, cpyMe, numChars);
  ret[numChars] = 0;
  delete sor;
  return ret;
}

DataFrame *DataFrame::fromScalar(Key &k, DistributedKDStore &s, int scalar) {
  Schema *sch = new Schema("I");
  DataFrame *df = new DataFrame(*sch);
  Row *r = new Row(*sch);

  r->set(0, scalar);

  df->add_row(*r);

  s.put(k, df);

  return df;
}

DataFrame *DataFrame::fromScalar(Key &k, DistributedKDStore &s, double scalar) {
  Schema *sch = new Schema("D");
  DataFrame *df = new DataFrame(*sch);
  Row *r = new Row(*sch);

  r->set(0, scalar);

  df->add_row(*r);

  s.put(k, df);

  return df;
}

DataFrame *DataFrame::fromScalar(Key &k, DistributedKDStore &s, bool scalar) {
  Schema *sch = new Schema("B");
  DataFrame *df = new DataFrame(*sch);
  Row *r = new Row(*sch);

  r->set(0, scalar);

  df->add_row(*r);

  s.put(k, df);

  return df;
}

DataFrame *DataFrame::fromScalar(Key &k, DistributedKDStore &s,
                                 String *scalar) {
  Schema *sch = new Schema("S");
  DataFrame *df = new DataFrame(*sch);
  Row *r = new Row(*sch);

  r->set(0, scalar);

  df->add_row(*r);

  s.put(k, df);

  return df;
}

DataFrame *DataFrame::fromArray(Key *k, DistributedKDStore *s, size_t size,
                                int *arr) {
  Schema sch("I");
  DataFrame *df = new DataFrame(sch);

  for (size_t i = 0; i < size; i++) {
    Row r(sch);
    r.set(0, arr[i]);

    df->add_row(r);
  }

  s->put(*k, df);

  return df;
}

DataFrame *DataFrame::fromArray(Key *k, DistributedKDStore *s, size_t size,
                                double *arr) {
  Schema sch("D");
  DataFrame *df = new DataFrame(sch);

  for (size_t i = 0; i < size; i++) {
    Row r(sch);
    r.set(0, arr[i]);
    df->add_row(r);
  }

  s->put(*k, df);

  return df;
}

DataFrame *DataFrame::fromArray(Key *k, DistributedKDStore *s, size_t size,
                                bool *arr) {
  Schema sch("B");
  DataFrame *df = new DataFrame(sch);

  for (size_t i = 0; i < size; i++) {
    Row r(sch);
    r.set(0, arr[i]);

    df->add_row(r);
  }

  s->put(*k, df);

  return df;
}

DataFrame *DataFrame::fromArray(Key *k, DistributedKDStore *s, size_t size,
                                String **arr) {
  Schema sch("S");
  DataFrame *df = new DataFrame(sch);

  for (size_t i = 0; i < size; i++) {
    Row r(sch);
    r.set(0, arr[i]);

    df->add_row(r);
  }

  s->put(*k, df);

  return df;
}

DataFrame *DataFrame::fromVisitor(Key *key, DistributedKDStore *store,
                                  const char *schema, Writer *w) {
  Schema *sch = new Schema(schema);
  DataFrame *d = new DataFrame(*sch);
  while (!w->done()) {
    Row *r = new Row(*sch);
    w->accept(*r);
    d->add_row(*r);
    delete r;
  }
  delete sch;

  store->put(*key, d);
  return d;
}

//------------------Serialization--------------

// Serialize a dataframe so that it is able to be sent between nodes
char *DataFrame::serialize() {
  // serialize the schema
  char *serSchema = this->schema_->serialize();
  size_t schemaSize = this->schema_->numSerializedBytes();

  // serialize the column storage
  char *serStorage = this->columns_->serialize();
  size_t storageSize = this->columns_->numSerializedBytes();

  size_t sizeSize = sizeof(size_t);
  size_t charSize = sizeof(char);

  size_t endSize = sizeSize      // numCols
                   + sizeSize    // numRows
                   + sizeSize    // num schema bytes
                   + schemaSize  // schema
                   + sizeSize    // num storage bytes
                   + storageSize // column storage
      ;

  char *ret = new char[endSize];
  this->serializedSoFar_ = 0;
  assert(serializedSoFar_ == 0);

  this->cpySerialize(ret, &this->num_cols_, sizeSize);
  this->cpySerialize(ret, &this->num_rows_, sizeSize);

  this->cpySerialize(ret, &schemaSize, sizeSize);
  for (size_t j = 0; j < schemaSize; j++) {
    this->cpySerialize(ret, &serSchema[j], charSize);
  }

  this->cpySerialize(ret, &storageSize, sizeSize);
  for (size_t j = 0; j < storageSize; j++) {
    this->cpySerialize(ret, &serStorage[j], charSize);
  }
  delete[] serStorage;

  delete[] serSchema;

  return ret;
}

DataFrame *DataFrame::deserialize(char *buf) {
  // create dummy schema
  Schema dummy;
  // create return dataframe
  DataFrame *ret = new DataFrame(dummy);

  size_t sizeSize = sizeof(size_t);
  ret->deserializedSoFar_ = 0;

  // deserialize num_cols
  ret->cpyDeserialize(&ret->num_cols_, buf, sizeSize);

  // deserialize num_rows
  ret->cpyDeserialize(&ret->num_rows_, buf, sizeSize);

  // deserialize schema
  size_t schemaSize = 0;
  ret->cpyDeserialize(&schemaSize, buf, sizeSize);
  char *serializedSchema = new char[schemaSize];
  ret->cpyDeserialize(serializedSchema, buf, schemaSize);
  Schema *deserializedSchema = Schema::deserialize(serializedSchema);
  delete[] serializedSchema;

  // deserialize columnStorage
  size_t storageSize = 0;
  ret->cpyDeserialize(&storageSize, buf, sizeSize);

  char *serializedColumns = new char[storageSize];
  ret->cpyDeserialize(serializedColumns, buf, storageSize);
  ColumnStorage *deserializedColumns =
      ColumnStorage::deserialize(serializedColumns);
  delete[] serializedColumns;

  delete ret->schema_;
  delete ret->columns_;

  ret->schema_ = deserializedSchema;
  ret->columns_ = deserializedColumns;

  return ret;
}

//---------Splitting and joining methods--------
std::vector<DataFrame *> DataFrame::split(size_t maxRows) {
  std::vector<DataFrame *> dfs;
  size_t numRows = this->nrows();

  // if the dataframe is empty
  if (numRows == 0) {
    dfs.push_back(this);
    return dfs;
  }

  size_t num_dataframes = numRows / maxRows;
  if (numRows % maxRows != 0) {
    num_dataframes++;
  }

  for (size_t i = 0; i < num_dataframes; i++) {
    DataFrame *current = new DataFrame(*this);

    // fill the current dataframe with maxRows rows from this dataframe
    size_t start = i * maxRows;
    size_t finish = (i + 1) * maxRows;
    for (size_t curRow = start; curRow < finish; curRow++) {
      if (curRow == numRows) {
        break;
      }
      Row *thisRow = new Row(this->get_schema());
      this->fill_row(curRow, *thisRow);
      current->add_row(*thisRow);
      delete thisRow;
    }

    dfs.push_back(current);
  }

  return dfs;
}

DataFrame *DataFrame::join(std::vector<DataFrame *> dfs) {
  assert(dfs.size() > 0);

  DataFrame *master = new DataFrame(dfs.at(0)->get_schema());

  for (size_t i = 0; i < dfs.size(); i++) {
    master->append_(dfs.at(i));
    delete dfs.at(i);
  }

  return master;
}

void DataFrame::append_(DataFrame *toAppend) {
  for (size_t i = 0; i < toAppend->nrows(); i++) {
    Row *curRow = new Row(this->get_schema());
    toAppend->fill_row(i, *curRow);
    this->add_row(*curRow);
    delete curRow;
  }
}

//============ Private helpers =============
// initializes the column storage to number of columns in the schema
void DataFrame::createColumns_(Schema *schema) {
  // Create storage for the number of columns in given schema
  num_cols_ = schema_->width();
  num_rows_ = 0;
  columns_ = new ColumnStorage();

  // Create the columns of the correct type based on the schema
  for (size_t i = 0; i < num_cols_; i++) {
    Column *empty_col;
    char col_type = schema_->col_type(i);
    if (col_type == 'I') {
      empty_col = new IntColumn();
    } else if (col_type == 'D') {
      empty_col = new DoubleColumn();
    } else if (col_type == 'B') {
      empty_col = new BoolColumn();
    } else if (col_type == 'S') {
      empty_col = new StringColumn();
    } else {
      assert(false); // this shouldn't happen
    }
    columns_->push_back(empty_col);
  }
}
