#pragma once

#include <cstddef>

#include "columns/columnstorage.h"

#include "columns/bool.h"
#include "columns/double.h"
#include "columns/int.h"
#include "columns/string.h"

#include "rower.h"
#include "rowerthread.h"

#include "../store/distributedKDStore.h"
#include "../store/key.h"
#include "../store/kvstore.h"
#include "row.h"
#include "schema.h"

#include "./rowers/writer.h"

class KDStore;
class DistributedKDStore;

/****************************************************************************
 * DataFrame::
 *
 * A DataFrame is table composed of columns of equal length. Each column
 * holds values of the same type (I, S, B, F). A dataframe has a schema that
 * describes it.
 */
class DataFrame : public Serializable {
public:
  ColumnStorage *columns_;
  Schema *schema_;
  size_t num_cols_;
  size_t num_rows_;

  /** Create a data frame with the same columns as the given df but with no
   * rows or rownmaes */
  DataFrame(DataFrame &df);

  /** Create a data frame from a schema and columns. All columns are created
   * empty. */
  DataFrame(Schema &schema);

  /** Destructor for dataframe */
  ~DataFrame();

  /** Returns the dataframe's schema. Modifying the schema after a dataframe
   * has been created in undefined. */
  Schema &get_schema();

  /** Adds a column this dataframe, updates the schema, the new column
   * is external, and appears as the last column of the dataframe,
   * A nullptr column is undefined. */
  void add_column(Column *col);

  /** Return the value at the given column and row. Accessing rows or
   *  columns out of bounds, or request the wrong type is undefined.*/
  int get_int(size_t col, size_t row);

  bool get_bool(size_t col, size_t row);

  double get_double(size_t col, size_t row);

  String *get_string(size_t col, size_t row);

  /** Set the value at the given column and row to the given value.
   * If the column is not  of the right type or the indices are out of
   * bound, the result is undefined. */
  void set(size_t col, size_t row, int val);

  void set(size_t col, size_t row, bool val);

  void set(size_t col, size_t row, double val);

  void set(size_t col, size_t row, String *val);

  /** Set the fields of the given row object with values from the columns at
   * the given offset.  If the row is not form the same schema as the
   * dataframe, results are undefined.
   */
  void fill_row(size_t idx, Row &row);

  /** Add a row at the end of this dataframe. The row is expected to have
   *  the right schema and be filled with values, otherwise undedined.  */
  void add_row(Row &row);

  /** The number of rows in the dataframe. */
  size_t nrows();

  /** The number of columns in the dataframe.*/
  size_t ncols();

  /** Visit rows in order */
  void map(Rower &r);

  /** This method clones the Rower and executes the map in parallel. Join is
   * used at the end to merge the results. */
  void pmap(Rower &r);

  // map a subset of the dataframe rows with finish_idx exclusive
  void map_subset(Rower &r, size_t start_idx, size_t finish_idx);

  /** Create a new dataframe, constructed from rows for which the given
   * Rower returned true from its accept method.
   * CALLEE needs to delete. */
  DataFrame *filter(Rower &r);

  /** Print the dataframe in SoR format to standard output. */
  void print();

  /** Determine dataframe equally based on the interal columns and schema*/
  bool equals(Object *o);

  /** Computes a SoR representation of the dataframe utilizing a string
   * buffer.
   */
  char *c_str();

  //------------Store related------------
  static DataFrame *fromScalar(Key &k, DistributedKDStore &s, int scalar);

  static DataFrame *fromScalar(Key &k, DistributedKDStore &s, double scalar);

  static DataFrame *fromScalar(Key &k, DistributedKDStore &s, bool scalar);

  static DataFrame *fromScalar(Key &k, DistributedKDStore &s, String *scalar);

  static DataFrame *fromArray(Key *k, DistributedKDStore *s, size_t size,
                              int *arr);

  static DataFrame *fromArray(Key *k, DistributedKDStore *s, size_t size,
                              double *arr);

  static DataFrame *fromArray(Key *k, DistributedKDStore *s, size_t size,
                              bool *arr);

  static DataFrame *fromArray(Key *k, DistributedKDStore *s, size_t size,
                              String **arr);

  static DataFrame *fromVisitor(Key *key, DistributedKDStore *store,
                                const char *schema, Writer *w);

  //------------------Serialization--------------

  // Serialize a dataframe so that it is able to be sent between nodes
  virtual char *serialize();

  static DataFrame *deserialize(char *buf);

  //---------Splitting and joining methods--------
  // split a dataframe into many smaller dataframes with max rows
  std::vector<DataFrame *> split(size_t maxRows);

  // join smaller dataframes into one large dataframe
  static DataFrame *join(std::vector<DataFrame *>);

  // helper to append one dataframe to this dataframe
  void append_(DataFrame *toAppend);

  //============ Private helpers =============
  // initializes the column storage to number of columns in the schema
  void createColumns_(Schema *schema);
};
