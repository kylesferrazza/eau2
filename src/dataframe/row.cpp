#include "row.h"

#include "columns/bool.h"
#include "columns/double.h"
#include "columns/int.h"
#include "columns/string.h"

Row::Row(Row &row) {
  this->storage_ = new ColumnStorage(row.storage_);
  this->schema_ = new Schema(*row.schema_);
}

Row::Row(Schema &scm) {
  this->storage_ = new ColumnStorage();
  this->schema_ = new Schema(scm);
  int schemaWidth = this->schema_->width();
  for (int i = 0; i < schemaWidth; i++) {
    char typ = this->schema_->col_type(i);
    if (typ == 'I') {
      this->storage_->push_back(new IntColumn(1, 1));
    } else if (typ == 'S') {
      String *mt = new String("");
      this->storage_->push_back(new StringColumn(1, mt));
      delete mt;
    } else if (typ == 'B') {
      this->storage_->push_back(new BoolColumn(1, false));
    } else if (typ == 'D') {
      this->storage_->push_back(new DoubleColumn(1, 0.0));
    } else {
      assert(false);
    }
  }
}

Row::~Row() {
  this->storage_->deleteContents();
  delete this->storage_;
  delete this->schema_;
}

void Row::set(size_t col, int val) {
  char typ = this->col_type(col);
  if (typ != 'I')
    return;
  Column *c = this->storage_->get(col);
  IntColumn *ic = dynamic_cast<IntColumn *>(c);
  assert(ic != nullptr); // we did the typ check above
  ic->set(0, val);
}

void Row::set(size_t col, double val) {
  char typ = this->col_type(col);
  if (typ != 'D')
    return;
  Column *c = this->storage_->get(col);
  DoubleColumn *dc = dynamic_cast<DoubleColumn *>(c);
  assert(dc != nullptr); // we did the typ check above
  dc->set(0, val);
}

void Row::set(size_t col, bool val) {
  char typ = this->col_type(col);
  if (typ != 'B')
    return;
  Column *c = this->storage_->get(col);
  BoolColumn *bc = dynamic_cast<BoolColumn *>(c);
  assert(bc != nullptr); // we did the typ check above
  bc->set(0, val);
}

void Row::set(size_t col, String *val) {
  char typ = this->col_type(col);
  if (typ != 'S')
    return;
  Column *c = this->storage_->get(col);
  StringColumn *sc = dynamic_cast<StringColumn *>(c);
  assert(sc != nullptr); // we did the typ check above
  String *old = sc->get(0);
  assert(old != nullptr);
  sc->set(0, val);
}

void Row::set_idx(size_t idx) { this->idx_ = idx; }

size_t Row::get_idx() { return this->idx_; }

int Row::get_int(size_t col) {
  char typ = this->col_type(col);
  if (typ != 'I')
    return 0;
  Column *c = this->storage_->get(col);
  IntColumn *ic = dynamic_cast<IntColumn *>(c);
  return ic->get(0);
}

bool Row::get_bool(size_t col) {
  char typ = this->col_type(col);
  if (typ != 'B')
    return false;
  Column *c = this->storage_->get(col);
  BoolColumn *bc = dynamic_cast<BoolColumn *>(c);
  return bc->get(0);
}

double Row::get_double(size_t col) {
  char typ = this->col_type(col);
  if (typ != 'D')
    return -1;
  Column *c = this->storage_->get(col);
  DoubleColumn *dc = dynamic_cast<DoubleColumn *>(c);
  return dc->get(0);
}

String *Row::get_string(size_t col) {
  char typ = this->col_type(col);
  if (typ != 'S')
    return nullptr;
  Column *c = this->storage_->get(col);
  StringColumn *sc = dynamic_cast<StringColumn *>(c);
  return sc->get(0);
}

size_t Row::width() { return this->storage_->size(); }

char Row::col_type(size_t idx) {
  Column *c = this->storage_->get(idx);
  if (c == nullptr)
    return ' ';
  return c->get_type();
}

bool Row::equals(Object *o) {
  Row *that = dynamic_cast<Row *>(o);

  // ensure that given object is a Row
  if (that == nullptr) {
    return false;
  }

  return (this->schema_->equals(that->schema_)) &&
         (this->storage_->equals(that->storage_));
}
