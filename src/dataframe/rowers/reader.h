#pragma once

#include "../../util/map.h"
#include "../rower.h"

class Reader : public Rower {
public:
  Reader() {}
};

/****************************************************************************/
class Adder : public Reader {
  public:
    SIMap &map_; // String to Num map;  Num holds an int

    Adder(SIMap &map) : map_(map) {}

    bool accept(Row &r) override {
      String *word = r.get_string(0);
      assert(word != nullptr);
      // TODO: they gave us leaky code...
      Num *num = map_.contains(*word) ? map_.get(*word) : new Num();
      assert(num != nullptr);
      num->v++;
      map_.set(*word, num);
      return false;
    }

    Adder *dup() override {
      return new Adder(map_);
    }

    void join_delete(Rower *other) override {}
};
