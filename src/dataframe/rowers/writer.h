#pragma once

#include "../../util/map.h"
#include "../rower.h"
#include <stdio.h>

class Writer : public Rower {
public:
  Writer() {}

  virtual bool accept(Row &r) = 0;

  virtual bool done() = 0;
};

class FileReader : public Writer {
public:
  char *buf_;
  size_t end_ = 0;
  size_t i_ = 0;
  const char *path;
  FILE *file_;

  /** Reads next word and stores it in the row. Actually read the word.
      While reading the word, we may have to re-fill the buffer  */
  bool accept(Row &r) override {
    assert(i_ < end_);
    assert(!isspace(buf_[i_]));
    size_t wStart = i_;
    while (true) {
      if (i_ == end_) {
        if (feof(file_)) {
          ++i_;
          break;
        }
        i_ = wStart;
        wStart = 0;
        fillBuffer_();
      }
      if (isspace(buf_[i_]))
        break;
      ++i_;
    }
    buf_[i_] = 0;
    String word(buf_ + wStart, i_ - wStart);
    r.set(0, &word);
    ++i_;
    skipWhitespace_();
    return true;
  }

  /** Returns true when there are no more words to read.  There is nothing
     more to read if we are at the end of the buffer and the file has
     all been read.     */
  bool done() override { return (i_ >= end_) && feof(file_); }

  ~FileReader() {
    fclose(this->file_);
    delete[] buf_;
  }

  /** Creates the reader and opens the file for reading.  */
  FileReader(const char *path) {
    this->path = path;
    file_ = fopen(path, "r");
    if (file_ == nullptr) {
      fprintf(stderr, "Cannot open file '%s'\n", path);
      exit(1);
    }
    buf_ = new char[BUFSIZE + 1]; //  null terminator
    fillBuffer_();
    skipWhitespace_();
  }

  static const size_t BUFSIZE = 1024;

  /** Reads more data from the file. */
  void fillBuffer_() {
    size_t start = 0;
    // compact unprocessed stream
    if (i_ != end_) {
      start = end_ - i_;
      memcpy(buf_, buf_ + i_, start);
    }
    // read more contents
    end_ = start + fread(buf_ + start, sizeof(char), BUFSIZE - start, file_);
    i_ = start;
  }

  /** Skips spaces.  Note that this may need to fill the buffer if the
      last character of the buffer is space itself.  */
  void skipWhitespace_() {
    while (true) {
      if (i_ == end_) {
        if (feof(file_))
          return;
        fillBuffer_();
      }
      // if the current character is not whitespace, we are done
      if (!isspace(buf_[i_]))
        return;
      // otherwise skip it
      ++i_;
    }
  }

  void join_delete(Rower *other) override {}

  FileReader *dup() override { return new FileReader(this->path); }
};

/***************************************************************************/
class Summer : public Writer {
public:
  SIMap &map_;
  size_t i = 0;
  size_t j = 0;
  size_t seen = 0;

  Summer(SIMap &map) : map_(map) {
    if (!done()) {
      if (!k()) {
        next();
      }
    }
  }

  void next() {
    assert(!done());
    if (i == map_.capacity_)
      return;
    j++;
    ++seen;
    if (j >= map_.items_[i].keys_.size()) {
      ++i;
      j = 0;
      while (i < map_.capacity_ && map_.items_[i].keys_.size() == 0) {
        i++;
      }
    }
  }

  String *k() {
    if (i == map_.capacity_ || j == map_.items_[i].keys_.size()) {
      return nullptr;
    }
    return (String *)(map_.items_[i].keys_.get_(j));
  }

  size_t v() {
    assert(i != map_.capacity_);
    assert(j != map_.items_[i].keys_.size());
    return ((Num *)(map_.items_[i].vals_.get_(j)))->v;
  }

  bool accept(Row &r) {
    String &key = *k();
    size_t value = v();
    r.set(0, &key);
    r.set(1, (int)value);
    next();
    return true;
  }

  bool done() { return seen == map_.size(); }

  Summer *dup() override { return new Summer(map_); }

  void join_delete(Rower *other) override {}
};
