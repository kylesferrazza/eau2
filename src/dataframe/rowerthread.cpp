#include "rowerthread.h"

RowerThread::RowerThread(DataFrame *df, Rower &r, size_t start_idx,
			 size_t finish_idx)
    : Thread() {
  df_ = df;
  rower_ = &r;
  start_ = start_idx;
  finish_ = finish_idx;
}

void RowerThread::run(std::atomic<bool> &running) {
  df_->map_subset(*rower_, start_, finish_);
}
