#pragma once

#include "rower.h"
#include "dataframe.h"
#include "../util/thread.h"

class DataFrame;

class RowerThread : public Thread {
public:
  DataFrame *df_;
  Rower *rower_;
  size_t start_;
  size_t finish_;

  /** A Thread wraps the thread operations in the standard library.
   *  author: vitekj@me.com */
  RowerThread(DataFrame *df, Rower &r, size_t start_idx, size_t finish_idx);

  /** Subclass responsibility, the body of the run method */
  void run(std::atomic<bool> &running);
};
