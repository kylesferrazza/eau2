#include "schema.h"

Schema::Schema(Schema &from) { colTypes_ = new CharStorage(from.colTypes_); }

Schema::Schema() { colTypes_ = new CharStorage(); }

Schema::~Schema() { delete colTypes_; }

Schema::Schema(const char *types) {
  int numCols = strlen(types);
  colTypes_ = new CharStorage();
  for (int i = 0; i < numCols; i++) {
    char current = types[i];
    this->add_column(current);
  }
}

void Schema::add_column(char typ) { colTypes_->push_back(typ); }

char Schema::col_type(size_t idx) { return colTypes_->get(idx); }

size_t Schema::width() { return colTypes_->size(); }

bool Schema::sameSchema(Schema *that) {
  return this->colTypes_->equals(that->colTypes_);
}

bool Schema::equals(Object *other) {
  if (other == this)
    return true;
  Schema *otherSchema = dynamic_cast<Schema *>(other);
  if (other == nullptr)
    return false;
  return sameSchema(otherSchema);
}

char *Schema::serialize() {
  char *ret = this->colTypes_->serialize();
  this->serializedSoFar_ = this->colTypes_->serializedSoFar_;
  return ret;
}

Schema *Schema::deserialize(char *buf) {
  Schema *sch = new Schema();
  delete sch->colTypes_;
  CharStorage *cols = CharStorage::deserialize(buf);
  sch->colTypes_ = cols;

  return sch;
}
