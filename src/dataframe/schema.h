#pragma once

#include "../util/object.h"
#include "../util/serializable.h"
#include "columns/int.h"

/*************************************************************************
 * Schema::
 * A schema is a description of the contents of a data frame, the schema
 * knows the number of columns and number of rows, the type of each column.
 * The valid types are represented by the chars 'S', 'B', 'I' and 'D'.
 */
class Schema : public Serializable {
public:
  CharStorage *colTypes_;

  /** Copying constructor */
  Schema(Schema &from);

  /** Create an empty schema **/
  Schema();

  ~Schema();

  /** Create a schema from a string of types. A string that contains
   * characters other than those identifying the four type results in
   * undefined behavior. The argument is external, a nullptr argument is
   * undefined. **/
  Schema(const char *types);

  /** Add a column of the given type and name (can be nullptr), name
   * is external. Names are expectd to be unique, duplicates result
   * in undefined behavior. */
  void add_column(char typ);

  /** Return type of column at idx. An idx >= width is undefined. */
  char col_type(size_t idx);

  /** The number of columns */
  size_t width();

  bool sameSchema(Schema *that);

  /** Equality method for Schema that compares the column types,
   * column names. and row names */
  bool equals(Object *other) override;

  //--------------Serialization------------
  virtual char *serialize() override;

  static Schema *deserialize(char *buf);
};
