#include "print.h"

#include <cstdio>

void clientDebug(int sport, int cport, const char *x, bool debug) {
  if (debug)
    fprintf(stderr, "[\033[0;35mCLIENT (%d) (%d)\033[0m] %s\n", sport, cport,
            x);
}

void clientDebug(const char *x, bool debug) {
  if (debug)
    fprintf(stderr, "[\033[0;35mCLIENT\033[0m] %s\n", x);
}

void serverDebug(const char *x, bool debug) {
  if (debug)
    fprintf(stderr, "[\033[0;32mSERVER\033[0m] %s\n", x);
}
