#include "client.h"
#include "clientthread.h"
#include "network.h"
#include <unistd.h>

#include "../debug/print.h"

Client::Client(struct sockaddr_in clientAddress,
               struct sockaddr_in serverAddress, size_t nodeIdx) {
  this->clientAddress_ = clientAddress;
  this->serverAddress_ = serverAddress;
  this->nodeIdx_ = nodeIdx;
  this->serverSockFd_ = 0;
}

Client::Client(struct sockaddr_in clientAddress,
               struct sockaddr_in serverAddress, size_t nodeIdx, bool debug) {
  this->clientAddress_ = clientAddress;
  this->serverAddress_ = serverAddress;
  this->nodeIdx_ = nodeIdx;
  this->serverSockFd_ = 0;
  this->debug_ = debug;
}

Client::~Client() {}

void Client::start() {
  this->running_ = true;
  listenForMessages_();

  sendRegistrationMessage_();
}

void Client::stop() {
  // create an unregister message
  Unregister *unreg =
      new Unregister(this->nodeIdx_, 0, 0, this->clientAddress_);
  char *serializedMessage = unreg->serialize();
  size_t numBytes = unreg->serializedSoFar_;

  // send unregister to server
  sendToServer_(serializedMessage, numBytes);
  delete[] serializedMessage;
  delete unreg;

  // clear peer list
  this->peerAddrs_.clear();
  this->peerIdxs_.clear();

  stopListening_();

  this->running_ = false;
  clientDebug(this->serverAddress_.sin_port, this->clientAddress_.sin_port,
              "SHUTTING DOWN", this->debug_);
}

std::vector<size_t> Client::peerList() { return this->peerIdxs_; }

size_t Client::getNumPeers() { return this->peerIdxs_.size(); }

bool Client::isRunning() { return this->running_; }

void Client::sendData(size_t peerIndex, Data *data) {
  char *serializedMsg = data->serialize();
  size_t numBytes = data->serializedSoFar_;

  sendToPeer(peerIndex, serializedMsg, numBytes);

  delete[] serializedMsg;
}

void Client::sendPut(size_t peerIndex, Put *put) {
  char *serializedMsg = put->serialize();
  size_t numBytes = put->serializedSoFar_;

  while (!this->hasPeer(peerIndex)) {
    // wait until this peer has connected
    usleep(100);
  }

  sendToPeer(peerIndex, serializedMsg, numBytes);

  delete[] serializedMsg;
}

void Client::sendGet(size_t peerIndex, Get *get) {
  char *serializedMsg = get->serialize();
  size_t numBytes = get->serializedSoFar_;

  while (!this->hasPeer(peerIndex)) {
    // wait until this peer has connected
    usleep(100);
  }

  sendToPeer(peerIndex, serializedMsg, numBytes);

  delete[] serializedMsg;
}

void Client::sendWaitAndGet(size_t peerIndex, WaitAndGet *wag) {
  char *serializedMsg = wag->serialize();
  size_t numBytes = wag->serializedSoFar_;

  while (!this->hasPeer(peerIndex)) {
    // wait until this peer has connected
    usleep(100);
  }

  sendToPeer(peerIndex, serializedMsg, numBytes);

  delete[] serializedMsg;
}

void Client::sendReply(size_t peerIndex, Reply *reply) {
  char *serializedMsg = reply->serialize();
  size_t numBytes = reply->serializedSoFar_;

  sendToPeer(peerIndex, serializedMsg, numBytes);

  delete[] serializedMsg;
}

bool Client::hasPeer(size_t peerIndex) {
  for (size_t i = 0; i < this->getNumPeers(); i++) {
    if (this->peerIdxs_.at(i) == peerIndex) {
      return true;
    }
  }
  return false;
}

void Client::connectToServer_() {
  connectToDest(this->serverAddress_, this->serverSockFd_);
}

void Client::sendRegistrationMessage_() {
  // create the register message
  Register reg(this->nodeIdx_, 0, 0, this->clientAddress_);
  char *serializedMsg = reg.serialize();
  size_t numBytes = reg.numSerializedBytes();

  clientDebug(this->serverAddress_.sin_port, this->clientAddress_.sin_port,
              "SENDING REGISTRATION MESSAGE", this->debug_);
  sendToServer_(serializedMsg, numBytes);
  delete[] serializedMsg;
}

void Client::sendToServer_(char *serializedMsg, size_t size) {
  connectToServer_();

  send(this->serverSockFd_, serializedMsg, size, 0);

  // close server socket
  close(this->serverSockFd_);
}

void Client::sendToPeer(size_t peerIndex, char *serializedMsg,
                        size_t numBytes) {
  if (this->running_) {
    bool connected = false;
    // create socket for connection
    int clientSockFd = 0;
    for (size_t i = 0; i < this->getNumPeers(); i++) {
      if (this->peerIdxs_.at(i) == peerIndex) {
        connectToDest(this->peerAddrs_.at(i), clientSockFd);
        connected = true;
      }
    }

    // send message on created socket
    assert(connected);
    assert(serializedMsg != nullptr);
    send(clientSockFd, serializedMsg, numBytes, 0);
    close(clientSockFd);
  } else {
    fprintf(stderr, "Client is currently not running!");
  }
}

void Client::listenForMessages_() {
  // bind the socket to the client address
  int clientSockFd = bindTo(this->clientAddress_);

  this->r_thread_ = new ClientThread(this, clientSockFd);
  this->r_thread_->start();
  clientDebug(this->serverAddress_.sin_port, this->clientAddress_.sin_port,
              "LISTENING FOR MESSAGES", this->debug_);
}

void Client::stopListening_() {
  this->r_thread_->stop();

  // TODO: why does this cause an error?
  // calling stop correctly exits out of thread loop
  this->r_thread_->join();
  delete this->r_thread_;
}

void Client::handleMessage_(Message *msg) {
  clientDebug(this->serverAddress_.sin_port, this->clientAddress_.sin_port,
              "HANDLING INCOMING MESSAGE", this->debug_);
  MsgKind kind = msg->kind_;

  if (kind == MsgKind::DIRECTORY) {
    clientDebug(this->serverAddress_.sin_port, this->clientAddress_.sin_port,
                "DIRECTORY", this->debug_);
    Directory *dir = dynamic_cast<Directory *>(msg);

    // update peer list
    handleDirectory_(dir);
  } else if (kind == MsgKind::DATA) {
    clientDebug(this->serverAddress_.sin_port, this->clientAddress_.sin_port,
                "DATA", this->debug_);
    Data *data = dynamic_cast<Data *>(msg);

    handleData_(data);
  } else if (kind == MsgKind::PUT) {
    clientDebug(this->serverAddress_.sin_port, this->clientAddress_.sin_port,
                "PUT", this->debug_);
    Put *put = dynamic_cast<Put *>(msg);

    handlePut_(put);
  } else if (kind == MsgKind::GET) {
    clientDebug(this->serverAddress_.sin_port, this->clientAddress_.sin_port,
                "GET", this->debug_);
    Get *get = dynamic_cast<Get *>(msg);

    handleGet_(get);
  } else if (kind == MsgKind::WAITANDGET) {
    clientDebug(this->serverAddress_.sin_port, this->clientAddress_.sin_port,
                "WAIT AND GET", this->debug_);
    WaitAndGet *wag = dynamic_cast<WaitAndGet *>(msg);

    handleWaitAndGet_(wag);
  } else if (kind == MsgKind::REPLY) {
    clientDebug(this->serverAddress_.sin_port, this->clientAddress_.sin_port,
                "REPLY", this->debug_);
    Reply *reply = dynamic_cast<Reply *>(msg);

    handleReply_(reply);
  } else if (kind == MsgKind::KILL) {
    clientDebug(this->serverAddress_.sin_port, this->clientAddress_.sin_port,
                "KILL", this->debug_);
    stop();
  } else {
    clientDebug(this->serverAddress_.sin_port, this->clientAddress_.sin_port,
                "Recieved invalid message type!", this->debug_);
    assert(false);
  }
}

void Client::handleDirectory_(Directory *dir) {
  clientDebug(this->serverAddress_.sin_port, this->clientAddress_.sin_port,
              "HANDLING DIRECTORY", this->debug_);
  // clear the current client list
  this->peerAddrs_.clear();
  this->peerIdxs_.clear();

  // iterate through the given client list
  // and add all clients that are not self to list
  for (size_t i = 0; i < dir->numClients_; i++) {
    sockaddr_in current = dir->clientAddrs_[i];
    size_t idx = dir->clientIdxs_[i];
    if (!addrEqual(this->clientAddress_, current)) {
      this->peerAddrs_.push_back(current);
      this->peerIdxs_.push_back(idx);
    }
  }
}

void Client::handleData_(Data *data) {
  // handling data will be fleshed out in the future, for now print
  clientDebug(this->serverAddress_.sin_port, this->clientAddress_.sin_port,
              data->data_, this->debug_);
}

void Client::handlePut_(Put *put) {
  // handling put will be fleshed out in the future, for now print
  clientDebug(this->serverAddress_.sin_port, this->clientAddress_.sin_port,
              put->key_, this->debug_);
}

void Client::handleGet_(Get *get) {
  // handling data will be fleshed out in the future, for now print
  clientDebug(this->serverAddress_.sin_port, this->clientAddress_.sin_port,
              get->key_, this->debug_);
}

void Client::handleWaitAndGet_(WaitAndGet *wag) {
  // handling data will be fleshed out in the future, for now print
  clientDebug(this->serverAddress_.sin_port, this->clientAddress_.sin_port,
              wag->key_, this->debug_);
}

void Client::handleReply_(Reply *reply) {
  // handling data will be fleshed out in the future, for now print
  clientDebug(this->serverAddress_.sin_port, this->clientAddress_.sin_port,
              reply->blob_, this->debug_);
}
