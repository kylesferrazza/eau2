#pragma once

#include <cstddef>
#include <vector>

#include "message.h"
#include "message/data.h"
#include "message/directory.h"
#include "message/get.h"
#include "message/put.h"
#include "message/register.h"
#include "message/reply.h"
#include "message/unregister.h"
#include "message/waitandget.h"

#include "clientthread.h"

class ClientThread;

class Client {
public:
  struct sockaddr_in clientAddress_;
  struct sockaddr_in serverAddress_;
  size_t nodeIdx_;
  int serverSockFd_;
  ClientThread *r_thread_;

  std::vector<struct sockaddr_in> peerAddrs_;
  std::vector<size_t> peerIdxs_;
  bool running_ = false;
  bool debug_ = false;

  Client(struct sockaddr_in clientAddress, struct sockaddr_in serverAddress,
         size_t nodeIdx);

  Client(struct sockaddr_in clientAddress, struct sockaddr_in serverAddress,
         size_t nodeIdx, bool debug);

  virtual ~Client();

  /**
   * Start the client by listening for incoming messages and then
   * connecting to the server
   */
  void start();

  /**
   * Shut down the client by deregistering with the server
   */
  void stop();

  /**
   * return a list of client node indexes that could be sent to
   */
  std::vector<size_t> peerList();

  /**
   * returns the number of connected peers
   */
  size_t getNumPeers();

  /**
   * returns whether or not the client is running
   */
  bool isRunning();

  /**
   * Sends a data message to a peer at the given index
   */
  void sendData(size_t peerIndex, Data *data);

  /**
   * Send a put message to a peer at the given index
   */
  void sendPut(size_t peerIndex, Put *put);

  /**
   * Send a get message to a peer at the given index
   */
  void sendGet(size_t peerIndex, Get *get);

  /**
   * Send a wait and get message to a peer at the given index
   */
  void sendWaitAndGet(size_t peerIndex, WaitAndGet *wag);

  /**
   * Send a reply  message to a peer at the given index
   */
  void sendReply(size_t peerIndex, Reply *reply);

  /**
   * determine if this client is currently connected to the given peer index
   */
  bool hasPeer(size_t peerIndex);

  //========================Private Helper Methods=============================
  // connects to server and sends a registration message.  Returns the
  // server socket created
  void connectToServer_();

  // send a registration message to the server
  void sendRegistrationMessage_();

  // sends a given serialized message to the server
  void sendToServer_(char *serializedMsg, size_t size);

  void sendToPeer(size_t peerIndex, char *serializedMsg, size_t numBytes);

  // listens for incoming messages
  void listenForMessages_();

  // shuts down the thread that is listening for messages
  void stopListening_();

  void handleMessage_(Message *msg);

  // handle a directory message by clearing the peer list
  // and adding all clients that are not self
  void handleDirectory_(Directory *dir);

  // handle data
  virtual void handleData_(Data *data);

  // handle put
  virtual void handlePut_(Put *put);

  // handle data by printing the content to standard out
  virtual void handleGet_(Get *get);

  // handle data by printing the content to standard out
  virtual void handleWaitAndGet_(WaitAndGet *wag);

  // handle data by printing the content to standard out
  virtual void handleReply_(Reply *reply);
};
