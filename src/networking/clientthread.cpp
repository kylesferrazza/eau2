#include "clientthread.h"
#include "network.h"

// receives messages
ClientThread::ClientThread(Client *cl, int &client_sock) {
  cl_ = cl;
  client_sock_ = client_sock;
}

void ClientThread::run(std::atomic<bool> &running) {
  // loop indefinitely, receiving messages
  while (running.load()) {
    Message *msg = receive(client_sock_);

    if (msg != nullptr) {
      this->cl_->handleMessage_(msg);
      delete msg;
    }
  }
};
