#pragma once

#include "client.h"
#include "../util/thread.h"

class Client;

// receives messages
class ClientThread : public Thread {
public:
  Client *cl_;
  int client_sock_;

  ClientThread(Client *cl, int &client_sock);

  void run(std::atomic<bool> &running);
};
