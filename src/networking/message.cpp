#include "message.h"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "message/register.h"
#include "message/unregister.h"
#include "message/kill.h"
#include "message/put.h"
#include "message/get.h"
#include "message/waitandget.h"
#include "message/directory.h"
#include "message/data.h"
#include "message/reply.h"

Message::Message(MsgKind kind, size_t sender, size_t target, size_t id) {
  this->kind_ = kind;
  this->sender_ = sender;
  this->target_ = target;
  this->id_ = id;
}

Message::Message(char *buf) {
  deserializedSoFar_ = 0;
  assert(deserializedSoFar_ == 0);

  deserializedSoFar_ = 0;

  int typ = 0;
  memcpy(&typ, buf + deserializedSoFar_, intSize);
  this->kind_ = (MsgKind)typ;
  deserializedSoFar_ += intSize;

  this->sender_ = 0;
  memcpy(&this->sender_, buf + deserializedSoFar_, sizeSize);
  deserializedSoFar_ += sizeSize;

  this->target_ = 0;
  memcpy(&this->target_, buf + deserializedSoFar_, sizeSize);
  deserializedSoFar_ += sizeSize;

  this->id_ = 0;
  memcpy(&this->id_, buf + deserializedSoFar_, sizeSize);
  deserializedSoFar_ += sizeSize;
}

// Takes the data out of the buffer to create a new Message
// I own the buffer
Message *Message::deserialize(char *buf) {
  int typ = 0;
  memcpy(&typ, buf, sizeof(int));
  MsgKind kind = (MsgKind)typ;
  Message *m;
  switch (kind) {
  case MsgKind::REGISTER:
    m = new Register(buf);
    break;
  case MsgKind::DIRECTORY:
    m = new Directory(buf);
    break;
  case MsgKind::DATA:
    m = new Data(buf);
    break;
  case MsgKind::KILL:
    m = new Kill(buf);
    break;
  case MsgKind::UNREGISTER:
    m = new Unregister(buf);
    break;
  case MsgKind::PUT:
    m = new Put(buf);
    break;
  case MsgKind::GET:
    m = new Get(buf);
    break;
  case MsgKind::WAITANDGET:
    m = new WaitAndGet(buf);
    break;
  case MsgKind::REPLY:
    m = new Reply(buf);
    break;
  default:
    fprintf(stderr, "BAD MESSAGE!\n");
    m = nullptr;
    break;
  }

  return m;
}

char *Message::startSubclassSerialize(size_t additionalSize) {
  char *super = Message::serialize();
  assert(serializedSoFar_ > 0);
  size_t endSize = serializedSoFar_ + additionalSize;
  char *ret = new char[endSize];
  memcpy(ret, super, serializedSoFar_);
  delete[] super;
  return ret;
}

// caller owns the returned buffer
char *Message::serialize() {
  size_t endSize = intSize    // typ
		   + sizeSize // sender_
		   + sizeSize // target_
		   + sizeSize // id_
      ;

  char *ret = new char[endSize];
  serializedSoFar_ = 0;
  assert(serializedSoFar_ == 0);

  this->cpySerialize(ret, &this->kind_, intSize);
  this->cpySerialize(ret, &this->sender_, sizeSize);
  this->cpySerialize(ret, &this->target_, sizeSize);
  this->cpySerialize(ret, &this->id_, sizeSize);

  return ret;
}

size_t Message::numSerializedBytes() { return serializedSoFar_; }
