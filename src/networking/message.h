#pragma once

#include <arpa/inet.h>
#include <sys/socket.h>

#include "../util/serializable.h"

enum class MsgKind {
  REGISTER = 0,
  DIRECTORY = 1,
  DATA = 2,
  KILL = 3,
  UNREGISTER = 4,
  PUT = 5,
  GET = 6,
  WAITANDGET = 7,
  REPLY = 8
};

class Message : public Serializable {
public:
  MsgKind kind_;  // the message kind
  size_t sender_; // the index of the sender node
  size_t target_; // the index of the receiver node
  size_t id_;     // an id t unique within the node

  // Only meant to be called by subclasses!
  Message(MsgKind kind, size_t sender, size_t target, size_t id);

  // Only meant to be called by subclasses!
  // copies data from buf, setting fields
  Message(char *buf);

  // Takes the data out of the buffer to create a new M
  // I own the buffer
  static Message *deserialize(char *buf);

  char *startSubclassSerialize(size_t additionalSize);

  // caller owns the returned buffer
  virtual char *serialize();

  size_t numSerializedBytes();
};
