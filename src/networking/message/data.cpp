#include "data.h"

#include <cassert>

Data::Data(size_t sender, size_t target, size_t id, size_t dataLength,
	   char *data)
    : Message(MsgKind::DATA, sender, target, id) {
  this->dataLength_ = dataLength;
  this->data_ = data;
}

Data::Data(char *buf) : Message(buf) {
  assert(deserializedSoFar_ > 0);

  this->cpyDeserialize(&this->dataLength_, buf, sizeSize);

  this->data_ = new char[this->dataLength_];
  this->cpyDeserialize(this->data_, buf, dataLength_);
}

Data::~Data() { delete[] this->data_; }

char *Data::serialize() {
  char *ret = this->startSubclassSerialize(sizeSize + dataLength_);

  this->cpySerialize(ret, &this->dataLength_, sizeSize);
  this->cpySerialize(ret, this->data_, dataLength_);

  return ret;
}
