#pragma once

#include <cstddef>
#include "../message.h"

// Message sent from client to client, specifying some data
class Data : public Message {
public:
  size_t dataLength_;
  char *data_;

  // I own data, and it must be made with `new`.
  Data(size_t sender, size_t target, size_t id, size_t dataLength, char *data);

  // deserialize a Data message
  Data(char *buf);

  ~Data();

  virtual char *serialize();
};
