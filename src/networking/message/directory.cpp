#include "directory.h"

#include <cassert>

Directory::Directory(size_t sender, size_t target, size_t id, size_t numClients,
	  sockaddr_in *clientAddrs, size_t *clientIdxs)
    : Message(MsgKind::DIRECTORY, sender, target, id) {
  this->numClients_ = numClients;
  this->clientAddrs_ = clientAddrs;
  this->clientIdxs_ = clientIdxs;
}

Directory::Directory(char *buf) : Message(buf) {
  assert(deserializedSoFar_ > 0);
  size_t addrSize = sizeof(sockaddr_in);

  this->cpyDeserialize(&this->numClients_, buf, sizeSize);

  size_t dataLen = numClients_ * addrSize;
  this->clientAddrs_ = new sockaddr_in[this->numClients_];
  this->cpyDeserialize(this->clientAddrs_, buf, dataLen);

  dataLen = numClients_ * sizeSize;
  this->clientIdxs_ = new size_t[this->numClients_];
  this->cpyDeserialize(this->clientIdxs_, buf, dataLen);
}

Directory::~Directory() {
  delete[] this->clientAddrs_;
  delete[] this->clientIdxs_;
}

char *Directory::serialize() {
  size_t addrSize = sizeof(sockaddr_in);
  size_t allAddrsSize = addrSize * numClients_;
  size_t allIdxSize = sizeSize * numClients_;

  char *ret =
      this->startSubclassSerialize(sizeSize + allAddrsSize + allIdxSize);

  this->cpySerialize(ret, &this->numClients_, sizeSize);
  this->cpySerialize(ret, this->clientAddrs_, allAddrsSize);
  this->cpySerialize(ret, this->clientIdxs_, allIdxSize);

  return ret;
}
