#pragma once

#include <cstddef>

#include "../message.h"

// Message sent from server to client to update client directory
class Directory : public Message {
public:
  size_t numClients_;
  sockaddr_in *clientAddrs_;
  size_t *clientIdxs_;

  // I own clientAddrs, and it must be made with `new`
  Directory(size_t sender, size_t target, size_t id, size_t numClients,
            sockaddr_in *clientAddrs, size_t *clientIdxs);

  // deserialize a Directory message
  Directory(char *buf);

  ~Directory();

  virtual char *serialize();
};
