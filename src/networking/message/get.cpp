#include "get.h"

#include <cassert>
#include <cstring>

Get::Get(size_t sender, size_t target, size_t id, char *key, StoreTypes type)
    : Message(MsgKind::GET, sender, target, id) {
  // copy the key value
  this->key_ = new char[strlen(key) + 1];
  strcpy(this->key_, key);

  this->type_ = type;
}

Get::Get(char *buf) : Message(buf) {
  assert(deserializedSoFar_ > 0);
  size_t keySize = 0;

  this->cpyDeserialize(&keySize, buf, sizeSize);
  this->key_ = new char[keySize];
  this->cpyDeserialize(this->key_, buf, keySize);
  int type_number = 0;
  this->cpyDeserialize(&type_number, buf, intSize);
  this->type_ = StoreTypes(type_number);
}

Get::~Get() { delete[] key_; }

char *Get::serialize() {
  size_t keySize = strlen(key_) + 1;

  char *ret = this->startSubclassSerialize(sizeSize + keySize + intSize);

  this->cpySerialize(ret, &keySize, sizeSize);
  this->cpySerialize(ret, this->key_, keySize);
  int type = static_cast<int>(this->type_);
  this->cpySerialize(ret, &type, intSize);

  return ret;
}
