#pragma once

#include "../../store/storeTypes.h"
#include "../message.h"

// Message sent from client to client instructing a get operation
class Get : public Message {
public:
  char *key_;
  StoreTypes type_;

  // blob and key are owned and should be created with new
  Get(size_t sender, size_t target, size_t id, char *key, StoreTypes type);

  // deserialize a put messasge
  Get(char *buf);

  ~Get();

  virtual char *serialize();
};
