#include "kill.h"

Kill::Kill(size_t sender, size_t target, size_t id)
    : Message(MsgKind::KILL, sender, target, id) {
  // no-op
}

Kill::Kill(char *buf) : Message(buf) {
  // no-op
}
