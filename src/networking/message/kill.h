#pragma once

#include "../message.h"

// Message sent when server shuts down to shut down a client
class Kill : public Message {
public:
  Kill(size_t sender, size_t target, size_t id);

  // deserialize a Kill message
  Kill(char *buf);
};
