#include "put.h"

#include <cassert>

Put::Put(size_t sender, size_t target, size_t id, char *blob, size_t blobSize,
         char *key, StoreTypes type)
    : Message(MsgKind::PUT, sender, target, id) {
  this->blob_ = new char[blobSize];
  this->blobSize_ = blobSize;
  memcpy(this->blob_, blob, blobSize);

  this->key_ = new char[strlen(key)];
  strcpy(this->key_, key);

  this->type_ = type;
}

Put::Put(char *buf) : Message(buf) {
  assert(deserializedSoFar_ > 0);
  size_t keySize = 0;

  this->cpyDeserialize(&this->blobSize_, buf, sizeSize);
  this->blob_ = new char[this->blobSize_];
  this->cpyDeserialize(this->blob_, buf, this->blobSize_);
  this->cpyDeserialize(&keySize, buf, sizeSize);
  this->key_ = new char[keySize];
  this->cpyDeserialize(this->key_, buf, keySize);
  int type_number = 0;
  this->cpyDeserialize(&type_number, buf, intSize);
  this->type_ = StoreTypes(type_number);
}

Put::~Put() {
  delete[] blob_;
  delete[] key_;
}

char *Put::serialize() {
  size_t keySize = strlen(key_) + 1;

  char *ret = this->startSubclassSerialize(sizeSize + blobSize_ + sizeSize +
                                           keySize + intSize);

  this->cpySerialize(ret, &this->blobSize_, sizeSize);
  this->cpySerialize(ret, this->blob_, this->blobSize_);
  this->cpySerialize(ret, &keySize, sizeSize);
  this->cpySerialize(ret, this->key_, keySize);
  int type = static_cast<int>(this->type_);
  this->cpySerialize(ret, &type, intSize);

  return ret;
}
