#pragma once

#include "../../store/storeTypes.h"
#include "../message.h"

// Message sent from client to client instructing a put operation
class Put : public Message {
public:
  char *blob_;
  size_t blobSize_;
  char *key_;
  StoreTypes type_;

  // blob and key are owned and should be created with new
  Put(size_t sender, size_t target, size_t id, char *blob, size_t blobSize,
      char *key, StoreTypes type);

  // deserialize a put messasge
  Put(char *buf);

  ~Put();

  virtual char *serialize();
};
