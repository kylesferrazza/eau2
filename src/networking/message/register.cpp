#include "register.h"

#include <cassert>

Register::Register(size_t sender, size_t target, size_t i, sockaddr_in client)
    : Message(MsgKind::REGISTER, sender, target, i) {
  this->client_ = client;
}

Register::Register(char *buf) : Message(buf) {
  assert(deserializedSoFar_ > 0);
  size_t addrSize = sizeof(sockaddr_in);
  this->cpyDeserialize(&this->client_, buf, addrSize);
}

char *Register::serialize() {
  size_t addrSize = sizeof(sockaddr_in);
  char *ret = this->startSubclassSerialize(addrSize);
  this->cpySerialize(ret, &this->client_, addrSize);
  return ret;
}
