#pragma once

#include "../message.h"
#include <cstddef>

// Message sent from client to server to register
class Register : public Message {
public:
  sockaddr_in client_;

  Register(size_t sender, size_t target, size_t i, sockaddr_in client);

  // deserialize a Register message
  Register(char *buf);

  virtual char *serialize();
};
