#include "reply.h"

#include <cassert>

Reply::Reply(size_t sender, size_t target, size_t id, char *blob,
             size_t blobSize, StoreTypes type)
    : Message(MsgKind::REPLY, sender, target, id) {
  this->blob_ = new char[blobSize];
  this->blobSize_ = blobSize;
  memcpy(this->blob_, blob, blobSize);

  this->type_ = type;
}

Reply::Reply(char *buf) : Message(buf) {
  assert(deserializedSoFar_ > 0);
  size_t keySize = 0;

  this->cpyDeserialize(&this->blobSize_, buf, sizeSize);
  this->blob_ = new char[this->blobSize_];
  this->cpyDeserialize(this->blob_, buf, this->blobSize_);

  int type_number = 0;
  this->cpyDeserialize(&type_number, buf, intSize);
  this->type_ = StoreTypes(type_number);
}

Reply::~Reply() { delete[] blob_; }

char *Reply::serialize() {

  char *ret =
      this->startSubclassSerialize(sizeSize + this->blobSize_ + intSize);

  this->cpySerialize(ret, &this->blobSize_, sizeSize);
  this->cpySerialize(ret, this->blob_, this->blobSize_);

  int type = static_cast<int>(this->type_);
  this->cpySerialize(ret, &type, intSize);

  return ret;
}
