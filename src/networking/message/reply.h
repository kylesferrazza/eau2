#pragma once

#include "../../store/storeTypes.h"
#include "../message.h"

// Message sent from client to client replying to a get request
class Reply : public Message {
public:
  char *blob_;
  size_t blobSize_;
  StoreTypes type_;

  // blob and key are owned and should be created with new
  Reply(size_t sender, size_t target, size_t id, char *blob, size_t blobSize,
        StoreTypes type);

  // deserialize a put messasge
  Reply(char *buf);

  ~Reply();

  virtual char *serialize();
};
