#include "unregister.h"

Unregister::Unregister(size_t sender, size_t target, size_t i,
		       sockaddr_in client)
    : Register(sender, target, i, client) {
  this->kind_ = MsgKind::UNREGISTER;
}

Unregister::Unregister(char *buf) : Register(buf) {
  // no-op
}
