#pragma once

#include "../message.h"
#include "register.h"

// Message sent from client to server to unregister the client
// extends register because it works the same, just a different message kind
class Unregister : public Register {
public:
  Unregister(size_t sender, size_t target, size_t i, sockaddr_in client);

  Unregister(char *buf);
};
