#include "waitandget.h"

WaitAndGet::WaitAndGet(size_t sender, size_t target, size_t id, char *key,
                       StoreTypes type)
    : Get(sender, target, id, key, type) {
  this->kind_ = MsgKind::WAITANDGET;
}

WaitAndGet::WaitAndGet(char *buf) : Get(buf) {
  // no-op
}
