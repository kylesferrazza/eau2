#pragma once

#include "../../store/storeTypes.h"
#include "../message.h"
#include "get.h"

// A special kind of get request that waits for the key to be present
class WaitAndGet : public Get {
public:
  WaitAndGet(size_t sender, size_t target, size_t id, char *key,
             StoreTypes type);

  WaitAndGet(char *buf);
};
