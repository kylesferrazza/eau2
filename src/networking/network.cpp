#include "network.h"

#include <cassert>
#include <fcntl.h> /* Added for the nonblocking socket */
#include <unistd.h>

int bindTo(struct sockaddr_in addr) {
  int opt = 1;
  int my_sock;
  struct sockaddr *address = (struct sockaddr *)&addr;

  // Creating socket file descriptor
  if ((my_sock = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
    fprintf(stderr, "creating binding socket failed\n");
    return 0;
  }

  // Set the socket options
  if (setsockopt(my_sock, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt,
                 sizeof(opt))) {
    fprintf(stderr, "error setting sockopt");
    return 0;
  }

  // Bind server to correct address any port
  if (bind(my_sock, address, sizeof(sockaddr_in)) < 0) {
    fprintf(stderr, "binding socket failed\n");
    return 0;
  }

  // make the socket non blocking
  fcntl(my_sock, F_SETFL, O_NONBLOCK);

  return my_sock;
}

Message *receive(int socket) {
  struct sockaddr_in sourceAddress;
  int sourceSocket;

  if (listen(socket, SOMAXCONN) < 0) {
    printf("Error on listen");
    assert(false);
  }

  int addrLen = sizeof(sockaddr_in);

  sourceSocket =
      accept(socket, (struct sockaddr *)&sourceAddress, (socklen_t *)&addrLen);
  if (sourceSocket > 0) {
    char *buf = new char[BUFFER_SIZE];
    char *tempBuf;
    size_t numRepeats = 0;
    size_t numRead = read(sourceSocket, buf, BUFFER_SIZE);

    while (numRead == BUFFER_SIZE) {
      numRepeats++;
      size_t lastSize = BUFFER_SIZE * numRepeats;
      size_t newSize = BUFFER_SIZE * (numRepeats + 1);

      // copy buf to temp buf
      tempBuf = new char[lastSize];
      memcpy(tempBuf, buf, lastSize);
      delete[] buf;

      // resize buf
      buf = new char[newSize];

      // copy tempBuf back into buf
      memcpy(buf, tempBuf, lastSize);
      delete[] tempBuf;

      // read into an offset
      numRead = read(sourceSocket, buf + lastSize, BUFFER_SIZE);
    }
    assert(numRead != -1);

    Message *m = Message::deserialize(buf);
    delete[] buf;
    return m;
  }

  // by default, return nullptr
  return nullptr;
}

void connectToDest(struct sockaddr_in dest_addr, int &fd) {
  // create socket
  if ((fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    fprintf(stderr, "Socket creation error\n");
    assert(false);
  }

  // connect socket
  if (connect(fd, (struct sockaddr *)&dest_addr, sizeof(dest_addr)) < 0) {
    fprintf(stderr, "Connection to destination failed trying to connect to client at port: %d\n", ntohs(dest_addr.sin_port));
    assert(false);
  }
}

sockaddr_in createAddress(const char *ip, int port) {
  sockaddr_in my_addr;

  my_addr.sin_family = AF_INET;
  my_addr.sin_port = htons(port);
  my_addr.sin_addr.s_addr = inet_addr(ip);

  return my_addr;
}

bool addrEqual(sockaddr_in first, sockaddr_in second) {
  return (first.sin_family == second.sin_family) &&
         (first.sin_port == second.sin_port) &&
         (first.sin_addr.s_addr == second.sin_addr.s_addr);
}
