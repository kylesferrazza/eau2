#pragma once

#include "message.h"

#define BUFFER_SIZE 4096

/** Helper function to create a socket binding to an ip and port.
 *  It also takes the file descriptor of the desired socket as well as the
 *  address struct to be populated.
 */
int bindTo(struct sockaddr_in addr);

/** receives a message on the given socket
 *  and deserializes it
 */
Message *receive(int socket);

/** Helper function to create a socket connecting to a specified sockadd_in.
 *  It returns the file descriptor of the created socket
 */
void connectToDest(struct sockaddr_in dest_addr, int &fd);

/** Helper function to create a sockaddr_in based on an ip and port
 */
sockaddr_in createAddress(const char *ip, int port);

/** helper to compare equality of two sockaddr_in structs */
bool addrEqual(sockaddr_in first, sockaddr_in second);
