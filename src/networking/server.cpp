#include "server.h"

#include "message/directory.h"
#include "message/kill.h"

#include <unistd.h>

#include "../debug/print.h"

Server::Server(struct sockaddr_in address) {
  this->address_ = address;
  this->numClients_ = 0;
}

Server::Server(struct sockaddr_in address, bool debug) {
  this->address_ = address;
  this->numClients_ = 0;
  this->debug_ = debug;
}

void Server::start() {
  struct sockaddr_in address;
  int receiveSock = bindTo(this->address_);
  assert(receiveSock != 0);
  this->s_thread_ = new ServerThread(this, receiveSock, this->debug_);
  this->s_thread_->start();
  this->running_ = true;
  serverDebug("STARTING SERVER", this->debug_);
}

void Server::stop() {
  // create a kill message
  Kill k(0, 0, 0);
  char *serializedMsg = k.serialize();
  size_t numBytes = k.numSerializedBytes();

  // send a kill to all clients
  serverDebug("SENDING KILL MESSAGES", this->debug_);
  broadcast_(serializedMsg, numBytes);
  serverDebug("STOPPING SERVER", this->debug_);
  delete[] serializedMsg;

  // clear client list
  this->clientAddrs_.clear();
  this->clientIdxs_.clear();

  this->numClients_ = 0;

  stopListening_();
  this->running_ = false;
}

std::vector<size_t> Server::getClients() { return clientIdxs_; }

size_t Server::getNumClients() { return numClients_; }

void Server::handleMessage_(Message *msg) {
  MsgKind kind = msg->kind_;

  // we only care about register
  if (kind == MsgKind::REGISTER) {
    Register *reg = dynamic_cast<Register *>(msg);
    handleRegister_(reg);
  } else if (kind == MsgKind::UNREGISTER) {
    Unregister *unreg = dynamic_cast<Unregister *>(msg);
    handleUnregister_(unreg);
  } else {
    serverDebug("Recieved invalid message type at server!", this->debug_);
    assert(false);
  }
}

// handles a new client registration message
void Server::handleRegister_(Register *reg) {
  serverDebug("HANDLING INCOMING REGISTRATION", this->debug_);
  // store client address
  struct sockaddr_in clientAddr = reg->client_;
  size_t clientIdx = reg->sender_;

  // add client to list of registered clients
  this->clientAddrs_.push_back(clientAddr);
  this->clientIdxs_.push_back(clientIdx);

  this->numClients_++;

  // send updated client list to all clients
  sendDirectoryUpdates_();
}

void Server::handleUnregister_(Unregister *unreg) {
  serverDebug("HANDLING INCOMING UNREGISTRATION", this->debug_);
  // iterate through all clients and remove the client
  // that is trying to unregister
  for (size_t i = 0; i < this->numClients_; i++) {
    sockaddr_in current = this->clientAddrs_.at(i);
    if (addrEqual(current, unreg->client_)) {
      this->clientAddrs_.erase(this->clientAddrs_.begin() + i);
      this->clientIdxs_.erase(this->clientIdxs_.begin() + i);

      this->numClients_--;
    }
  }

  // update all clients with the current list
  sendDirectoryUpdates_();
}

void Server::sendDirectoryUpdates_() {
  serverDebug("SENDING DIRECTORY UPDATES", this->debug_);
  // create a primitive array of sockaddr_in
  struct sockaddr_in *addresses = new sockaddr_in[numClients_];
  size_t *idxs = new size_t[numClients_];

  for (size_t i = 0; i < numClients_; i++) {
    addresses[i] = clientAddrs_.at(i);
    idxs[i] = clientIdxs_.at(i);
  }

  // create directory message
  // TODO: do we want ids for server/client/message based on Message?
  Directory *dir = new Directory(0, 0, 0, numClients_, addresses, idxs);
  char *serializedMsg = dir->serialize();
  size_t numBytes = dir->numSerializedBytes();

  // send message to every registered client
  broadcast_(serializedMsg, numBytes);

  delete[] serializedMsg;
  delete dir;
}

void Server::broadcast_(char *serializedMsg, size_t size) {
  serverDebug("BROADCASTING MESSAGE", this->debug_);
  // send message to every registered client
  for (size_t i = 0; i < numClients_; i++) {
    // get current client address
    struct sockaddr_in clientAddr = clientAddrs_.at(i);

    // create socket for connection
    int clientSockFd = 0;
    connectToDest(clientAddr, clientSockFd);

    // send message on created socket
    send(clientSockFd, serializedMsg, size, 0);

    // close socket to client
    close(clientSockFd);
  }
}

void Server::stopListening_() {
  this->s_thread_->stop();
  this->s_thread_->join();
  delete this->s_thread_;
}
