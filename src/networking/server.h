#pragma once

#include "message.h"
#include "message/kill.h"
#include "message/register.h"
#include "message/unregister.h"
#include "network.h"
#include "serverthread.h"
#include <netinet/in.h>
#include <vector>

class ServerThread;

class Server {
public:
  struct sockaddr_in address_;
  size_t numClients_;
  bool debug_ = false;

  // NOTE: I changed this to hold only sockaddr_ins, since we don't have ids in
  // a directory update
  std::vector<struct sockaddr_in> clientAddrs_;

  std::vector<size_t> clientIdxs_;

  ServerThread *s_thread_;

  bool running_ = false;

  Server(struct sockaddr_in address);

  Server(struct sockaddr_in address, bool debug);

  //==========================Public API Methods===============================

  /**
   * Starts a server by creating a thread that will handle incoming messages
   */
  void start();

  /**
   * Stops a server by telling all clients to shut themselves
   * down and handling server cleanup
   */
  void stop();

  /**
   * Returns the list of currently connected client Idxs
   */
  std::vector<size_t> getClients();

  /**
   * Returns the number of currently connected clients
   */
  size_t getNumClients();

  //========================Private Helper Methods=============================

  // handle an incoming message to the server
  void handleMessage_(Message *msg);

  // handles a new client registration message
  void handleRegister_(Register *reg);

  // handles an unregister message from a client
  void handleUnregister_(Unregister *unreg);

  /** Update each client in the list of clients by sending them
   *  a directory message
   */
  void sendDirectoryUpdates_();

  // broadcasts a message to every connected client
  void broadcast_(char *serializedMsg, size_t size);

  // stops listening for incoming messages by shutting down the
  // receiving thread
  void stopListening_();
};
