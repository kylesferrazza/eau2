#include "serverthread.h"
#include <atomic>

ServerThread::ServerThread(Server *s, int socketFd) {
  this->s_ = s;
  this->socketFd_ = socketFd;
}

ServerThread::ServerThread(Server *s, int socketFd, bool debug) {
  this->s_ = s;
  this->socketFd_ = socketFd;
  this->debug_ = debug;
}

void ServerThread::run(std::atomic<bool> &running) {
  while (running.load()) {
    // I own this message
    Message *msg = receive(this->socketFd_);

    if (msg != nullptr) {
      ServerThread::debug("HANDLING INCOMING MESSAGE");

      this->s_->handleMessage_(msg);
      delete msg;
    }
  }
}

void ServerThread::debug(const char *x) {
  if (this->debug_)
    fprintf(stderr, "[\033[0;32mSERVER THREAD\033[0m] %s\n", x);
}
