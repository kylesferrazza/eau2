#pragma once

#include "../util/thread.h"
#include "server.h"

class Server;

class ServerThread : public Thread {
public:
  Server *s_;
  int socketFd_;
  bool debug_ = false;

  ServerThread(Server *s, int socketFd);

  ServerThread(Server *s, int socketFd, bool debug);

  void run(std::atomic<bool> &running);

  void debug(const char *x);
};
