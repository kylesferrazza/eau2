#pragma once

#include "../dataframe/dataframe.h"
#include "../store/keyList.h"
#include <condition_variable>
#include <mutex>

// Messages between receive thread and client.
class ThreadMsg {
public:
  // Pointer for answers from receive thread
  DataFrame *dfanswer;
  KeyList *klanswer;

  // Ensure we aren't touching the pointer at the same time as the receive
  // thread
  std::mutex answerMutex;

  // Let receive thread notify us when the pointer is ready
  std::condition_variable answerCV;

  bool keepWaiting;

  ThreadMsg();
};

extern ThreadMsg globalMsg;
