#include "distributedKDStore.h"

#include <algorithm>
#include <mutex>
#include <netinet/in.h>

#include "../networking/client.h"
#include "kdstore.h"
#include "storeTypes.h"

#include "../networking/threadmsg.h"

#include <unistd.h>

size_t MAX_ROWS = 1000;

// helper to generate a random string
std::string random_string(size_t length) {
  auto randchar = []() -> char {
    const char charset[] = "0123456789"
                           "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                           "abcdefghijklmnopqrstuvwxyz";
    const size_t max_index = (sizeof(charset) - 1);
    return charset[rand() % max_index];
  };
  std::string str(length, 0);
  std::generate_n(str.begin(), length, randchar);
  return str;
}

// TODO: make this thread-safe:
//    generated IDs for messages,
//    map from IDs to whatever the receive-thread needs to give back to us
//    wait for the receive thread to signal (using broadcast) some conditional
//    variable, then check if it added our ID, and use the associated value

DistributedKDStore::DistributedKDStore(struct sockaddr_in clientAddress,
                                       struct sockaddr_in serverAddress,
                                       size_t nodeIdx)
    : Client(clientAddress, serverAddress, nodeIdx) {
  dfstore = new KDStore();
  klstore = new KKLStore();
}

DistributedKDStore::~DistributedKDStore() {
  delete dfstore;
  delete klstore;
}

void DistributedKDStore::put(Key k, DataFrame *df) {
  // chunk the dataframe
  std::vector<DataFrame *> chunks = df->split(MAX_ROWS);

  // get all other peers
  std::vector<size_t> peerIds = this->peerList();
  peerIds.push_back(this->nodeIdx_);

  // create key list that will eventually get stored at the master key
  KeyList *kl = new KeyList();

  // loop through each chunk and put it at a peer
  for (size_t i = 0; i < chunks.size(); i++) {
    size_t index = (peerIds.size() - 1) % (i + 1);
    size_t currentPeerID = peerIds.at(index);
    DataFrame *curChunk = chunks.at(i);

    // create the custom key to store it at
    std::string rand = random_string(10);
    Key *curKey = new Key(rand, currentPeerID);

    kl->push_back(curKey);

    if (currentPeerID == this->nodeIdx_) {
      this->dfstore->put(*curKey, curChunk);
    } else {
      // send a put request to the correct node
      char *serialized = curChunk->serialize();
      Put p(this->nodeIdx_, currentPeerID, 0, serialized,
            curChunk->numSerializedBytes(), curKey->key_cstr(),
            StoreTypes::DATAFRAME);
      this->sendPut(curKey->homeNode_, &p);
      delete[] serialized;
    }
  }

  // store the keyList at the correct home node
  if (k.homeNode_ == this->nodeIdx_) {
    this->klstore->put(k, kl);
  } else {
    char *serialized = kl->serialize();
    Put p(this->nodeIdx_, k.homeNode_, 0, serialized, kl->numSerializedBytes(),
          k.key_cstr(), StoreTypes::KEYLIST);
    this->sendPut(k.homeNode_, &p);
    delete[] serialized;
  }
}

KeyList *DistributedKDStore::get(Key k) {
  // if the idx of the key matches this node, get locally
  if (k.homeNode_ == this->nodeIdx_) {
    return this->klstore->get(k);
  } else {
    // otherwise send a get request to the correct node
    Get *g = new Get(this->nodeIdx_, k.homeNode_, 0, k.key_cstr(),
                     StoreTypes::KEYLIST);

    this->sendGet(k.homeNode_, g);

    // wait for receive thread to handle reply
    std::unique_lock<std::mutex> lock(globalMsg.answerMutex);
    globalMsg.answerCV.wait(lock, [] { return !globalMsg.keepWaiting; });

    // get dataframe from shared dataframe memory
    KeyList *ret = globalMsg.klanswer;
    globalMsg.klanswer = nullptr;

    // give lock back to receive thread
    globalMsg.keepWaiting = true;
    lock.unlock();

    return ret;
  }
}

KeyList *DistributedKDStore::waitAndGet(Key k) {
  // if the idx of the key matches this node, get locally
  if (k.homeNode_ == this->nodeIdx_) {
    KeyList *kl = this->klstore->get(k);
    if (kl != nullptr) {
      return kl;
    } else {
      // sleep for a second to wait for a put at that key to come in and try
      // again
      usleep(1000);
      return waitAndGet(k);
    }
  } else {
    // otherwise send a get request to the correct node
    WaitAndGet *wag = new WaitAndGet(this->nodeIdx_, k.homeNode_, 0,
                                     k.key_cstr(), StoreTypes::KEYLIST);

    this->sendWaitAndGet(k.homeNode_, wag);
  }

  // wait for receive thread to handle reply
  std::unique_lock<std::mutex> lock(globalMsg.answerMutex);
  globalMsg.answerCV.wait(lock, [] { return !globalMsg.keepWaiting; });

  // get keylist from shared keylist memory
  KeyList *ret = globalMsg.klanswer;

  globalMsg.klanswer = nullptr;

  // give lock back to receive thread
  globalMsg.keepWaiting = true;
  lock.unlock();

  return ret;
}

DataFrame *DistributedKDStore::toDataFrame(KeyList *kl) {
  std::vector<DataFrame *> dflist;
  for (size_t i = 0; i < kl->size(); i++) {
    char *keyString = kl->at(i)->key_cstr();
    DataFrame *curDf = this->getDf_(*kl->at(i));
    dflist.push_back(curDf);
  }

  return DataFrame::join(dflist);
}

DataFrame *DistributedKDStore::getDf_(Key k) {
  // if the idx of the key matches this node, get locally
  if (k.homeNode_ == this->nodeIdx_) {
    return this->dfstore->get(k);
  } else {
    // otherwise send a get request to the correct node
    Get *g = new Get(this->nodeIdx_, k.homeNode_, 0, k.key_cstr(),
                     StoreTypes::DATAFRAME);

    this->sendGet(k.homeNode_, g);

    // wait for receive thread to handle reply
    std::unique_lock<std::mutex> lock(globalMsg.answerMutex);
    globalMsg.answerCV.wait(lock, [] { return !globalMsg.keepWaiting; });

    // get dataframe from shared dataframe memory
    DataFrame *ret = globalMsg.dfanswer;
    globalMsg.dfanswer = nullptr;

    // give lock back to receive thread
    globalMsg.keepWaiting = true;
    lock.unlock();

    return ret;
  }
}

void DistributedKDStore::handlePut_(Put *p) {
  // the message should be destined to me...
  assert(p->target_ == this->nodeIdx_);

  // grab the key from the put request
  Key k(p->key_, this->nodeIdx_);

  // deserailize the request based on the store type
  if (p->type_ == StoreTypes::KEYLIST) {
    KeyList *kl = KeyList::deserialize(p->blob_);
    this->klstore->put(k, kl);
  } else if (p->type_ == StoreTypes::DATAFRAME) {
    DataFrame *df = DataFrame::deserialize(p->blob_);
    this->dfstore->put(k, df);
  } else {
    assert(false);
  }

  // determine if we need to reply to any wait and get requests
  int idx = this->isWaitingOn_(p->key_);
  if (idx == -1) {
    return;
  } else if (idx == this->nodeIdx_) {
    {
      std::lock_guard<std::mutex> lock(globalMsg.answerMutex);
      if (p->type_ == StoreTypes::KEYLIST) {
        // deserialize the keyList in the reply
        KeyList *kl = KeyList::deserialize(p->blob_);
        globalMsg.klanswer = kl;
      } else if (p->type_ == StoreTypes::DATAFRAME) {
        // deserialize the dataframe in the reply
        DataFrame *df = DataFrame::deserialize(p->blob_);
        globalMsg.dfanswer = df;
      } else {
        assert(false);
      }

      globalMsg.keepWaiting = false;
    }

    // notify main thread that there is a new reply
    globalMsg.answerCV.notify_all();
  } else {
    WaitAndGet wag = waiting.at(idx);
    Reply r(this->nodeIdx_, wag.sender_, 0, p->blob_, p->blobSize_, wag.type_);

    this->sendReply(wag.sender_, &r);
  }
}

void DistributedKDStore::handleGet_(Get *g) {
  // the message should be destined to me...
  assert(g->target_ == this->nodeIdx_);

  // get the value at Key locally and send a reply
  Key k(g->key_, this->nodeIdx_);

  char *blob;
  size_t blobSize;

  if (g->type_ == StoreTypes::KEYLIST) {
    KeyList *kl = this->klstore->get(k);
    blob = kl->serialize();
    blobSize = kl->numSerializedBytes();
    Reply r(this->nodeIdx_, g->sender_, 0, blob, blobSize, StoreTypes::KEYLIST);
    this->sendReply(g->sender_, &r);
  } else if (g->type_ == StoreTypes::DATAFRAME) {
    DataFrame *df = this->dfstore->get(k);
    blob = df->serialize();
    blobSize = df->numSerializedBytes();
    Reply r(this->nodeIdx_, g->sender_, 0, blob, blobSize,
            StoreTypes::DATAFRAME);
    this->sendReply(g->sender_, &r);
  } else {
    assert(false);
  }

  delete[] blob;
}

void DistributedKDStore::handleWaitAndGet_(WaitAndGet *wag) {
  // the message should be destined to me...
  assert(wag->target_ == this->nodeIdx_);

  // get the value at Key locally and send a reply
  Key k(wag->key_, this->nodeIdx_);

  // reply right away to the request
  char *blob;
  size_t blobSize;

  if (wag->type_ == StoreTypes::KEYLIST) {
    KeyList *kl = this->klstore->get(k);
    // if our local store doesn't have the key
    if (kl == nullptr) {
      // add key to the list of keys we are waiting on
      waiting.push_back(*wag);
      return;
    }
    blob = kl->serialize();
    blobSize = kl->numSerializedBytes();
    Reply r(this->nodeIdx_, wag->sender_, 0, blob, blobSize,
            StoreTypes::KEYLIST);
    this->sendReply(wag->sender_, &r);
  } else if (wag->type_ == StoreTypes::DATAFRAME) {
    DataFrame *df = this->dfstore->get(k);
    // if our local store doesn't have the key
    if (df == nullptr) {
      // add key to the list of keys we are waiting on
      waiting.push_back(*wag);
      return;
    }
    blob = df->serialize();
    blobSize = df->numSerializedBytes();
    Reply r(this->nodeIdx_, wag->sender_, 0, blob, blobSize,
            StoreTypes::DATAFRAME);
    this->sendReply(wag->sender_, &r);
  } else {
    assert(false);
  }

  delete[] blob;
}

void DistributedKDStore::handleReply_(Reply *r) {
  // create new scope
  {
    std::lock_guard<std::mutex> lock(globalMsg.answerMutex);
    if (r->type_ == StoreTypes::KEYLIST) {
      // deserialize the keyList in the reply
      KeyList *kl = KeyList::deserialize(r->blob_);
      globalMsg.klanswer = kl;
    } else if (r->type_ == StoreTypes::DATAFRAME) {
      // deserialize the dataframe in the reply
      DataFrame *df = DataFrame::deserialize(r->blob_);
      globalMsg.dfanswer = df;
    } else {
      assert(false);
    }

    globalMsg.keepWaiting = false;
  }

  // notify main thread that there is a new dataframe reply
  globalMsg.answerCV.notify_all();
}

int DistributedKDStore::isWaitingOn_(char *key) {
  for (size_t i = 0; i < this->waiting.size(); i++) {
    WaitAndGet current = this->waiting.at(i);
    char *waitingKey = current.key_;
    if (strcmp(key, waitingKey) == 0) {
      return i;
    }
  }
  return -1;
}

void DistributedKDStore::local_map(KeyList *kl, Reader *reader) {
  for (int i = 0; i < kl->size(); i++) {
    Key *k = kl->at(i);
    if (k->homeNode_ == this->nodeIdx_) {
      DataFrame *df = this->getDf_(*k);
      df->map(*reader);
      delete df;
    }
  }
}
