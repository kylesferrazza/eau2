#pragma once

#include <vector>

#include "../dataframe/dataframe.h"
#include "kdstore.h"

#include "kklstore.h"

#include "../networking/client.h"
#include "key.h"
#include "keyList.h"

#include "../networking/message/get.h"
#include "../networking/message/put.h"
#include "../networking/message/reply.h"
#include "../networking/message/waitandget.h"

#include "../dataframe/rowers/reader.h"

class DataFrame;
class KDStore;

class Put;
class Get;
class WaitAndGet;
class Reply;

class DistributedKDStore : public Client {
public:
  KDStore *dfstore;
  KKLStore *klstore;

  // when I see one of these keys about to come in, send it to the
  // associated waiting node
  std::vector<WaitAndGet> waiting;

  // Construct a distributed KD Store.
  DistributedKDStore(struct sockaddr_in clientAddress,
                     struct sockaddr_in serverAddress, size_t nodeIdx);

  ~DistributedKDStore();

  //---------------------------------------------------------
  // public methods for the user to use
  //---------------------------------------------------------

  // Will put the dataframe at the given key by splitting it up and
  // distributing between the other nodes
  void put(Key k, DataFrame *df);

  // Will return a keyList containing the keys
  // associated with the desired dataframe or nullptr
  // if the dataframe doesn;t exist yet
  KeyList *get(Key k);

  // Will wait for the given key to exist and then
  // return a keyList containing the keys
  // associated with the desired dataframe
  KeyList *waitAndGet(Key k);

  // convert the keylist of chunks to a dataframe to an actual
  // dataframe by getting each chunk and then merging them into
  // a single dataframe
  DataFrame *toDataFrame(KeyList *kl);

  // Helper to send a get request to a node to retrieve a dataframe chunk
  DataFrame *getDf_(Key k);

  //------------------------------------------------------------------------
  // override client handle methods to handle each put, get, waitAndGet, and
  // reply correctly for the store
  //------------------------------------------------------------------------

  virtual void handlePut_(Put *p) override;

  virtual void handleGet_(Get *g) override;

  virtual void handleWaitAndGet_(WaitAndGet *wag) override;

  virtual void handleReply_(Reply *r) override;

  // private helper that will return what index the waiting get request is at
  // otherwise -1 if the given key is not being waited on
  int isWaitingOn_(char *key);

  // Map for just the keys at the current node
  void local_map(KeyList *kl, Reader *reader);
};
