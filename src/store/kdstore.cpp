#include "kdstore.h"

KDStore::KDStore() { this->kv_ = new KVStore(); }

KDStore::~KDStore() { delete this->kv_; }

void KDStore::put(Key k, DataFrame *df) {
  char *val = df->serialize();
  this->kv_->put(val, k);
}

DataFrame *KDStore::get(Key k) {
  char *val = this->kv_->get(k);
  if (val == nullptr)
    return nullptr;
  DataFrame *df = DataFrame::deserialize(val);
  // delete[] val; // TODO: this delete should not happen here
  return df;
}
