#pragma once

#include "../dataframe/dataframe.h"
#include "kvstore.h"

class DataFrame;

/**
 * A class that represents a store from key to dataframe
 * It will use the KV store underlying and convert a Value
 * to a dataframe for simplicity at the Application layer
 */
class KDStore {
public:
  KVStore *kv_;
  KDStore();
  ~KDStore();

  // Will put the dataframe at the given key
  void put(Key k, DataFrame *df);

  // Will return a dataframe at the given key
  DataFrame *get(Key k);
};
