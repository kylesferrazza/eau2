#include "key.h"

Key::Key(std::string key, size_t homeNode) {
  key_ = key;
  homeNode_ = homeNode;
}

bool Key::equals(Object *other) {
  Key *k = dynamic_cast<Key *>(other);
  if (k == nullptr)
    return false;
  return (this->key_.compare(k->key_) == 0) &&
         (this->homeNode_ == k->homeNode_);
}

char *Key::key_cstr() {
  // TODO: how to remove const properly?
  return (char *)this->key_.c_str();
}

char *Key::serialize() {
  this->serializedSoFar_ = 0;
  char *key = this->key_cstr();
  size_t keySize = strlen(key) + 1;
  size_t bufSize = sizeSize + sizeSize + keySize;
  char *ret = new char[bufSize];

  this->cpySerialize(ret, &keySize, sizeSize);
  this->cpySerialize(ret, key, keySize);
  this->cpySerialize(ret, &this->homeNode_, sizeSize);

  return ret;
}

Key *Key::deserialize(char *buf) {
  Key *k = new Key("", 0);

  size_t keySize = 0;
  k->cpyDeserialize(&keySize, buf, k->sizeSize);

  char *key = new char[keySize];
  k->cpyDeserialize(key, buf, keySize);

  k->key_ = std::string(key);
  delete[] key;

  k->cpyDeserialize(&k->homeNode_, buf, k->sizeSize);

  return k;
}
