#pragma once

#include "../util/object.h"
#include "../util/serializable.h"

#include <string>

class Key : public Serializable {
public:
  // member variables
  std::string key_;
  size_t homeNode_;

  // constructor
  Key(std::string key, size_t homeNode);

  // compare this key to the given other key
  bool equals(Object *other) override;

  char *key_cstr();

  char *serialize() override;

  static Key *deserialize(char *buf);
};
