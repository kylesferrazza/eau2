#include "keyList.h"

KeyList::KeyList() {
  // do nothing
}

KeyList::~KeyList() {
  // TODO: should do this, but there is an issue between deserialized
  // keys are regularly passed in keys
  // for (size_t i = 0; i < this->size(); i++) {
  //   Key *current = this->at(i);
  //   delete current;
  // }
}

Key *KeyList::at(size_t idx) { return this->keys.at(idx); }

void KeyList::push_back(Key *k) { return this->keys.push_back(k); }

size_t KeyList::size() { return this->keys.size(); }

bool KeyList::equals(Object *o) {
  KeyList *kl = dynamic_cast<KeyList *>(o);
  if (kl == nullptr) {
    return false;
  } else if (this->size() != kl->size()) {
    return false;
  }

  for (size_t i = 0; i < kl->size(); i++) {
    if (!this->at(i)->equals(kl->at(i))) {
      return false;
    }
  }

  return true;
}

char *KeyList::serialize() {
  char **serializedKeys = new char *[this->size()]; // serialized data
  size_t *keySizes = new size_t[this->size()];

  for (size_t i = 0; i < this->size(); i++) {
    Key *current = this->at(i);
    char *serKey = current->serialize();
    size_t serBytes = current->numSerializedBytes();

    serializedKeys[i] = serKey;
    keySizes[i] = serBytes;
  }

  // calculate the end size
  size_t endSize = sizeSize; // number of keys
  // for every key
  for (size_t i = 0; i < this->size(); i++) {
    endSize += sizeSize;    // size_t to tell how many bytes that key is
    endSize += keySizes[i]; // number of serialized bytes for that key
  }

  // create return buffer of size endSize
  char *ret = new char[endSize];
  this->serializedSoFar_ = 0;

  // start putting serialized data in return buffer
  size_t numKeys = this->size();
  this->cpySerialize(ret, &numKeys,
                     sizeSize); // start with total number of keys

  // for every key
  for (size_t i = 0; i < this->size(); i++) {
    this->cpySerialize(ret, &keySizes[i],
                       sizeSize); // number of serialized bytes

    for (size_t j = 0; j < keySizes[i]; j++) {
      this->cpySerialize(ret, &serializedKeys[i][j], charSize);
    }
  }

  for (size_t i = 0; i < this->size(); i++) {
    delete[] serializedKeys[i];
  }

  delete[] serializedKeys;
  delete[] keySizes;

  return ret;
}

KeyList *KeyList::deserialize(char *buf) {
  KeyList *ret = new KeyList();

  size_t numKeys = 0;
  ret->cpyDeserialize(&numKeys, buf, ret->sizeSize);

  size_t keyBytes = 0;

  for (size_t i = 0; i < numKeys; i++) {
    ret->cpyDeserialize(&keyBytes, buf,
                        ret->sizeSize); // store the number of serialized bytes
                                        // for the current key

    char *keyBuf = new char[keyBytes];
    ret->cpyDeserialize(keyBuf, buf, keyBytes);

    Key *current = Key::deserialize(keyBuf);
    ret->push_back(current);

    delete[] keyBuf;
  }

  return ret;
}

void KeyList::print() {
  for (size_t i = 0; i < this->size(); i++) {
    Key *current = this->at(i);
    printf("%s\n", current->key_cstr());
  }
  return;
}