#pragma once

#include "../util/object.h"
#include "../util/serializable.h"

#include "key.h"
#include <vector>

// wrapper around a vector of keys that allows it to be serialzized
// for the purpose of sending over the network
class KeyList : public Serializable {
public:
  std::vector<Key *> keys;

  KeyList();

  ~KeyList();

  Key *at(size_t idx);

  void push_back(Key *k);

  size_t size();

  bool equals(Object *o) override;

  char *serialize() override;

  static KeyList *deserialize(char *buf);

  void print();
};