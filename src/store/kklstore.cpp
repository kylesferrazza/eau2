#include "kklstore.h"

KKLStore::KKLStore() { this->kv_ = new KVStore(); }

KKLStore::~KKLStore() { delete this->kv_; }

void KKLStore::put(Key k, KeyList *kl) {
  char *val = kl->serialize();
  this->kv_->put(val, k);
}

KeyList *KKLStore::get(Key k) {
  char *val = this->kv_->get(k);
  if (val == nullptr)
    return nullptr;
  KeyList *kl = KeyList::deserialize(val);
  // delete[] val; // TODO: this delete should not happen here
  return kl;
}
