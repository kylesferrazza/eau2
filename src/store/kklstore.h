#pragma once

#include "keyList.h"
#include "kvstore.h"

/**
 * A class that represents a store from key to KeyList
 * It will use the KV store underlying and convert a Value
 * to a keylist for simplicity at the Application layer
 */
class KKLStore {
public:
  KVStore *kv_;
  KKLStore();
  ~KKLStore();

  // Will put the KeyList at the given key
  void put(Key k, KeyList *kl);

  // Will return a KeyList at the given key
  KeyList *get(Key k);
};