#include "kvstore.h"

KVStore::KVStore() { map = new std::map<std::string, char *>(); }

KVStore::~KVStore() {
  std::map<std::string, char *>::iterator it;
  // delete all stored blobs
  for (it = map->begin(); it != map->end(); ++it) {
    delete[] it->second;
  }
  delete map;
}

// Will put the dataframe at the given key
void KVStore::put(char *v, Key k) { map->insert(std::make_pair(k.key_, v)); }

// Will return a dataframe at the given key
char *KVStore::get(Key k) {
  if (map->find(k.key_) == map->end())
    return nullptr;
  return map->at(k.key_);
}
