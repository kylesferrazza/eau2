#pragma once

#include "key.h"
#include <map>
#include <string>

/**
 * A representation of a key to Value store.  This operates on
 * Values and more specific stores will be used to expose to the
 * Application layer
 */
class KVStore {
public:
  std::map<std::string, char *> *map;

  KVStore();

  ~KVStore();

  // Will put the dataframe at the given key
  void put(char *v, Key k);

  // Will return a dataframe at the given key
  char *get(Key k);
};
