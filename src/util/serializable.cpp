#include "serializable.h"

#include <cstring>

Serializable::Serializable() {
  this->serializedSoFar_ = 0;
  this->deserializedSoFar_ = 0;
}

void Serializable::cpySerialize(char *ret, void *mem, size_t size) {
  memcpy(ret + serializedSoFar_, mem, size);
  serializedSoFar_ += size;
}

void Serializable::cpyDeserialize(void *dest, char *buf, size_t size) {
  memcpy(dest, buf + deserializedSoFar_, size);
  deserializedSoFar_ += size;
}

size_t Serializable::numSerializedBytes() { return serializedSoFar_; }
