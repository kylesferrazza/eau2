#pragma once

#include "object.h"

class Serializable : public Object {
public:
  size_t serializedSoFar_;
  size_t deserializedSoFar_;

  size_t sizeSize = sizeof(size_t);
  size_t charSize = sizeof(char);
  size_t intSize = sizeof(int);
  size_t boolSize = sizeof(bool);
  size_t doubleSize = sizeof(double);

  Serializable();

  void cpySerialize(char *ret, void *mem, size_t size);

  void cpyDeserialize(void *dest, char *buf, size_t size);

  size_t numSerializedBytes();

  virtual char *serialize() = 0;
};
