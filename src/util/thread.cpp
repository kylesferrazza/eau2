#include "thread.h"

void Thread::start() {
  this->running_.store(true);
  thread_ = std::thread([this] { this->run(std::ref(running_)); });
}

void Thread::stop() { this->running_.store(false); }

void Thread::join() { thread_.join(); }

void Thread::yield() { std::this_thread::yield(); }

void Thread::sleep(size_t millis) {
  std::this_thread::sleep_for(std::chrono::milliseconds(millis));
}

void Thread::detach() { thread_.detach(); }

void Thread::run(std::atomic<bool> &running) { assert(false); }

String *Thread::thread_id() {
  std::stringstream buf;
  buf << std::this_thread::get_id();
  std::string buffer(buf.str());
  return new String(buffer.c_str(), buffer.size());
}

void Lock::lock() { mtx_.lock(); }

void Lock::unlock() { mtx_.unlock(); }

void Lock::wait() { cv_.wait(mtx_); }

void Lock::notify_all() { cv_.notify_all(); }

Counter::Counter() { next_ = 0; }

size_t Counter::next() {
  size_t r = next_++;
  return r;
}
size_t Counter::prev() {
  size_t r = next_--;
  return r;
}

size_t Counter::current() { return next_; }
