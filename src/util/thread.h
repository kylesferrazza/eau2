#pragma once

#include "object.h"
#include "string.h"
#include <atomic>
#include <condition_variable>
#include <cstdlib>
#include <mutex>
#include <sstream>
#include <thread>

/** A Thread wraps the thread operations in the standard library.
 *  author: vitekj@me.com */
class Thread : public Object {
public:
  std::thread thread_;
  std::atomic<bool> running_{false};

  /** Starts running the thread, invoked the run() method. */
  void start();

  void stop();

  /** Wait on this thread to terminate. */
  void join();

  /** Yield execution to another thread. */
  static void yield();

  /** Sleep for millis milliseconds. */
  static void sleep(size_t millis);

  /** detach thread so that it can run freely */
  void detach();

  /** Subclass responsibility, the body of the run method */
  virtual void run(std::atomic<bool> &running);

  // there's a better way to get an CwC value out of a threadid, but this'll do
  // for now
  /** Return the id of the current thread */
  static String *thread_id();
};

/** A convenient lock and condition variable wrapper. */
class Lock : public Object {
public:
  std::mutex mtx_;
  std::condition_variable_any cv_;

  /** Request ownership of this lock.
   *
   *  Note: This operation will block the current thread until the lock can
   *  be acquired.
   */
  void lock();

  /** Release this lock (relinquish ownership). */
  void unlock();

  /** Sleep and wait for a notification on this lock.
   *
   *  Note: After waking up, the lock is owned by the current thread and
   *  needs released by an explicit invocation of unlock().
   */
  void wait();

  // Notify all threads waiting on this lock
  void notify_all();
};

/** A simple thread-safe counter. */
class Counter : public Object {
public:
  std::atomic<size_t> next_;

  Counter();

  size_t next();

  size_t prev();

  size_t current();
};
