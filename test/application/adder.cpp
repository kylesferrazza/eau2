#include "../../googletest-src/googletest/include/gtest/gtest.h"

#include "../../src/application/application.h"
#include "../../src/dataframe/dataframe.h"
#include "../../src/store/key.h"
#include "../../src/dataframe/rowers/reader.h"

class AdderTest : public Application {
public:
  AdderTest(size_t idx) : Application(idx) {}
  void run_() {
    Key key("triv", 0);
    SIMap *simap = new SIMap();
    Adder *adder = new Adder(*simap);
    Schema sch("S");
    DataFrame *df = new DataFrame(sch);
    Row *r = new Row(sch);

    String *a = new String("a");
    r->set(0, a);

    for (int i = 0; i < 3; i++) {
      df->add_row(*r);
    }

    String *b = new String("b");
    r->set(0, b);

    for (int i = 0; i < 2; i++) {
      df->add_row(*r);
    }

    String *c = new String("c");
    r->set(0, c);

    df->add_row(*r);

    df->map(*adder);

    df->print();

    size_t numA = simap->get(*a)->v;
    size_t numB = simap->get(*b)->v;
    size_t numC = simap->get(*c)->v;

    ASSERT_EQ(numA, 3);
    ASSERT_EQ(numB, 2);
    ASSERT_EQ(numC, 1);

    delete df;
    delete r;
    delete adder;
    delete a;
    delete b;
    delete c;
    delete simap;
  }
};

TEST(application, adder) {
  AdderTest app(0);
  app.run_();

  printf("Ran successfully\n");
}
