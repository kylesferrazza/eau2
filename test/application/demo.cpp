#include "../../src/application/application.h"
#include "../../src/dataframe/dataframe.h"
#include "../../src/store/key.h"
#include "../../src/store/keyList.h"
#include "../../src/util/helper.h"
#include <cstring>
#include <stdlib.h>
#include <unistd.h>

size_t SZ = 100 * 1000;

class Demo : public Application {
public:
  Key main;
  Key verify;
  Key check;

  Demo(size_t idx)
      : Application(idx), main("main", 0), verify("verif", 0), check("ck", 0) {}

  void run_() override {
    switch (this_node()) {
    case 0:
      producer();
      break;
    case 1:
      counter();
      break;
    case 2:
      summarizer();
    }
  }

  void producer() {
    double *vals = new double[SZ];
    double sum = 0;
    for (size_t i = 0; i < SZ; ++i)
      sum += vals[i] = i;
    DataFrame::fromArray(&main, kv, SZ, vals);
    DataFrame::fromScalar(check, *kv, sum);
  }

  void counter() {
    KeyList *kl = kv->waitAndGet(main);
    DataFrame *v = kv->toDataFrame(kl);
    assert(v != nullptr);
    double sum = 0;
    for (size_t i = 0; i < SZ; ++i) {
      double val = v->get_double(0, i);
      sum += val;
    }

    printf("The sum is %f\n", sum);
    DataFrame::fromScalar(verify, *kv, sum);
  }

  void summarizer() {
    KeyList *resultKl = kv->waitAndGet(verify);
    DataFrame *result = kv->toDataFrame(resultKl);
    KeyList *expectedKl = kv->waitAndGet(check);
    DataFrame *expected = kv->toDataFrame(expectedKl);
    assert(result != nullptr);
    assert(expected != nullptr);
    double expected_sum = expected->get_double(0, 0);
    double result_sum = result->get_double(0, 0);

    pln(expected_sum == result_sum ? "SUCCESS" : "FAILURE");
    delete result;
    delete expected;
  }
};

int main() {
  Demo app0(0);
  app0.run_();
  usleep(100);
  Demo app1(1);
  Demo app2(2);
  app1.run_();
  app2.run_();
}
