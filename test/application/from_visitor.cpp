#include "../../src/application/application.h"
#include "../../src/dataframe/dataframe.h"
#include "../../src/store/key.h"
#include "../../src/dataframe/rowers/writer.h"

class FromVisitorTest : public Application {
public:
  FromVisitorTest(size_t idx) : Application(idx) {}
  void run_() {
    Key key("triv", 0);
    Writer *writer = new FileReader("from_visitor.txt");
    DataFrame *df = DataFrame::fromVisitor(&key, this->kv, "S", writer);
    df->print();
    delete df;
    delete writer;
  }
};

int main() {
  FromVisitorTest app(0);
  app.run_();

  printf("Ran successfully\n");
}
