#include "../../src/application/application.h"
#include "../../src/dataframe/dataframe.h"
#include "../../src/store/keyList.h"

#include <stdlib.h>

class Trivial : public Application {
public:
  Trivial(size_t idx) : Application(idx) {}
  void run_() {
    size_t SZ = 1000 * 1000;
    double *vals = new double[SZ];
    double sum = 0;
    for (size_t i = 0; i < SZ; ++i)
      sum += vals[i] = i;
    Key key("triv", 0);
    DataFrame *df = DataFrame::fromArray(&key, kv, SZ, vals);
    assert(df->get_double(0, 1) == 1);
    KeyList *kl = kv->get(key);
    DataFrame *df2 = kv->toDataFrame(kl);

    for (size_t i = 0; i < SZ; ++i)
      sum -= df2->get_double(0, i);
    assert(sum == 0);
    delete[] vals;
    delete df;
    delete df2;
  }
};

int main() {
  Trivial app(0);
  app.run_();

  printf("Ran successfully\n");
}
