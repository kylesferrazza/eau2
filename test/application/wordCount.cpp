#include "../../src/application/application.h"
#include "../../src/dataframe/dataframe.h"
#include "../../src/dataframe/rowers/reader.h"
#include "../../src/dataframe/rowers/writer.h"

#include "../../src/util/map.h"

#include <unistd.h>

class KeyBuff {
public:
  const char *prefix;
  int homeNode;

  KeyBuff(const char *prefix, int homeNode) {
    this->prefix = prefix;
    this->homeNode = homeNode;
  }

  Key *c(char *suffix) {
    StrBuff *s = new StrBuff();
    s->c(prefix);
    s->c(suffix);
    String *full = s->get();
    char *out = full->c_str();
    Key *k = new Key(out, homeNode);
    delete full;
    delete s;
    return k;
  }

  Key *c(size_t suffix) {
    std::string str = std::to_string(suffix);
    char *cstr = (char *)str.c_str();
    return this->c(cstr);
  }
};

/****************************************************************************
 * Calculate a word count for given file:
 *   1) read the data (single node)
 *   2) produce word counts per homed chunks, in parallel
 *   3) combine the results
 *************************************************based on author: pmaj ****/
class WordCount : public Application {
public:
  static const size_t BUFSIZE = 1024;
  Key in;
  // TODO: create some keyBuf (put with key file)
  KeyBuff kbuf;

  SIMap all;

  char *path;

  // NOTE: Our app doesn't take in a network
  WordCount(size_t idx, char *path)
      : Application(idx), in("data", 0), kbuf("wc-map-", 0), path(path) {}

  /** The master nodes reads the input, then all of the nodes count. */
  void run_() override {
    if (this->idx_ == 0) {
      FileReader fr(path);
      delete DataFrame::fromVisitor(&in, kv, "S", &fr);
    }
    local_count();
    reduce();
  }

  /** Returns a key for given node.  These keys are homed on master node
   *  which then joins them one by one. */
  Key *mk_key(size_t idx) {
    Key *k = kbuf.c(idx);
    char *cstr = k->key_cstr();
    fprintf(stderr, "Created key %s\n", cstr);
    return k;
  }

  /** Compute word counts on the local node and build a data frame. */
  void local_count() {
    KeyList *kl = kv->waitAndGet(in);
    p("Node ").p(this->idx_).pln(": starting local count...");

    SIMap map;

    Adder add(map);

    kv->local_map(kl, &add); // I think the error is here somehow

    delete kl;

    Summer cnt(map);
    delete DataFrame::fromVisitor(mk_key(this->idx_), kv, "SI", &cnt);
  }

  /** Merge the data frames of all nodes */
  void reduce() {
    if (this_node() != 0)
      return;
    pln("Node 0: reducing counts...");
    SIMap map;
    Key *own = mk_key(0);
    KeyList *my = kv->get(*own); // list of keys
    DataFrame *df = kv->toDataFrame(my);
    delete my;
    merge(df, map);
    std::vector<size_t> peerIDs = kv->peerList();
    for (size_t i = 0; i < peerIDs.size(); i++) { // merge other nodes
      size_t curId = peerIDs.at(i);
      Key *ok = mk_key(curId);
      KeyList *okKL = kv->waitAndGet(*ok);
      DataFrame *okDF = kv->toDataFrame(okKL);
      delete okKL;
      merge(okDF, map);
      delete ok;
    }
    p("Different words: ").pln(map.size());
    delete own;
  }

  void merge(DataFrame *df, SIMap &m) {
    Adder add(m);
    df->map(add);
    delete df;
  }
};

int main(int argc, char **argv) {
  const char *usage = "./wordcount [node_id] [word_file]";
  if (argc != 3) {
    printf("USAGE: %s\n", usage);
    exit(0);
  }

  char *nodeID = argv[1];
  char *fileName = argv[2];
  size_t id = atoi(nodeID);

  WordCount app(id, fileName);

  sleep(5);

  app.run_();
}
