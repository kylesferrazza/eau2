// LANGUAGE: Cpp
#include "../../googletest-src/googletest/include/gtest/gtest.h"
#include "../../src/dataframe/dataframe.h"
#include "testRowers.h"
#include <vector>

#define EXPECT_EQUAL(a, b) EXPECT_TRUE(a->equals(b) && b->equals(a))
#define ASSERT_EQUAL(a, b) ASSERT_TRUE(a->equals(b) && b->equals(a))

class PersonalFixtures : public ::testing::Test {
protected:
  String *hello, *world;
  Column *ic, *sc, *dc, *bc;
  Row *r1, *r2;
  Schema *sch;
  DataFrame *df;

  Schema *schema, *schemaTwo;

  void SetUp() override {
    hello = new String("hello");
    world = new String("world");
    ic = new IntColumn();
    sc = new StringColumn();
    dc = new DoubleColumn();
    bc = new BoolColumn();

    sch = new Schema("IBDS");

    r1 = new Row(*sch);
    r1->set(0, 10);
    r1->set(1, true);
    r1->set(2, 42.24);
    r1->set(3, hello);

    r2 = new Row(*sch);
    r2->set(0, 20);
    r2->set(1, false);
    r2->set(2, 66.66);
    r2->set(3, world);

    df = new DataFrame(*sch);

    schema = new Schema();
    schemaTwo = new Schema("ISDB");
  }

  void TearDown() override {
    delete hello;
    delete world;
    delete ic;
    delete sc;
    delete dc;
    delete bc;
    delete sch;
    delete r1;
    delete r2;
    delete df;
    delete schema;
    delete schemaTwo;
  }
};

/****************************************************************************
 * Column specific tests
 */
TEST_F(PersonalFixtures, intcolumn) {
  EXPECT_EQ(ic->size(), 0);
  EXPECT_EQ(ic->as_int(), ic);
  EXPECT_EQ(ic->as_string(), nullptr);
  EXPECT_EQ(ic->as_double(), nullptr);
  EXPECT_EQ(ic->as_bool(), nullptr);
  EXPECT_EQ(ic->get_type(), 'I');

  ic->push_back(true);
  EXPECT_EQ(ic->size(), 0);
  ic->push_back(1.0f);
  EXPECT_EQ(ic->size(), 0);
  ic->push_back(hello);
  EXPECT_EQ(ic->size(), 0);

  for (int i = 0; i < 300; i++) {
    ic->push_back(i * 4);
  }

  EXPECT_EQ(ic->size(), 300);

  IntColumn *icc = ic->as_int();
  for (int i = 0; i < 300; i++) {
    EXPECT_EQ(icc->get(i), i * 4);
  }
}

TEST_F(PersonalFixtures, stringcolumn) {
  EXPECT_EQ(sc->size(), 0);
  EXPECT_EQ(sc->as_string(), sc);
  EXPECT_EQ(sc->as_int(), nullptr);
  EXPECT_EQ(sc->as_double(), nullptr);
  EXPECT_EQ(sc->as_bool(), nullptr);
  EXPECT_EQ(sc->get_type(), 'S');

  sc->push_back(true);
  EXPECT_EQ(sc->size(), 0);
  sc->push_back(1.0f);
  EXPECT_EQ(sc->size(), 0);
  sc->push_back(1);
  EXPECT_EQ(sc->size(), 0);

  sc->push_back(hello);
  EXPECT_EQ(sc->size(), 1);
  sc->push_back(world);
  EXPECT_EQ(sc->size(), 2);

  StringColumn *scc = sc->as_string();
  EXPECT_EQUAL(scc->get(0), hello);
  EXPECT_EQUAL(scc->get(1), world);
}

TEST_F(PersonalFixtures, boolcolumn) {
  EXPECT_EQ(bc->size(), 0);
  EXPECT_EQ(bc->as_int(), nullptr);
  EXPECT_EQ(bc->as_string(), nullptr);
  EXPECT_EQ(bc->as_double(), nullptr);
  EXPECT_EQ(bc->as_bool(), bc);
  EXPECT_EQ(bc->get_type(), 'B');

  bc->push_back(1);
  EXPECT_EQ(bc->size(), 0);
  bc->push_back(1.0f);
  EXPECT_EQ(bc->size(), 0);
  bc->push_back(hello);
  EXPECT_EQ(bc->size(), 0);

  for (int i = 0; i < 300; i++) {
    bool divisByThree = i % 3 == 0;
    bc->push_back(divisByThree);
  }

  EXPECT_EQ(bc->size(), 300);

  BoolColumn *bcc = bc->as_bool();
  for (int i = 0; i < 300; i++) {
    bool divisByThree = i % 3 == 0;
    EXPECT_EQ(bcc->get(i), divisByThree);
  }
}

TEST_F(PersonalFixtures, doublecolumn) {
  EXPECT_EQ(dc->size(), 0);
  EXPECT_EQ(dc->as_double(), dc);
  EXPECT_EQ(dc->as_string(), nullptr);
  EXPECT_EQ(dc->as_int(), nullptr);
  EXPECT_EQ(dc->as_bool(), nullptr);
  EXPECT_EQ(dc->get_type(), 'D');

  dc->push_back(true);
  EXPECT_EQ(dc->size(), 0);
  dc->push_back(1);
  EXPECT_EQ(dc->size(), 0);
  dc->push_back(hello);
  EXPECT_EQ(dc->size(), 0);

  for (int i = 0; i < 300; i++) {
    double d = i * 4.50;
    dc->push_back(d);
  }

  EXPECT_EQ(dc->size(), 300);

  DoubleColumn *dcc = dc->as_double();
  for (int i = 0; i < 300; i++) {
    double d = i * 4.50;
    EXPECT_EQ(dcc->get(i), d);
  }
}

TEST_F(PersonalFixtures, varargs) {
  StringColumn *sc = new StringColumn(2, hello, world);
  EXPECT_EQ(sc->size(), 2);
  EXPECT_EQUAL(sc->get(0), hello);
  EXPECT_EQUAL(sc->get(1), world);
  delete sc;

  DoubleColumn *dc = new DoubleColumn(2, 1.00, 3.00);
  EXPECT_EQ(dc->size(), 2);
  EXPECT_EQ(dc->get(0), 1.00);
  EXPECT_EQ(dc->get(1), 3.00);
  delete dc;

  BoolColumn *bc = new BoolColumn(3, false, false, true);
  EXPECT_EQ(bc->size(), 3);
  EXPECT_EQ(bc->get(0), false);
  EXPECT_EQ(bc->get(1), false);
  EXPECT_EQ(bc->get(2), true);
  delete bc;

  IntColumn *ic = new IntColumn(3, 3, 4, 7);
  EXPECT_EQ(ic->size(), 3);
  EXPECT_EQ(ic->get(0), 3);
  EXPECT_EQ(ic->get(1), 4);
  EXPECT_EQ(ic->get(2), 7);
  delete ic;
}

/****************************************************************************
 * Row specific tests
 */
TEST_F(PersonalFixtures, row_set) {
  r1->set(0, 123);
  EXPECT_EQ(r1->get_int(0), 123);

  r1->set(1, false);
  EXPECT_EQ(r1->get_bool(1), false);

  r1->set(2, 123.456);
  EXPECT_EQ(r1->get_double(2), 123.456);

  String *testStr = new String("test");
  r1->set(3, testStr);
  EXPECT_EQUAL(r1->get_string(3), testStr);

  delete testStr;
}

TEST_F(PersonalFixtures, row_idx) {
  r1->set_idx(100);

  EXPECT_EQ(r1->get_idx(), 100);
}

TEST_F(PersonalFixtures, row_width) {
  Schema *tenCols = new Schema("SSSSSSSSSS");
  Row *r3 = new Row(*tenCols);

  EXPECT_EQ(r1->width(), 4);
  EXPECT_EQ(r3->width(), 10);

  delete tenCols;
  delete r3;
}

TEST_F(PersonalFixtures, row_col_type) {
  EXPECT_EQ(r1->col_type(0), 'I');
  EXPECT_EQ(r1->col_type(1), 'B');
  EXPECT_EQ(r1->col_type(2), 'D');
  EXPECT_EQ(r1->col_type(3), 'S');
}

TEST_F(PersonalFixtures, row_equals) {
  Row *r3 = new Row(*r1);

  EXPECT_EQUAL(r3, r1);

  delete r3;
}

/****************************************************************************
 * Schema specific tests
 */

TEST_F(PersonalFixtures, schema_coltype) {
  EXPECT_EQ(schemaTwo->col_type(0), 'I');
  EXPECT_EQ(schemaTwo->col_type(1), 'S');
  EXPECT_EQ(schemaTwo->col_type(2), 'D');
  EXPECT_EQ(schemaTwo->col_type(3), 'B');
}

TEST_F(PersonalFixtures, schema_width) {
  EXPECT_EQ(schema->width(), 0);
  EXPECT_EQ(schemaTwo->width(), 4);

  schemaTwo->add_column('B');

  EXPECT_EQ(schemaTwo->width(), 5);
}

TEST_F(PersonalFixtures, schema_equals) {
  Schema *testSchema = new Schema(*schemaTwo);

  EXPECT_EQUAL(testSchema, schemaTwo);

  delete testSchema;
}

/****************************************************************************
 * DataFrame specific tests
 */
TEST_F(PersonalFixtures, df_get_schema) {
  Schema *df_schema = &(df->get_schema());
  EXPECT_EQUAL(df_schema, sch);
}

TEST_F(PersonalFixtures, df_add_column) {
  IntColumn *ic2 = new IntColumn();

  EXPECT_EQ(df->ncols(), 4);

  sch->add_column('I');
  df->add_column(ic2);

  // test that dataframe updates the schema
  Schema *df_schema = &(df->get_schema());
  EXPECT_EQUAL(df_schema, sch);

  // test that dataframe increased column size
  EXPECT_EQ(df->ncols(), 5);
  delete ic2;
}

TEST_F(PersonalFixtures, df_get_elements) {
  df->add_row(*r1);
  df->add_row(*r2);

  EXPECT_EQ(df->get_int(0, 0), 10);
  EXPECT_EQ(df->get_bool(1, 0), true);
  EXPECT_EQ(df->get_double(2, 0), 42.24);
  EXPECT_EQUAL(df->get_string(3, 0), hello);

  EXPECT_EQ(df->get_int(0, 1), 20);
  EXPECT_EQ(df->get_bool(1, 1), false);
  EXPECT_EQ(df->get_double(2, 1), 66.66);
  EXPECT_EQUAL(df->get_string(3, 1), world);
}

TEST_F(PersonalFixtures, df_set) {
  df->add_row(*r1);
  df->add_row(*r2);

  String *nString = new String("abcdefg");

  EXPECT_EQ(df->get_int(0, 0), 10);
  EXPECT_EQ(df->get_bool(1, 0), true);
  EXPECT_EQ(df->get_double(2, 0), 42.24);
  EXPECT_EQUAL(df->get_string(3, 0), hello);

  df->set(0, 0, 100);
  df->set(1, 0, false);
  df->set(2, 0, 100.5f);
  df->set(3, 0, nString);

  EXPECT_EQ(df->get_int(0, 0), 100);
  EXPECT_EQ(df->get_bool(1, 0), false);
  EXPECT_EQ(df->get_double(2, 0), 100.5);
  EXPECT_EQUAL(df->get_string(3, 0), nString);

  delete nString;
}

TEST_F(PersonalFixtures, df_fill_row) {
  // add rows to dataframe
  df->add_row(*r1);
  df->add_row(*r2);

  // create row to fill
  Row *r3 = new Row(*sch);

  df->fill_row(0, *r3);
  EXPECT_EQUAL(r3, r1);

  df->fill_row(1, *r3);
  EXPECT_EQUAL(r3, r2);

  delete r3;
}

TEST_F(PersonalFixtures, df_add_row) {
  // TODO
}

TEST_F(PersonalFixtures, df_nrows) {
  EXPECT_EQ(df->nrows(), 0);

  df->add_row(*r1);
  EXPECT_EQ(df->nrows(), 1);

  df->add_row(*r2);
  EXPECT_EQ(df->nrows(), 2);
}

TEST_F(PersonalFixtures, df_ncols) {
  EXPECT_EQ(df->ncols(), 4);

  IntColumn *ic2 = new IntColumn();
  df->add_column(ic2);
  EXPECT_EQ(df->ncols(), 5);
  delete ic2;
}

TEST_F(PersonalFixtures, df_map) {
  df->add_row(*r1);
  df->add_row(*r2);

  Rower *addRower = new AddRower(5, df);
  df->map(*addRower);

  String *strExpected = new String("<15><1><47.24><hello>\n"
                                   "<25><0><71.66><world>\n");
  char *expected = strExpected->c_str();

  char *dfStr = df->c_str();
  EXPECT_STREQ(dfStr, expected);
  delete[] dfStr;

  delete addRower;
  delete strExpected;
}

TEST_F(PersonalFixtures, df_pmap) {
  df->add_row(*r1);
  df->add_row(*r2);

  Rower *addRower = new AddRower(5, df);
  df->pmap(*addRower);

  String *strExpected = new String("<15><1><47.24><hello>\n"
                                   "<25><0><71.66><world>\n");
  char *expected = strExpected->c_str();

  char *dfStr = df->c_str();
  EXPECT_STREQ(dfStr, expected);
  delete[] dfStr;

  delete addRower;
  delete strExpected;
}

TEST_F(PersonalFixtures, df_filter) {
  df->add_row(*r1);
  df->add_row(*r2);

  Rower *filter = new FilterTrueRower();
  DataFrame *filtered = df->filter(*filter);

  String *strExpected = new String("<10><1><42.24><hello>\n");
  char *expected = strExpected->c_str();

  char *asStr = filtered->c_str();
  EXPECT_STREQ(asStr, expected);
  delete[] asStr;

  delete filter;
  delete strExpected;
  delete filtered;
}

TEST_F(PersonalFixtures, df_c_str) {
  df->add_row(*r1);
  df->add_row(*r2);

  String *strExpected = new String("<10><1><42.24><hello>\n"
                                   "<20><0><66.66><world>\n");
  char *expected = strExpected->c_str();

  char *dfStr = df->c_str();
  EXPECT_STREQ(dfStr, expected);
  delete[] dfStr;
  delete strExpected;
}

TEST_F(PersonalFixtures, df_equals) {
  DataFrame *df2 = new DataFrame(*sch);

  EXPECT_EQUAL(df, df2);

  df->add_row(*r1);
  df2->add_row(*r1);

  EXPECT_EQUAL(df, df2);

  IntColumn *ic2 = new IntColumn();

  // add rows to the column so that it matches the schema of the dataframes
  ic2->push_back(10);

  df->add_column(ic2);
  df2->add_column(ic2);

  EXPECT_EQUAL(df, df2);

  delete df2;
  delete ic2;
}

TEST_F(PersonalFixtures, df_splitJoin) {
  size_t num_rows = 103;
  size_t max_rows = 21;
  // add r1 8 times
  for (size_t i = 0; i < num_rows; i++) {
    if (i % 2 == 0) {
      df->add_row(*r1);
    } else {
      df->add_row(*r2);
    }
  }

  std::vector<DataFrame *> dfs = df->split(max_rows);
  size_t num_dataframes = num_rows / max_rows;
  if (num_rows % max_rows != 0) {
    num_dataframes++;
  }

  EXPECT_TRUE(dfs.size() == num_dataframes);

  DataFrame *df2 = DataFrame::join(dfs);

  EXPECT_EQUAL(df, df2);

  delete df2;
}

TEST(piazza, rower) {
  // Creating a data frame with the right structure
  Schema scm("IDBII"); // the schema
  DataFrame df(scm);   // the data frame

  // Populating the data frame can be done  in different ways, let's assume
  // there is data in it

  // Computing the taxes
  class Taxes : public Rower {
  public:
    DataFrame *df_;
    Taxes(DataFrame *df) : df_(df) {}
    size_t salary = 0, rate = 1, isded = 2, ded = 3,
           taxes = 4; // these are for convenience
    bool accept(Row &r) override {
      int tx = (int)(r.get_int(salary) * r.get_double(rate));
      tx -= r.get_bool(isded) ? r.get_int(ded) : 0;
      df_->set(taxes, r.get_idx(), tx);
      return true;
    }

    void join_delete(Rower *other) override {}

    Taxes *dup() { return new Taxes(df_); }
  };

  Row jan(scm);
  jan.set(0, 6);
  jan.set(1, 2.50);
  jan.set(2, true);
  jan.set(3, 1);
  jan.set(4, 0);

  df.add_row(jan);
  Taxes tx(&df); // create our rower
  df.map(tx);    // apply it to every row

  EXPECT_EQ(df.get_int(4, 0), 14);
}
