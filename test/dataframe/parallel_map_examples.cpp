// LANGUAGE: Cpp
#include "../../src/dataframe/dataframe.h"
#include "testRowers.h"
#include "../../googletest-src/googletest/include/gtest/gtest.h"

#define EXPECT_EQUAL(a, b) EXPECT_TRUE(a->equals(b) && b->equals(a))
#define ASSERT_EQUAL(a, b) ASSERT_TRUE(a->equals(b) && b->equals(a))

class PersonalFixtures : public ::testing::Test {
protected:
  String *hello, *world;
  Column *ic, *sc, *fc, *bc;
  Row *r1, *r2;
  Schema *sch;
  DataFrame *df, *df2;
  Rower *rower, *rower2;

  void SetUp() override {
    hello = new String("hello");
    world = new String("world");

    sch = new Schema("IBDS");

    r1 = new Row(*sch);
    r1->set(0, 10);
    r1->set(1, true);
    r1->set(2, 42.24);
    r1->set(3, hello);

    r2 = new Row(*sch);
    r2->set(0, 20);
    r2->set(1, false);
    r2->set(2, 66.66);
    r2->set(3, world);

    df = new DataFrame(*sch);
    df2 = new DataFrame(*sch);

    rower = new AddRower(5, df);
    rower2 = new AddRower(5, df2);
  }

  void TearDown() override {
    delete hello;
    delete world;
    delete sch;
    delete df;
    delete df2;
    delete r1;
    delete r2;
    delete rower;
    delete rower2;
  }
};

TEST_F(PersonalFixtures, map_1) {
  // add 2,000,000 rows
  for (size_t i = 0; i < 2000000; i++) {
    df->add_row(*r1);
  }

  df->map(*rower);
}

TEST_F(PersonalFixtures, pmap_1) {
  // add 2,000,000 rows
  for (size_t i = 0; i < 2000000; i++) {
    df->add_row(*r1);
  }
  df->pmap(*rower);
}

TEST_F(PersonalFixtures, map_2) {
  // add 50,000 rows
  for (size_t i = 0; i < 50000; i++) {
    df->add_row(*r1);
  }
  df->map(*rower);
}

TEST_F(PersonalFixtures, pmap_2) {
  // add 50,000 rows
  for (size_t i = 0; i < 50000; i++) {
    df->add_row(*r1);
  }
  df->pmap(*rower);
}

TEST_F(PersonalFixtures, pmap_map_equal) {
  // add 2,000,000 rows
  for (size_t i = 0; i < 2000000; i++) {
    df->add_row(*r1);
    df2->add_row(*r1);
  }
  df->map(*rower);
  df2->pmap(*rower2);

  ASSERT_EQUAL(df, df2);
}
