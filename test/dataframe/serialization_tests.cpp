// LANGUAGE: Cpp
#include <stdlib.h>

#include "../../googletest-src/googletest/include/gtest/gtest.h"
#include "../../src/dataframe/columns/int.h"
#include "../../src/dataframe/dataframe.h"
#include "../../src/util/string.h"

#define EXPECT_EQUAL(a, b) EXPECT_TRUE(a->equals(b) && b->equals(a))
#define ASSERT_EQUAL(a, b) ASSERT_TRUE(a->equals(b) && b->equals(a))

String *gen_random_string(const int len) {
  char *s = new char[len];
  static const char alphanum[] = "0123456789"
                                 "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                 "abcdefghijklmnopqrstuvwxyz";

  for (int i = 0; i < len - 1; ++i) {
    s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
  }

  s[len - 1] = 0;
  String *ret = new String(true, s, len - 1);

  return ret;
}

//=================STORAGE SERIALIZATION TESTS=======================
TEST(serialization, IntStorage) {
  // create an int storage
  size_t storageSize = 1000;

  IntStorage *s1 = new IntStorage();

  for (size_t i = 0; i < storageSize; i++) {
    int random = rand() % 100;
    s1->push_back(random);
  }

  // serialize the int storage
  char *serialized = s1->serialize();

  IntStorage *s2 = IntStorage::deserialize(serialized);

  EXPECT_EQUAL(s1, s2);

  delete s1;
  delete s2;
  delete[] serialized;
}

TEST(serialization, BoolStorage) {
  // create an int storage
  size_t storageSize = 1000;

  BoolStorage *s1 = new BoolStorage();

  for (size_t i = 0; i < storageSize; i++) {
    bool random = rand() % 2;
    s1->push_back(random);
  }

  // serialize the int storage
  char *serialized = s1->serialize();

  BoolStorage *s2 = BoolStorage::deserialize(serialized);

  EXPECT_EQUAL(s1, s2);

  delete s1;
  delete s2;
  delete[] serialized;
}

TEST(serialization, DoubleStorage) {
  // create an int storage
  size_t storageSize = 1000;

  DoubleStorage *s1 = new DoubleStorage();

  for (size_t i = 0; i < storageSize; i++) {
    double random = (rand() % 1000) * 0.10;
    s1->push_back(random);
  }

  // serialize the int storage
  char *serialized = s1->serialize();

  DoubleStorage *s2 = DoubleStorage::deserialize(serialized);

  EXPECT_EQUAL(s1, s2);

  delete s1;
  delete s2;
  delete[] serialized;
}

TEST(serialization, StringStorage) {
  // create an int storage
  size_t storageSize = 1000;

  StringStorage *s1 = new StringStorage();

  for (size_t i = 0; i < storageSize; i++) {
    String *random = gen_random_string(10);
    s1->push_back(random);
  }

  // serialize the int storage
  char *serialized = s1->serialize();

  StringStorage *s2 = StringStorage::deserialize(serialized);

  EXPECT_EQUAL(s1, s2);

  delete s1;
  delete s2;
  delete[] serialized;
}

TEST(serialization, ColumnStorage) {
  // specify size of all columns
  size_t columnSize = 1000;

  ColumnStorage *s1 = new ColumnStorage();

  // create 1 column of each type
  BoolColumn *bc = new BoolColumn();
  DoubleColumn *dc = new DoubleColumn();
  IntColumn *ic = new IntColumn();
  StringColumn *sc = new StringColumn();

  // fill the columns with random values
  for (size_t i = 0; i < columnSize; i++) {
    bool randomBool = rand() % 2;
    bc->push_back(randomBool);

    double randomDouble = (rand() % 1000) * 0.10;
    dc->push_back(randomDouble);

    int randomInt = rand() % 100;
    ic->push_back(randomInt);

    String *randomString = gen_random_string(10);
    sc->push_back(randomString);
    delete randomString;
  }

  s1->push_back(bc);
  s1->push_back(dc);
  s1->push_back(ic);
  s1->push_back(sc);

  // serialize the column storage
  char *serialized = s1->serialize();

  ColumnStorage *s2 = ColumnStorage::deserialize(serialized);

  EXPECT_EQUAL(s1, s2);

  delete[] serialized;
  delete bc;
  delete dc;
  delete ic;
  delete sc;

  delete s1;
  s2->deleteContents(); // TODO: this should go in ColumnStorage/ObjectStorage
  delete s2;
}

//=================COLUMN SERIALIZATION TESTS=======================
TEST(serialization, BoolColumn) {
  size_t columnSize = 1000;

  BoolColumn *c1 = new BoolColumn();

  for (size_t i = 0; i < columnSize; i++) {
    bool random = rand() % 2;
    c1->push_back(random);
  }

  // serialize the schema
  char *serialized = c1->serialize();

  BoolColumn *c2 = BoolColumn::deserialize(serialized);

  EXPECT_EQUAL(c1, c2);

  delete c1;
  delete c2;
  delete[] serialized;
}

TEST(serialization, DoubleColumn) {
  size_t columnSize = 1000;

  DoubleColumn *c1 = new DoubleColumn();

  for (size_t i = 0; i < columnSize; i++) {
    double random = (rand() % 1000) * 0.10;
    c1->push_back(random);
  }

  // serialize the schema
  char *serialized = c1->serialize();

  DoubleColumn *c2 = DoubleColumn::deserialize(serialized);

  EXPECT_EQUAL(c1, c2);

  delete c1;
  delete c2;
  delete[] serialized;
}

TEST(serialization, IntColumn) {
  size_t columnSize = 1000;

  IntColumn *c1 = new IntColumn();

  for (size_t i = 0; i < columnSize; i++) {
    int random = rand() % 100;
    c1->push_back(random);
  }

  // serialize the schema
  char *serialized = c1->serialize();

  IntColumn *c2 = IntColumn::deserialize(serialized);

  EXPECT_EQUAL(c1, c2);

  delete c1;
  delete c2;
  delete[] serialized;
}

TEST(serialization, StringColumn) {
  size_t columnSize = 1000;

  StringColumn *c1 = new StringColumn();

  for (size_t i = 0; i < columnSize; i++) {
    String *random = gen_random_string(10);
    c1->push_back(random);
    delete random;
  }

  // serialize the schema
  char *serialized = c1->serialize();

  StringColumn *c2 = StringColumn::deserialize(serialized);

  EXPECT_EQUAL(c1, c2);

  delete c1;
  delete c2;
  delete[] serialized;
}

//=================SCHEMA SERIALIZATION TESTS=======================
TEST(serialization, Schema) {
  Schema *s1 = new Schema("IBFBSIFBS");

  // serialize the schema
  char *serialized = s1->serialize();

  Schema *s2 = Schema::deserialize(serialized);

  EXPECT_EQUAL(s1, s2);

  delete s1;
  delete s2;
  delete[] serialized;
}

//=================DATAFRAME SERIALIZATION TESTS=======================
TEST(serialization, DataFrame) {
  Schema *sch = new Schema();
  DataFrame *df = new DataFrame(*sch);

  // specify size of all columns
  size_t columnSize = 1000;

  // create 1 column of each type
  BoolColumn *bc = new BoolColumn();
  DoubleColumn *dc = new DoubleColumn();
  IntColumn *ic = new IntColumn();
  StringColumn *sc = new StringColumn();

  // fill the columns with random values
  for (size_t i = 0; i < columnSize; i++) {
    bool randomBool = rand() % 2;
    bc->push_back(randomBool);

    double randomDouble = (rand() % 1000) * 0.10;
    dc->push_back(randomDouble);

    int randomInt = rand() % 100;
    ic->push_back(randomInt);

    String *randomString = gen_random_string(10);
    sc->push_back(randomString);
    delete randomString;
  }

  // add the columns to the dataframe
  df->add_column(bc);
  df->add_column(dc);
  df->add_column(ic);
  df->add_column(sc);

  // serialize the DataFrame
  char *serialized = df->serialize();

  DataFrame *df2 = DataFrame::deserialize(serialized);

  EXPECT_EQUAL(df, df2);

  delete sch;
  delete df;
  delete df2;
  delete[] serialized;

  delete dc;
  delete bc;
  delete sc;
  delete ic;
}

TEST(serialization, DataFrame_simple) {
  Schema *sch = new Schema("IDBS");
  DataFrame *df = new DataFrame(*sch);

  // serialize the DataFrame
  char *serialized = df->serialize();

  DataFrame *df2 = DataFrame::deserialize(serialized);

  EXPECT_EQUAL(df, df2);

  delete sch;
  delete df;
  delete df2;
  delete[] serialized;
}
