#pragma once

#include "../../src/dataframe/dataframe.h"

/*******************************************************************************
 *  Rower::
 *  A rower that increments all ints and floats in a column by the specified
 * amount. This rower is specifically used as a map operation
 */
class AddRower : public Rower {
public:
  int addend_;
  DataFrame *df_;

  AddRower(int addend, DataFrame *df) {
    addend_ = addend;
    df_ = df;
  }

  /** This method is called once per row. The row object is on loan and
    should not be retained as it is likely going to be reused in the next
    call. The return value is used in filters to indicate that a row
    should be kept. */
  virtual bool accept(Row &r) {
    for (size_t i = 0; i < r.width(); i++) {
      char col_type = r.col_type(i);
      if (col_type == 'I') {
        int new_val = r.get_int(i) + addend_;
        df_->set(i, r.get_idx(), new_val);
      } else if (col_type == 'D') {
        float new_val = r.get_double(i) + float(addend_);
        df_->set(i, r.get_idx(), new_val);
      } else if (col_type == 'B') {
        // No add operation for bool
      } else if (col_type == 'S') {
        // No add operation for string
      }
    }

    return true;
  }

  /** Once traversal of the data frame is complete the rowers that were
    split off will be joined.  There will be one join per split. The
    original object will be the last to be called join on. The join method
    is responsible for cleaning up memory. */
  virtual void join_delete(Rower *other) { delete other; }

  virtual AddRower *dup() { return new AddRower(addend_, df_); }
};

/*******************************************************************************
 *  FilterTrueRower::
 *  A rower to be used a filter to only include rows that cannot some true
 * boolean value somewhere in the row
 */
class FilterTrueRower : public Rower {
public:
  FilterTrueRower() {}

  /** This method is called once per row. The row object is on loan and
    should not be retained as it is likely going to be reused in the next
    call. The return value is used in filters to indicate that a row
    should be kept. */
  virtual bool accept(Row &r) {
    for (size_t i = 0; i < r.width(); i++) {
      char col_type = r.col_type(i);
      if (col_type == 'B') {
        if (r.get_bool(i) == true) {
          return true;
        }
      }
    }

    return false;
  }

  /** Once traversal of the data frame is complete the rowers that were
    split off will be joined.  There will be one join per split. The
    original object will be the last to be called join on. The join method
    is responsible for cleaning up memory. */
  virtual void join_delete(Rower *other) { delete other; }

  virtual FilterTrueRower *dup() { return new FilterTrueRower(); }
};
