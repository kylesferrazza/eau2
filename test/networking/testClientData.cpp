#include <unistd.h>
#include <vector>

#include "../../src/debug/print.h"
#include "../../src/networking/client.h"
#include "../../src/networking/network.h"

int main(int argc, char **argv) {
  const char *client_ip = nullptr;
  const char *client_port = nullptr;
  const char *server_ip = nullptr;
  const char *client_idx = nullptr;

  const char *usage = "USAGE: ./client -cip [CLIENT_IP] -cp [CLIENT_PORT] -sip "
                      "[SERVER_IP] -cidx [CLIENT_IDX]";
  if (argc != 9) {
    printf("%s\n", usage);
    exit(0);
  }
  for (size_t i = 1; i < 9; i += 2) {
    const char *flag = argv[i];
    const char *parameter = argv[i + 1];
    if (strcmp(flag, "-cip") == 0) {
      client_ip = parameter;
    } else if (strcmp(flag, "-cp") == 0) {
      client_port = parameter;
    } else if (strcmp(flag, "-sip") == 0) {
      server_ip = parameter;
    } else if (strcmp(flag, "-cidx") == 0) {
      client_idx = parameter;
    } else {
      printf("%s\n", usage);
      exit(0);
    }
  }
  if (client_ip == nullptr || client_port == nullptr || server_ip == nullptr ||
      client_idx == nullptr) {
    printf("%s\n", usage);
    exit(0);
  }

  int clientPort = atoi(client_port);
  size_t clientIdx = atoi(client_idx);

  sockaddr_in serverAddr = createAddress(server_ip, 9000);
  clientDebug("SERVER IS AT IP ", true);
  fprintf(stderr, "(%s) and port (%d)\n", server_ip, 9000);
  sockaddr_in clientAddr = createAddress(client_ip, clientPort);
  clientDebug("STARTING ON IP ", true);
  fprintf(stderr, "(%s) and port (%d)\n", client_ip, clientPort);

  Client *c = new Client(clientAddr, serverAddr, clientIdx, true);
  c->start();

  while (c->getNumPeers() == 0) {
    sleep(1);
    clientDebug("No Peers...", true);
  }
  // We have friends now!

  // send a message to each peer
  for (size_t i = 0; i < c->getNumPeers(); i++) {
    char *msg = (char *)"THIS IS THE FIRST MESSAGE!\n";
    size_t peerIdx = c->peerList().at(i);
    Data *d = new Data(c->nodeIdx_, peerIdx, 0, strlen(msg), msg);
    c->sendData(peerIdx, d);
  }

  sleep(2);

  // send a second message to each peer
  for (size_t i = 0; i < c->getNumPeers(); i++) {
    char *msg = (char *)"THIS IS THE SECOND MESSAGE!\n";
    size_t peerIdx = c->peerList().at(i);
    Data *d = new Data(c->nodeIdx_, peerIdx, 0, strlen(msg), msg);
    c->sendData(peerIdx, d);
  }

  sleep(2);

  // unregister the client
  c->stop();
}
