#include <unistd.h>
#include <vector>

#include "../../src/debug/print.h"
#include "../../src/networking/client.h"
#include "../../src/networking/network.h"
#include "../../src/store/storeTypes.h"

int main(int argc, char **argv) {
  const char *client_ip = nullptr;
  const char *client_port = nullptr;
  const char *server_ip = nullptr;
  const char *client_idx = nullptr;

  const char *usage = "USAGE: ./client -cip [CLIENT_IP] -cp [CLIENT_PORT] -sip "
                      "[SERVER_IP] -cidx [CLIENT_IDX]";
  if (argc != 9) {
    printf("%s\n", usage);
    exit(0);
  }
  for (size_t i = 1; i < 9; i += 2) {
    const char *flag = argv[i];
    const char *parameter = argv[i + 1];
    if (strcmp(flag, "-cip") == 0) {
      client_ip = parameter;
    } else if (strcmp(flag, "-cp") == 0) {
      client_port = parameter;
    } else if (strcmp(flag, "-sip") == 0) {
      server_ip = parameter;
    } else if (strcmp(flag, "-cidx") == 0) {
      client_idx = parameter;
    } else {
      printf("%s\n", usage);
      exit(0);
    }
  }
  if (client_ip == nullptr || client_port == nullptr || server_ip == nullptr ||
      client_idx == nullptr) {
    printf("%s\n", usage);
    exit(0);
  }

  int clientPort = atoi(client_port);
  size_t clientIdx = atoi(client_idx);

  sockaddr_in serverAddr = createAddress(server_ip, 9000);
  clientDebug("SERVER IS AT IP ", true);
  fprintf(stderr, "(%s) and port (%d)\n", server_ip, 9000);
  sockaddr_in clientAddr = createAddress(client_ip, clientPort);
  clientDebug("STARTING ON IP ", true);
  fprintf(stderr, "(%s) and port (%d)\n", client_ip, clientPort);

  sleep(1);
  Client *c = new Client(clientAddr, serverAddr, clientIdx, true);
  c->start();

  while (c->getNumPeers() == 0) {
    sleep(1);
  }
  // We have friends now!

  // send a each of the extra message types to the other peer
  // if this client has idx 1
  if (c->nodeIdx_ == 1) {
    size_t peerIdx = c->peerList().at(0);

    char *blob = new char[5];
    blob[0] = 'b';
    blob[1] = 'l';
    blob[2] = 'o';
    blob[3] = 'b';
    blob[4] = '\0';

    char *key = new char[2];
    key[0] = 'k';
    key[1] = '\0';

    Put *p =
        new Put(c->nodeIdx_, peerIdx, 0, blob, 5, key, StoreTypes::DATAFRAME);
    Get *g = new Get(c->nodeIdx_, peerIdx, 0, key, StoreTypes::DATAFRAME);
    WaitAndGet *wag =
        new WaitAndGet(c->nodeIdx_, peerIdx, 0, key, StoreTypes::DATAFRAME);
    Reply *r =
        new Reply(c->nodeIdx_, peerIdx, 0, blob, 5, StoreTypes::DATAFRAME);

    // send each message type to the other peer
    c->sendPut(peerIdx, p);
    c->sendGet(peerIdx, g);
    c->sendWaitAndGet(peerIdx, wag);
    c->sendReply(peerIdx, r);
  }

  sleep(5);

  // unregister the client
  c->stop();
}
