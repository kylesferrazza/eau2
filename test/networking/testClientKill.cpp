#include <unistd.h>
#include <vector>

#include "../../src/debug/print.h"
#include "../../src/networking/client.h"
#include "../../src/networking/network.h"

const char *client_ip = nullptr;
const char *client_port = nullptr;
const char *server_ip = nullptr;
const char *client_idx = nullptr;

void parseArgs(int argc, char **argv) {
  const char *usage = "USAGE: ./client -cip [CLIENT_IP] -cp [CLIENT_PORT] -sip "
                      "[SERVER_IP] -cidx [CLIENT_IDX]";
  if (argc != 9) {
    printf("%s\n", usage);
    exit(0);
  }
  for (size_t i = 1; i < 9; i += 2) {
    const char *flag = argv[i];
    const char *parameter = argv[i + 1];
    if (strcmp(flag, "-cip") == 0) {
      client_ip = parameter;
    } else if (strcmp(flag, "-cp") == 0) {
      client_port = parameter;
    } else if (strcmp(flag, "-sip") == 0) {
      server_ip = parameter;
    } else if (strcmp(flag, "-cidx") == 0) {
      client_idx = parameter;
    } else {
      printf("%s\n", usage);
      exit(0);
    }
  }
  if (client_ip == nullptr || client_port == nullptr || server_ip == nullptr ||
      client_idx == nullptr) {
    printf("%s\n", usage);
    exit(0);
  }
}

int main(int argc, char **argv) {
  parseArgs(argc, argv);

  int clientPort = atoi(client_port);
  size_t clientIdx = atoi(client_idx);

  sockaddr_in serverAddr = createAddress(server_ip, 9000);
  clientDebug("SERVER IS AT IP ", true);
  fprintf(stderr, "(%s) and port (%d)\n", server_ip, 9000);
  sockaddr_in clientAddr = createAddress(client_ip, clientPort);
  clientDebug("STARTING ON IP ", true);
  fprintf(stderr, "(%s) and port (%d)\n", client_ip, clientPort);

  sleep(1);
  Client *c = new Client(clientAddr, serverAddr, clientIdx, true);
  c->start();

  // wait for peers to connect
  while (c->getNumPeers() == 0) {
    sleep(1);
  }

  // wait until client is shut down by server
  while (c->isRunning()) {
    sleep(1);
  }

  // print number of peers (should be 0);
  fprintf(stderr, "Number of peers: %zu\n", c->getNumPeers());

  // try to send a message (should print that client is shut down)
  char *msg = (char *)"SHOULDN'T WORK!\n";
  Data *d = new Data(c->nodeIdx_, 0, 1, strlen(msg), msg);
  c->sendData(0, d);
}
