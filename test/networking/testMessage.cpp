#include <cstdio>
#include <cstring>

#include "../../src/networking/message/data.h"
#include "../../src/networking/message/directory.h"
#include "../../src/networking/message/get.h"
#include "../../src/networking/message/kill.h"
#include "../../src/networking/message/put.h"
#include "../../src/networking/message/register.h"
#include "../../src/networking/message/reply.h"
#include "../../src/networking/message/unregister.h"
#include "../../src/networking/message/waitandget.h"
#include "../../src/networking/network.h"
#include "../../src/store/storeTypes.h"

#define RED fprintf(stderr, "\033[0;31m")
#define GREEN fprintf(stderr, "\033[0;32m")
#define NORMAL fprintf(stderr, "\033[0m")

size_t sender = 236;
size_t target = 110;
size_t id = 3;

void testTrue(const char *name, bool a) {
  if (a) {
    fprintf(stderr, "[");
    GREEN;
    fprintf(stderr, "SUCCESS");
    NORMAL;
    fprintf(stderr, "] %s\n", name);
  } else {
    fprintf(stderr, "[");
    RED;
    fprintf(stderr, "FAILURE");
    NORMAL;
    fprintf(stderr, "] %s\n", name);
  }
}

void testBaseMessage(Message *deserialized, MsgKind kind) {
  testTrue("kind", deserialized->kind_ == kind);
  testTrue("sender", deserialized->sender_ == sender);
  testTrue("target", deserialized->target_ == target);
  testTrue("id", deserialized->id_ == id);
}

void start(const char *name) {
  fprintf(stderr, "======= %s TESTS =======\n", name);
}

void reg() {
  start("Register message");
  MsgKind kind = MsgKind::REGISTER;
  sockaddr_in client = createAddress("127.0.0.1", 8080);
  Register *m = new Register(sender, target, id, client);

  char *serialized = m->serialize();

  Message *deserialized = Message::deserialize(serialized);

  testBaseMessage(deserialized, kind);

  Register *r = dynamic_cast<Register *>(deserialized);

  testTrue("client.sin_family", r->client_.sin_family == client.sin_family);
  testTrue("client.sin_port", r->client_.sin_port == client.sin_port);
  testTrue("client.sin_addr.s_addr",
           r->client_.sin_addr.s_addr == client.sin_addr.s_addr);

  delete m;
  delete[] serialized;
  delete r;
}

void dir() {
  start("Directory message");
  MsgKind kind = MsgKind::DIRECTORY;
  size_t numClients = 3;
  sockaddr_in *clientAddrs = new sockaddr_in[numClients];
  clientAddrs[0] = createAddress("127.0.0.1", 8080);
  clientAddrs[1] = createAddress("127.0.0.2", 8081);
  clientAddrs[2] = createAddress("127.0.0.3", 8082);

  size_t *clientIdxs = new size_t[numClients];
  clientIdxs[0] = 1;
  clientIdxs[1] = 2;
  clientIdxs[2] = 3;

  Directory *m =
      new Directory(sender, target, id, numClients, clientAddrs, clientIdxs);

  char *serialized = m->serialize();

  Message *deserialized = Message::deserialize(serialized);

  testBaseMessage(deserialized, kind);

  Directory *d = dynamic_cast<Directory *>(deserialized);

  testTrue("numClients", d->numClients_ == numClients);
  for (int i = 0; i < numClients; i++) {
    sockaddr_in realClient = clientAddrs[i];
    sockaddr_in deserializedClient = d->clientAddrs_[i];
    fprintf(stderr, "--> ITEM [%d]\n", i);
    testTrue("clientAddrs[i].sin_family",
             deserializedClient.sin_family == realClient.sin_family);
    testTrue("clientAddrs[i].sin_port",
             deserializedClient.sin_port == realClient.sin_port);
    testTrue("clientAddrs[i].sin_addr.s_addr",
             deserializedClient.sin_addr.s_addr == realClient.sin_addr.s_addr);

    size_t realIdx = clientIdxs[i];
    size_t deserializedIdx = d->clientIdxs_[i];
    testTrue("clientIdxs[i]", realIdx == deserializedIdx);
  }

  delete m;
  delete[] serialized;
  delete d;
}

void data() {
  start("Data message");
  MsgKind kind = MsgKind::DATA;
  size_t dataLength = 11;
  char *data = new char[dataLength];
  data[0] = 'T';
  data[1] = 'e';
  data[2] = 's';
  data[3] = 't';
  data[4] = ' ';
  data[5] = 'd';
  data[6] = 'a';
  data[7] = 't';
  data[8] = 'a';
  data[9] = '.';
  data[10] = 0;

  Data *m = new Data(sender, target, id, dataLength, data);
  char *serialized = m->serialize();

  Message *deserialized = Message::deserialize(serialized);

  testBaseMessage(deserialized, kind);

  Data *d = dynamic_cast<Data *>(deserialized);

  testTrue("dataLength", d->dataLength_ == dataLength);
  for (int i = 0; i < dataLength; i++) {
    char realChar = data[i];
    char deserializedChar = d->data_[i];
    fprintf(stderr, "--> CHAR [%d]\n", i);
    testTrue("equals original", realChar == deserializedChar);
  }

  delete m;
  delete[] serialized;
  delete d;
}

void kill() {
  start("Kill message");
  MsgKind kind = MsgKind::KILL;

  Kill *m = new Kill(sender, target, id);
  char *serialized = m->serialize();

  Message *deserialized = Message::deserialize(serialized);

  testBaseMessage(deserialized, kind);

  Kill *k = dynamic_cast<Kill *>(deserialized);

  delete m;
  delete[] serialized;
  delete k;
}

void unreg() {
  start("Unregister message");
  MsgKind kind = MsgKind::UNREGISTER;
  sockaddr_in client = createAddress("127.0.0.1", 8080);

  Unregister *m = new Unregister(sender, target, id, client);

  char *serialized = m->serialize();

  Message *deserialized = Message::deserialize(serialized);

  testBaseMessage(deserialized, kind);

  Unregister *u = dynamic_cast<Unregister *>(deserialized);

  testTrue("client.sin_family", u->client_.sin_family == client.sin_family);
  testTrue("client.sin_port", u->client_.sin_port == client.sin_port);
  testTrue("client.sin_addr.s_addr",
           u->client_.sin_addr.s_addr == client.sin_addr.s_addr);

  delete m;
  delete[] serialized;
  delete u;
}

void put() {
  start("put message");
  MsgKind kind = MsgKind::PUT;

  char *blob = new char[5];
  blob[0] = 'b';
  blob[1] = 'l';
  blob[2] = 'o';
  blob[3] = 'b';
  blob[4] = '\0';

  char *key = new char[2];
  key[0] = 'k';
  key[1] = '\0';

  Put *m = new Put(sender, target, id, blob, 5, key, StoreTypes::DATAFRAME);

  char *serialized = m->serialize();

  Message *deserialized = Message::deserialize(serialized);

  testBaseMessage(deserialized, kind);

  Put *p = dynamic_cast<Put *>(deserialized);

  testTrue("blob", strcmp(p->blob_, m->blob_) == 0);
  testTrue("blobSize", p->blobSize_ == m->blobSize_);
  testTrue("key", strcmp(p->key_, m->key_) == 0);
  testTrue("storeType", p->type_ == StoreTypes::DATAFRAME);

  delete m;
  delete[] serialized;
  delete p;
}

void get() {
  start("get message");
  MsgKind kind = MsgKind::GET;

  char *key = new char[2];
  key[0] = 'k';
  key[1] = '\0';

  Get *m = new Get(sender, target, id, key, StoreTypes::DATAFRAME);

  char *serialized = m->serialize();

  Message *deserialized = Message::deserialize(serialized);

  testBaseMessage(deserialized, kind);

  Get *g = dynamic_cast<Get *>(deserialized);

  testTrue("key", strcmp(g->key_, m->key_) == 0);
  testTrue("storeType", g->type_ == StoreTypes::DATAFRAME);

  delete m;
  delete[] serialized;
  delete g;
}

void waitAndGet() {
  start("waitAndGet message");
  MsgKind kind = MsgKind::WAITANDGET;

  char *key = new char[2];
  key[0] = 'k';
  key[1] = '\0';

  WaitAndGet *m =
      new WaitAndGet(sender, target, id, key, StoreTypes::DATAFRAME);

  char *serialized = m->serialize();

  Message *deserialized = Message::deserialize(serialized);

  testBaseMessage(deserialized, kind);

  WaitAndGet *wag = dynamic_cast<WaitAndGet *>(deserialized);

  testTrue("key", strcmp(wag->key_, m->key_) == 0);
  testTrue("storeType", wag->type_ == StoreTypes::DATAFRAME);

  delete m;
  delete[] serialized;
  delete wag;
}

void reply() {
  start("reply message");
  MsgKind kind = MsgKind::REPLY;

  char *blob = new char[5];
  blob[0] = 'b';
  blob[1] = 'l';
  blob[2] = 'o';
  blob[3] = 'b';
  blob[4] = '\0';

  Reply *m = new Reply(sender, target, id, blob, 5, StoreTypes::DATAFRAME);

  char *serialized = m->serialize();

  Message *deserialized = Message::deserialize(serialized);

  testBaseMessage(deserialized, kind);

  Reply *r = dynamic_cast<Reply *>(deserialized);

  testTrue("blob", strcmp(r->blob_, m->blob_) == 0);
  testTrue("blobSize", r->blobSize_ == m->blobSize_);
  testTrue("storeType", r->type_ == StoreTypes::DATAFRAME);

  delete m;
  delete[] serialized;
  delete r;
}

int main() {
  reg();
  dir();
  data();
  kill();
  unreg();
  put();
  get();
  waitAndGet();
  reply();
}
