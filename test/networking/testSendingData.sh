#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

#tests the functionality of sending data between clients

echo "START: Running Data Networking Test..."
cd ../../bin/
(./testServerData -ip 127.0.0.1) 2>serverOutput.txt &
(./testClientData -cip 127.0.0.1 -cp 1234 -sip 127.0.0.1 -cidx 1) 2>/dev/null &
(./testClientData -cip 127.0.0.1 -cp 1235 -sip 127.0.0.1 -cidx 2) 2>/dev/null &
./testClientData -cip 127.0.0.1 -cp 1236 -sip 127.0.0.1 -cidx 3 2>clientOutput.txt

#check that the client recieved a first message from the other two clients
if [ "$(grep -c "THIS IS THE FIRST MESSAGE!" clientOutput.txt)" -eq 2 ]; then
    echo "[${GREEN}SUCCESS${NC}] correct number of first messages"
else
    echo "[${RED}FAIL${NC}] incorrect number of first messages" 
fi

#check that the client recieved a second message from the other two clients
if [ "$(grep -c "THIS IS THE SECOND MESSAGE!" clientOutput.txt)" -eq 2 ]; then
    echo "[${GREEN}SUCCESS${NC}] correct number of second messages"
else
    echo "[${RED}FAIL${NC}] incorrect number of second messages" 
fi

#check that the server received three registrations
if [ "$(grep -c "HANDLING INCOMING REGISTRATION" serverOutput.txt)" -eq 3 ]; then
    echo "[${GREEN}SUCCESS${NC}] server received three registrations"
else
    echo "[${RED}FAIL${NC}] server did not receive three registrations" 
fi

#check that the server recieved three unregistrations
if [ "$(grep -c "HANDLING INCOMING UNREGISTRATION" serverOutput.txt)" -eq 3 ]; then
    echo "[${GREEN}SUCCESS${NC}] server received three unregistrations"
else
    echo "[${RED}FAIL${NC}] server did not receive three unregistrations" 
fi

#delete the test files that kept the stderr output of the client and server
rm clientOutput.txt serverOutput.txt

