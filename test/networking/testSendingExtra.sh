#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

#tests the functionality of sending data between clients

echo 'START: Running Extra Message Networking Test...'
cd ../../bin/
(./testServerExtra -ip 127.0.0.1) 2>serverOutput.txt &
(./testClientExtra -cip 127.0.0.1 -cp 1234 -sip 127.0.0.1 -cidx 1) 2>/dev/null &
./testClientExtra -cip 127.0.0.1 -cp 1235 -sip 127.0.0.1 -cidx 2 2>clientOutput.txt

#check that the client recieved a put message
if [ "$(grep -c "PUT" clientOutput.txt)" -eq 1 ]; then
    echo "[${GREEN}SUCCESS${NC}] received a put message"
else
    echo "[${RED}FAIL${NC}] did not receive a put message" 
fi

#check that the client recieved a get message
#(2 for GET and WAIT AND GET)
if [ "$(grep -c "GET" clientOutput.txt)" -eq 2 ]; then
    echo "[${GREEN}SUCCESS${NC}] received a get message"
else
    echo "[${RED}FAIL${NC}] did not receive a get message" 
fi

#check that the client recieved a waitAndGet message
if [ "$(grep -c "WAIT AND GET" clientOutput.txt)" -eq 1 ]; then
    echo "[${GREEN}SUCCESS${NC}] received a waitAndGet message"
else
    echo "[${RED}FAIL${NC}] did not receive a waitAndGet message" 
fi

#check that the client recieved a reply message
if [ "$(grep -c "REPLY" clientOutput.txt)" -eq 1 ]; then
    echo "[${GREEN}SUCCESS${NC}] received a reply message"
else
    echo "[${RED}FAIL${NC}] did not receive a reply message" 
fi

#delete the test files that kept the stderr output of the client and server
rm clientOutput.txt serverOutput.txt

