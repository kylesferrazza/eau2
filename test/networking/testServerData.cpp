#include <cstdio>
#include <unistd.h>

#include "../../src/debug/print.h"
#include "../../src/networking/server.h"

int main(int argc, char **argv) {
  const char *server_ip = nullptr;
  const char *usage = "USAGE: ./server -ip [SERVER_IP]";

  if (argc != 3) {
    printf("%s\n", usage);
    exit(0);
  }
  const char *flag = argv[1];
  const char *parameter = argv[2];

  if (strcmp(flag, "-ip") == 0) {
    server_ip = parameter;
  } else {
    printf("%s\n", usage);
    exit(0);
  }

  serverDebug("STARTING ON IP ", true);
  fprintf(stderr, "%s\n", server_ip);
  sockaddr_in server_addr = createAddress(server_ip, 9000);
  Server *s = new Server(server_addr, true);

  s->start();

  // wait for a client to connect
  while (s->getNumClients() == 0) {
    sleep(1);
  }

  size_t numClients = s->getNumClients();
  while (numClients > 0) {
    numClients = s->getNumClients();
  }

  s->stop();
}
