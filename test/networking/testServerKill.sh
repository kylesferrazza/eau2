#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

#tests the functionality of sending data between clients

echo 'START: Running Kill Networking Test...'
cd ../../bin/
(./testServerKill -ip 127.0.0.1) 2>serverOutput.txt &
(./testClientKill -cip 127.0.0.1 -cp 1234 -sip 127.0.0.1 -cidx 1) 2>/dev/null &
(./testClientKill -cip 127.0.0.1 -cp 1235 -sip 127.0.0.1 -cidx 2) 2>/dev/null &
./testClientKill -cip 127.0.0.1 -cp 1236 -sip 127.0.0.1 -cidx 3 2>clientOutput.txt

#check that the client has no peers after being killed
if [ "$(grep -c "Number of peers: 0" clientOutput.txt)" -eq 1 ]; then
    echo "[${GREEN}SUCCESS${NC}] client has no peers after being killed"
else
    echo "[${RED}FAIL${NC}] client has peers after being killed" 
fi

#check that a client sending a message fails after it has been killed
if [ "$(grep -c "Client is currently not running!" clientOutput.txt)" -eq 1 ]; then
    echo "[${GREEN}SUCCESS${NC}] sending a message fails"
else
    echo "[${RED}FAIL${NC}] sending a message does not fail" 
fi

#check that the server sends kill messages
if [ "$(grep -c "SENDING KILL MESSAGES" serverOutput.txt)" -eq 1 ]; then
    echo "[${GREEN}SUCCESS${NC}] server sent kill messages"
else
    echo "[${RED}FAIL${NC}] server did not send kill messages" 
fi

#delete the test files that kept the stderr output of the client and server
rm clientOutput.txt serverOutput.txt

