#include "../../googletest-src/googletest/include/gtest/gtest.h"
#include "../../src/store/kdstore.h"
#include "../../src/store/key.h"
#include "../../src/store/keyList.h"
#include "../../src/store/kklstore.h"

#define EXPECT_EQUAL(a, b) EXPECT_TRUE(a->equals(b) && b->equals(a))
#define ASSERT_EQUAL(a, b) ASSERT_TRUE(a->equals(b) && b->equals(a))

class PersonalFixtures : public ::testing::Test {
protected:
  void SetUp() override {}

  void TearDown() override {}
};

TEST_F(PersonalFixtures, serialize_key) {
  Key *k = new Key("abc", 123);

  char *serializedKey = k->serialize();
  Key *k2 = Key::deserialize(serializedKey);

  EXPECT_EQUAL(k, k2);

  delete[] serializedKey;
  delete k;
  delete k2;
}

TEST_F(PersonalFixtures, serialize_keyList) {
  Key *k = new Key("abc", 123);
  Key *k2 = new Key("def", 456);

  KeyList *kl = new KeyList();
  kl->push_back(k);
  kl->push_back(k2);

  char *serializedKeyList = kl->serialize();
  KeyList *kl2 = KeyList::deserialize(serializedKeyList);

  EXPECT_EQUAL(kl, kl2);

  delete[] serializedKeyList;

  delete k;
  delete k2;

  delete kl;
  delete kl2;
}

// TEST_F(PersonalFixtures, kdstore_putAndGet) {
//   KDStore *kdstore = new KDStore();
//   Key *master = new Key("master", 0);
//   Key *master2 = new Key("master2", 0);

//   Schema *sch = new Schema("S");
//   DataFrame *df = new DataFrame(*sch);

//   Schema *sch2 = new Schema("DSIB");
//   DataFrame *df2 = new DataFrame(*sch2);

//   kdstore->put(*master, df);
//   kdstore->put(*master2, df2);

//   DataFrame *df3 = kdstore->get(*master);
//   DataFrame *df4 = kdstore->get(*master2);

//   EXPECT_EQUAL(df3, df);
//   EXPECT_EQUAL(df4, df2);

//   delete df4;
//   delete df3;
//   delete df2;
//   delete sch2;

//   delete df;
//   delete sch;

//   delete master2;
//   delete master;
//   delete kdstore;
// }

// TEST_F(PersonalFixtures, kklstore_putAndGet) {
//   KKLStore *klstore = new KKLStore();
//   Key *master = new Key("master", 0);
//   Key *master2 = new Key("master2", 0);

//   Key *k = new Key("abc", 123);
//   Key *k2 = new Key("def", 456);

//   KeyList *kl = new KeyList();
//   kl->push_back(k);
//   kl->push_back(k2);

//   KeyList *kl2 = new KeyList();

//   klstore->put(*master, kl);
//   klstore->put(*master2, kl2);

//   KeyList *kl3 = klstore->get(*master);
//   KeyList *kl4 = klstore->get(*master2);

//   EXPECT_EQUAL(kl3, kl);
//   EXPECT_EQUAL(kl4, kl2);

//   KeyList *kl5 = klstore->get(*master);
//   EXPECT_EQUAL(kl5, kl);

//   delete k;
//   delete k2;

//   delete kl;
//   delete kl2;
//   delete kl3;
//   delete kl4;
//   delete kl5;

//   delete master2;
//   delete master;
//   delete klstore;
// }